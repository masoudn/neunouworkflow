<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 3/6/15
  Time: 11:44 AM
  To change this template use File | Settings | File Templates.
--%>
<html ng-app="NeuNouApp">
    <div class="container-fluid main-content" ng-controller="groupRolesController">


    <div class="page-title">
        <p2>
            Groups and Roles
        </p2>
    </div>
    <div class="row">
    <div class="col-sm-3">
        <div class="widget-container fluid-height">
            <div class="widget-content">
                <div class="panel-group" id="accordion">
                    <div class="panel">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <a class="accordion-toggle" data-parent="#accordion" data-toggle="collapse" href="#collapseOne">
                                    <div class="caret pull-right"></div>
                                    Groups</a>
                            </div>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseOne">
                            <div class="panel-body" >

                                <ng-switch on="groupsListBool" >
                                    <div class="text-center" ng-switch-when="false">
                                        <div class="widget-content padded clearfix">
                                            <a class="btn btn-sm btn-primary-outline pull-right" ng-click="getAllGroups()" id="add-row"><i class="fa fa-plus"></i>Get all groups</a>
                                        </div>
                                    </div>
                                    <div ng-switch-when="true">

                                        <table class="table">
                                            <tbody>
                                            <tr  ng-repeat="group in groups">
                                                <td>
                                                    <label><input name="optionsRadios1" type="radio" value="true" ng-click="getGroupById($index)"><span></span></label>

                                                </td>
                                                <td>
                                                    {{group.groupName}}
                                                </td>
                                                <td class="actions">
                                                    <div class="action-buttons">
                                                        <a class="table-actions" ng-click="excludeGroup($index)"><i class="fa fa-trash-o"></i></a>
                                                    </div>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                    </div>


                                </ng-switch>
                            </div>
                        </div>
                    </div>
                    <div class="panel filter-categories">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <a class="accordion-toggle" data-parent="#accordion" data-toggle="collapse" href="#collapseThree">
                                    <div class="caret pull-right"></div>
                                    Departments</a>
                            </div>
                        </div>
                        <div class="panel-collapse collapse in" id="collapseThree">
                            <div class="panel-body">
                                <div class="btn-group" data-toggle="buttons">
                                    <label class="btn btn-block btn-info-outline active"><i class="fa fa-stethoscope"></i>Business Office<input id="option1" name="options" type="checkbox"></label>
                                    <label class="btn btn-block btn-success-outline"><i class="fa fa-coffee"></i>Murray Imaging Center<input id="option2" name="options" type="checkbox"></label>
                                    <label class="btn btn-block btn-warning-outline"><i class="fa fa-gamepad"></i>Vascular Center<input id="option3" name="options" type="checkbox"></label>
                                    <label class="btn btn-block btn-danger-outline"><i class="fa fa-gift"></i>Vein Center<input id="option4" name="options" type="checkbox"></label>
                                    <label class="btn btn-block btn-magenta-outline"><i class="fa fa-trophy"></i>Ogden Imaging Center<input id="option5" name="options" type="checkbox"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-9">
        <div class="widget-container fluid-height">
            <div class="widget-content">

        <div class="animate-show" ng-show="groupState.show">
            <form role="form" class="form-horizontal">
                <div class="form-group">
                    <label class="control-label col-md-2">Group Name</label>
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Group Name" type="text" ng-model="selectedGroup.groupName">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-2">Department</label>
                    <div class="col-md-3">
                        <input class="form-control" placeholder="Department Name" type="text" ng-model="selectedGroup.department">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <button  class="btn btn-default-outline">Cancel</button>
                    </div>
                </div>

            </form>



                    <table class="table">
            <thead>
            <th>
                Person Name
            </th>
            <th>
                Department
            </th>

            <th class="hidden-xs">
                Status
            </th>
            <th>
                Actions
            </th>
            </thead>
            <tbody>
            <tr     >
                <td>
                    <%--{{hl7.deviceName}}--%>

                    Testing
                </td>
                <td>
                    <%--{{hl7.host}}--%>
                    Test
                </td>
                <td class="hidden-xs">
                    <span class="label label-danger">Offline</span>
                </td>
                <td class="actions">
                    <div class="action-buttons">
                        <a class="table-actions" ng-click="showHL7Device()"><i class="fa fa-eye"></i></a>
                        <a class="table-actions" ng-click="editHL7($index)"><i class="fa fa-pencil"></i></a>
                        <a class="table-actions" ng-click="deleteHL7($index)"><i class="fa fa-trash-o"></i></a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>


        </div>
                </div>

        </div>




    </div>
        <div ng-hide="groupState.show">
            <p id="handwriting">
                &#10550;   Please pick a group to modify and see more data
            </p>

        </div>
    </div>



        <div class="btn btn-lg btn-default-outline btn-block">
            Load more
        </div>
    </div>








    </div>
</html>

