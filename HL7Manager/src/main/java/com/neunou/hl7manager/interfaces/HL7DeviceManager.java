package com.neunou.hl7manager.interfaces;

import ca.uhn.hl7v2.HL7Exception;
import com.neunou.schema.HL7Device;
import org.springframework.stereotype.Service;

/**
 * Created by Masoud on 9/17/14.
 */
@Service
public interface HL7DeviceManager extends GenericInterface{
    void startHL7Device(HL7Device hl7Device) throws InterruptedException, HL7Exception;
    void stopHL7Device(HL7Device hl7Device);
}
