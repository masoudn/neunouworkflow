package com.neunou.schema;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by root on 6/5/15.
 */
public class TimeLog {
    private String timeLogId;
    private Date logDate;
    private Date startTime;
    private Date endTime;
    private String personId;
    private Date creationDate;
    private int point;
    private BigDecimal price;
    private String notes;

    public String getTimeLogId() {
        return timeLogId;
    }

    public void setTimeLogId(String timeLogId) {
        this.timeLogId = timeLogId;
    }

    public Date getLogDate() {
        return logDate;
    }

    public void setLogDate(Date logDate) {
        this.logDate = logDate;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
}
