package com.neunou.dicompersistence.Impl;

import com.neunou.dicompersistence.Interfaces.DICOMDeviceDAO;
import com.neunou.schema.DICOMDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by root on 3/7/16.
 */
@Repository
public class DICOMDeviceDAOImpl implements DICOMDeviceDAO{

    private static final Logger LOG = Logger.getLogger(DICOMDeviceDAOImpl.class.getName());
    @Autowired
    private MongoTemplate mongoTemplate;
    private String DICOM_DEVICE_COL = "dicomDevice";
    @Override
    public String add(Object object) {
        DICOMDevice dicomDevice = (DICOMDevice) object;
        if (dicomDevice.getDicomDeviceId()==null){
            dicomDevice.setDicomDeviceId(UUID.randomUUID().toString());
            dicomDevice.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(DICOM_DEVICE_COL)){
                mongoTemplate.createCollection(DICOM_DEVICE_COL);
                mongoTemplate.insert(dicomDevice,DICOM_DEVICE_COL);
                return dicomDevice.getDicomDeviceId();
            }
            else {
                mongoTemplate.insert(dicomDevice,DICOM_DEVICE_COL);
                return dicomDevice.getDicomDeviceId();
            }
        }
        else {
            this.update(object);
            return dicomDevice.getDicomDeviceId();
        }

    }

    @Override
    public void delete(Object object) {
        DICOMDevice dicomDevice = (DICOMDevice) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDeviceId").is(dicomDevice.getDicomDeviceId()));

        mongoTemplate.remove(query,DICOMDevice.class,DICOM_DEVICE_COL);

    }

    @Override
    public void update(Object object) {
        DICOMDevice dicomDevice = (DICOMDevice) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDeviceId").is(dicomDevice.getDicomDeviceId()));

        dicomDevice.setModifiedDate(new Date());
        Update update = new Update();
        update.set("dicomDeviceName",dicomDevice.getDicomDeviceName());
        update.set("aet",dicomDevice.getAet());
        update.set("dicomDeviceIP",dicomDevice.getDicomDeviceIP());
        update.set("dicomDevicePort",dicomDevice.getDicomDevicePort());
        update.set("local",dicomDevice.isLocal());
        update.set("remote",dicomDevice.isRemote());
        update.set("dicomDirId",dicomDevice.getDicomDirId());
        update.set("running",dicomDevice.getRunning());
        update.set("useTLS",dicomDevice.getUseTLS());
        update.set("modifiedDate",dicomDevice.getModifiedDate());

        mongoTemplate.findAndModify(query,update,DICOMDevice.class,DICOM_DEVICE_COL);
    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDeviceId").is(id));
        return mongoTemplate.findOne(query,DICOMDevice.class,DICOM_DEVICE_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(DICOMDevice.class,DICOM_DEVICE_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,DICOMDevice.class,DICOM_DEVICE_COL);
        return exists;
    }

    @Override
    public void deleteDicomDeviceById(String id) {
        System.out.println("##################### Removing the DICOM Device: "+id);
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDeviceId").is(id));

        mongoTemplate.remove(query,DICOMDevice.class,DICOM_DEVICE_COL);
    }
}
