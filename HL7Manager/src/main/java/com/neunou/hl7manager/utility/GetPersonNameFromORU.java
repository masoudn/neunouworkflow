package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import com.neunou.schema.PersonName;

/**
 * Created by root on 2/24/16.
 */
public class GetPersonNameFromORU {
    private ORU_R01 oru;
    private PersonName name;

    public ORU_R01 getOru() {
        return oru;
    }

    public void setOru(ORU_R01 oru) {
        this.oru = oru;
    }

    public PersonName getName() {
        return name;
    }

    public void setName(PersonName name) {
        this.name = name;
    }

    public PersonName nameFactory(){
        setName(new PersonName());
        XPN[] patientName = oru.getPATIENT_RESULT().getPATIENT().getPID().getPatientName();
        for (XPN entry : patientName) {
            String fNAme = entry.getGivenName().getValue().toString();
            String lName = entry.getFamilyName().getSurname().toString();
            getName().setPersonFirstName(fNAme);
            getName().setPersonLastName(lName);
            getName().setPersonFullName(fNAme + " " + lName);
        }
        return getName();
    }

    public GetPersonNameFromORU(ORU_R01 oru) {
        this.oru = oru;
    }
}
