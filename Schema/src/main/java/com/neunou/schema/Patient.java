package com.neunou.schema;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Masoud on 9/16/14.
 */
public class Patient extends Person {
    private String patientId;
    private List<String> mrnNumbers;
    private List<Insurance> insuranceList;
    private List<Visit> visits;
    private Date creationDate;
    private Date modifiedDate;

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public List<String> getMrnNumbers() {
        return mrnNumbers;
    }

    public void setMrnNumbers(List<String> mrnNumbers) {
        this.mrnNumbers = mrnNumbers;
    }

    public List<Insurance> getInsuranceList() {
        return insuranceList;
    }

    public void setInsuranceList(List<Insurance> insuranceList) {
        this.insuranceList = insuranceList;
    }

    public List<Visit> getVisits() {
        return visits;
    }

    public void setVisits(List<Visit> visits) {
        this.visits = visits;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


}
