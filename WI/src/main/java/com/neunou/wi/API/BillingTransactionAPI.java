package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.BillingTransactionManager;
import com.neunou.schema.BillingTransactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 4/23/15.
 */
@Controller
@RequestMapping(value="/api/billingTransaction")
public class BillingTransactionAPI {
    private static final Logger LOG = Logger.getLogger(BillingTransactionAPI.class.getName());
    @Autowired
    private BillingTransactionManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BillingTransactions> getAll(){
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String saveBillingTransaction(@RequestBody BillingTransactions billingTransactions){
        man.add(billingTransactions);
        return "dashboard";
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{billingTransactionId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BillingTransactions getBillingBudgetTransactionsById(@PathVariable("billingTransactionId") String billingTransactionId){
        return (BillingTransactions)man.getById(billingTransactionId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{billingTransactionId:.+}")
    public void deleteBillingTransactionById (@PathVariable("billingTransactionId") String billingTransactionId){
        man.delete(man.getById(billingTransactionId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateBillingTransaction(@RequestBody BillingTransactions billingTransactions){
        this.man.update(billingTransactions);
    }
}
