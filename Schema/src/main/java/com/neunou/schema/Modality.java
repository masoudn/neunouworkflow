package com.neunou.schema;

/**
 * Created by root on 12/7/15.
 */
public enum Modality {
    CE,CR,CT,CTA,DR,DD,EC,HW,IR,IU,MR,MRA,MRS,Mammo,NM,Other,XRay,PET,RT,RF,RI,US
}
