package com.neunou.dicommanager.Impl;

import com.neunou.dicommanager.Interfaces.DICOMDirManager;
import com.neunou.dicompersistence.Interfaces.DICOMDirDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by root on 3/7/16.
 */
@Component
public class DICOMDirImpl implements DICOMDirManager{
    @Autowired
    private DICOMDirDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }

    @Override
    public void deleteDicomDirById(String id) {
        dao.deleteDicomDirById(id);
    }
}
