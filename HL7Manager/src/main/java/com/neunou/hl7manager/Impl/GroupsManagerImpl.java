package com.neunou.hl7manager.Impl;

import com.neunou.hl7manager.interfaces.GroupsManager;
import com.neunou.hl7persistence.Interfaces.GroupsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Masoud on 3/19/15.
 */
@Component
public class GroupsManagerImpl implements GroupsManager{
    @Autowired
    private GroupsDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }
}
