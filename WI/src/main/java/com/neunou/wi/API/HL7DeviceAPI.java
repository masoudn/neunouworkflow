package com.neunou.wi.API;

import ca.uhn.hl7v2.HL7Exception;
import com.neunou.hl7manager.interfaces.HL7DeviceManager;
import com.neunou.schema.HL7Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 3/5/15.
 */
@Controller
@RequestMapping(value="/api/hl7Device")
public class HL7DeviceAPI {
    private static final Logger LOG = Logger.getLogger(HL7DeviceAPI.class.getName());
    @Autowired
    private HL7DeviceManager hl7DeviceManager;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HL7Device> getAllHL7Devices(){
        return hl7DeviceManager.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public HL7Device saveDevice(@RequestBody HL7Device hl7Device){
        System.out.println("Adding the HL7Device....");
        LOG.info("Adding new HL7Device...");
        if(hl7Device.getOutbound()==null){
            System.out.println("The outbound is null");
            hl7Device.setOutbound(Boolean.FALSE);
        }
        if(hl7Device.getInbound()==null){
            hl7Device.setInbound(Boolean.FALSE);
        }
        if(hl7Device.getUseTLS()==null){
            hl7Device.setUseTLS(Boolean.FALSE);
        }
        hl7Device.setRunning(Boolean.FALSE);
        hl7DeviceManager.add(hl7Device);
        return hl7Device;
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{deviceId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HL7Device getDeviceById(@PathVariable("deviceId") String deviceId){
        return (HL7Device)hl7DeviceManager.getById(deviceId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{deviceId:.+}")
    public void deleteDeviceById (@PathVariable("deviceId") String deviceId){
        LOG.info("Deleting HL7Device...");
        hl7DeviceManager.delete(hl7DeviceManager.getById(deviceId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateDevice(@RequestBody HL7Device hl7Device){
        LOG.info("Updating the HL7Device...");
        this.hl7DeviceManager.update(hl7Device);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/start/{deviceId:.+}")
    public void startDeviceById (@PathVariable("deviceId") String deviceId) throws InterruptedException,HL7Exception {
        LOG.info("Starting HL7Device, getting the ID...");
        HL7Device device = (HL7Device) hl7DeviceManager.getById(deviceId);
        if (device.getRunning()==false){
            device.setRunning(Boolean.TRUE);
            //TODO check the device and make sure it is running first
            hl7DeviceManager.update(device);
            hl7DeviceManager.startHL7Device(device);
        }
        else{
            LOG.info("The HL7Device is already running");
        }

    }
    @RequestMapping(method = RequestMethod.POST, value = "/stop/{deviceId:.+}")
    public void stopDeviceById (@PathVariable("deviceId") String deviceId) throws InterruptedException,HL7Exception {
        LOG.info("Stoping HL7Device...");
        HL7Device device = (HL7Device) hl7DeviceManager.getById(deviceId);
        if(device.getRunning()==true){
            device.setRunning(Boolean.FALSE);
            hl7DeviceManager.update(device);
            hl7DeviceManager.stopHL7Device(device);
        }
        else{
            LOG.info("The HL7Device is not running");
        }
    }

}
