package com.neunou.hl7persistence.Interfaces;

import com.neunou.schema.Patient;
import com.neunou.schema.PersonName;
import org.springframework.stereotype.Service;

/**
 * Created by root on 4/16/15.
 */
@Service
public interface PatientDAO extends GenericInterface {
    Patient findPatientByNameAndDOB(PersonName name,String DOB);
    Patient findPatientByVisitNumber(String visitNum);
}
