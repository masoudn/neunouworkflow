package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import com.neunou.schema.Address;

/**
 * Created by root on 2/24/16.
 */
public class GetPatientAddressFromORU {
    private ORU_R01 oru;
    private Address address;

    public GetPatientAddressFromORU(ORU_R01 oru) {
        this.setOru(oru);
    }


    public ORU_R01 getOru() {
        return oru;
    }

    public void setOru(ORU_R01 oru) {
        this.oru = oru;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address addressFactory(){
        setAddress(new Address());
        for (XAD xad : oru.getPATIENT_RESULT().getPATIENT().getPID().getPatientAddress()) {
            getAddress().setAddress1(xad.getXad1_StreetAddress().getStreetOrMailingAddress().toString());
            getAddress().setCity(xad.getCity().toString());
            getAddress().setCountry(xad.getCountry().toString());
            getAddress().setState(xad.getStateOrProvince().toString());
            getAddress().setCity(xad.getCity().toString());
            getAddress().setZipCode(xad.getZipOrPostalCode().toString());
        }

        return getAddress();
    }
}
