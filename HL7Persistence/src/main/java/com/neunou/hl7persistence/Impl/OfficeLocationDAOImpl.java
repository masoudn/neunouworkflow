package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.OfficeLocationDAO;
import com.neunou.schema.OfficeLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Masoud on 4/1/15.
 */
@Repository
public class OfficeLocationDAOImpl implements OfficeLocationDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String Office_COL = "officeLocation";
    @Override
    public String add(Object object) {
        OfficeLocation officeLocation = (OfficeLocation) object;
        if(officeLocation.getOfficeLocationId()==null){
            officeLocation.setOfficeLocationId(UUID.randomUUID().toString());
            officeLocation.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(Office_COL)){
                mongoTemplate.createCollection(Office_COL);
                mongoTemplate.insert(officeLocation,Office_COL);
                return officeLocation.getOfficeLocationId();
            }
            else{
                mongoTemplate.insert(officeLocation,Office_COL);
                return officeLocation.getOfficeLocationId();
            }
        }
        else {
            this.update(object);
            return officeLocation.getOfficeLocationId();
        }
    }

    @Override
    public void delete(Object object) {
        OfficeLocation officeLocation = (OfficeLocation) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("officeLocationId").is(officeLocation.getOfficeLocationId()));

        mongoTemplate.remove(query,OfficeLocation.class,Office_COL);
    }

    @Override
    public void update(Object object) {
        OfficeLocation officeLocation = (OfficeLocation) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("officeLocationId").is(officeLocation.getOfficeLocationId()));

        officeLocation.setModifiedDate(new Date());
        Update update = new Update();

        update.set("officeLocation.address.address1",officeLocation.getAddress().getAddress1());
        update.set("officeLocation.address.address2",officeLocation.getAddress().getAddress2());
        update.set("officeLocation.address.city",officeLocation.getAddress().getCity());
        update.set("officeLocation.address.state",officeLocation.getAddress().getState());
        update.set("officeLocation.address.country",officeLocation.getAddress().getCountry());
        update.set("officeLocation.address.zipCode",officeLocation.getAddress().getZipCode());
        update.set("locationName",officeLocation.getLocationName());
        update.set("officeLocation.phoneNumber.cellPhoneNumber",officeLocation.getPhoneNumber().getCellPhoneNumber());
        update.set("officeLocation.phoneNumber.homePhoneNumber",officeLocation.getPhoneNumber().getHomePhoneNumber());
        update.set("officeLocation.phoneNumber.officePhoneNumber",officeLocation.getPhoneNumber().getOfficePhoneNumber());
        update.set("officeLocation.phoneNumber.faxNumber",officeLocation.getPhoneNumber().getFaxNumber());
        update.set("modifiedDate",officeLocation.getModifiedDate());
        update.set("billingRefName",officeLocation.getBillingRefName());

        mongoTemplate.findAndModify(query,update,OfficeLocation.class,Office_COL);

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("officeLocationId").is(id));
        return mongoTemplate.findOne(query,OfficeLocation.class,Office_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(OfficeLocation.class,Office_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,OfficeLocation.class,Office_COL);
        return exists;
    }
}
