package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.ChargeActiveRecordsDAO;
import com.neunou.schema.ChargeActiveRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 5/7/15.
 */
@Repository
public class ChargeActiveRecordsDAOImpl implements ChargeActiveRecordsDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String CHARGE_COL = "chargeActiveRecords";
    @Override
    public String add(Object object) {
        ChargeActiveRecords chargeActiveRecords = (ChargeActiveRecords) object;
        if(chargeActiveRecords.getChargeActiveRecordsId()==null){
            chargeActiveRecords.setChargeActiveRecordsId(UUID.randomUUID().toString());
            chargeActiveRecords.setCreationDateAudit(new Date());
            if(!mongoTemplate.collectionExists(CHARGE_COL)){
                mongoTemplate.createCollection(CHARGE_COL);
                mongoTemplate.insert(chargeActiveRecords,CHARGE_COL);
                return chargeActiveRecords.getChargeActiveRecordsId();
            }
            else {
                mongoTemplate.insert(chargeActiveRecords,CHARGE_COL);
                return chargeActiveRecords.getChargeActiveRecordsId();
            }
        }
        else {
            this.update(object);
            return chargeActiveRecords.getChargeActiveRecordsId();
        }
    }

    @Override
    public void delete(Object object) {
        ChargeActiveRecords chargeActiveRecords = (ChargeActiveRecords) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("chargeActiveRecordsId").is(chargeActiveRecords.getChargeActiveRecordsId()));

        mongoTemplate.remove(query,ChargeActiveRecords.class,CHARGE_COL);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("chargeActiveRecordsId").is(id));
        return mongoTemplate.findOne(query,ChargeActiveRecords.class,CHARGE_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(ChargeActiveRecords.class,CHARGE_COL);    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,ChargeActiveRecords.class,CHARGE_COL);
        return exists;
    }
}
