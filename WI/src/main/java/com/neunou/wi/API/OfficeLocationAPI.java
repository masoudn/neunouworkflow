package com.neunou.wi.API;

import com.neunou.hl7manager.interfaces.OfficeLocationManager;
import com.neunou.schema.OfficeLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 4/1/15.
 */
@Controller
@RequestMapping(value="/api/officeLocation")
public class OfficeLocationAPI {
    private static final Logger LOG = Logger.getLogger(OfficeLocationAPI.class.getName());
    @Autowired
    private OfficeLocationManager manager;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OfficeLocation> getAllLocations(){
        return manager.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public OfficeLocation saveDevice(@RequestBody OfficeLocation officeLocation){
        LOG.info("Adding new OfficeLocation...");
        manager.add(officeLocation);
        return officeLocation;
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{officeLocationId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OfficeLocation getOfficeLocationById(@PathVariable("officeLocationId") String officeLocationId){
        return (OfficeLocation)manager.getById(officeLocationId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{officeLocationId:.+}")
    public void deleteOfficeLocationById (@PathVariable("officeLocationId") String officeLocationId){
        LOG.info("Deleting OfficeLocation...");
        manager.delete(manager.getById(officeLocationId));
    }
}
