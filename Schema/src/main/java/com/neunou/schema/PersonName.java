package com.neunou.schema;

/**
 * Created by Masoud on 9/12/14.
 */
public class PersonName {
    private String personNameId;
    private String personFirstName;
    private String personMiddleName;
    private String personLastName;
    private String personFullName;


    public String getPersonNameId() {
        return personNameId;
    }

    public void setPersonNameId(String personNameId) {
        this.personNameId = personNameId;
    }

    public String getPersonFirstName() {
        return personFirstName;
    }

    public void setPersonFirstName(String personFirstName) {
        this.personFirstName = personFirstName;
    }

    public String getPersonMiddleName() {
        return personMiddleName;
    }

    public void setPersonMiddleName(String personMiddleName) {
        this.personMiddleName = personMiddleName;
    }

    public String getPersonLastName() {
        return personLastName;
    }

    public void setPersonLastName(String personLastName) {
        this.personLastName = personLastName;
    }

    public String getPersonFullName() {
        return personFullName;
    }

    public void setPersonFullName(String personFullName) {
        this.personFullName = personFullName;
    }


}
