package com.neunou.dicompersistence.Interfaces;

import org.springframework.stereotype.Service;

/**
 * Created by root on 3/7/16.
 */
@Service
public interface DICOMDeviceDAO extends GenericInterface{
    void deleteDicomDeviceById(String id);
}
