package com.neunou.hl7manager.utility;

import com.neunou.schema.VisitStatus;

/**
 * Created by root on 12/10/15.
 */
public class VisitStatusEnum {
    private String orderControlString;
    private String orderStatusString;
    private VisitStatus visitStatus;

    public VisitStatusEnum(String orderControlString, String orderStatusString) {
        this.orderControlString = orderControlString;
        this.orderStatusString = orderStatusString;
        System.out.println("++++OrderControl is: "+orderControlString);
        System.out.println("++++OrderStatus is: "+orderStatusString);
    }

    public VisitStatus transformToEnum(){
        if (this.orderControlString.equals("CA")){
            visitStatus = VisitStatus.Canceled;
        }
        if (this.orderControlString.equals("XO")){

            System.out.println("Status Code is: "+visitStatus.getStatusCode());
            if (this.orderStatusString.equals("CM")){
                visitStatus = VisitStatus.Changed;
                System.out.println();
            }
        }
        if (this.orderControlString.equals("NW")){
            visitStatus = VisitStatus.Scheduled;
            System.out.println("Status Code is: "+visitStatus.getStatusCode());
        }
        return visitStatus;
    }
}
