package com.neunou.dicommanager.Impl;

import com.neunou.dicommanager.Interfaces.DICOMDeviceManager;
import com.neunou.dicompersistence.Interfaces.DICOMDeviceDAO;
import com.neunou.dicompersistence.Interfaces.DICOMDirDAO;
import com.neunou.schema.DICOMDevice;
import com.neunou.schema.DICOMDirInfo;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.ConnectionMonitor;
import org.dcm4che3.net.Device;
import org.dcm4che3.tool.dcmdir.DcmDir;
import org.dcm4che3.tool.dcmqrscp.DcmQRSCP;
import org.dcm4che3.tool.storescp.StoreSCP;
import org.dcm4che3.tool.storescu.StoreSCU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

/**
 * Created by root on 3/7/16.
 */
@Component
public class DICOMDeviceImpl implements DICOMDeviceManager{
    @Autowired
    private DICOMDeviceDAO dao;
    @Autowired
    private DcmDir dcmDir;

    @Autowired
    private DICOMDirDAO dirDAO;

    @Autowired
    private DcmQRSCP dcmQRSCP;

    @Autowired
    private StoreSCP storeSCP;

    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }

    @Override
    public void deleteDicomDeviceById(String id) {
        dao.deleteDicomDeviceById(id);
    }

    @Override
    public void startDICOMDevice(String id) {
        System.out.println("DICOM DEVICE IMPL #################**********************"+id);
        DICOMDevice dicomDevice = (DICOMDevice) this.getById(id);
        System.out.println("DICOM DEVICE IMPL #################**********************"+dicomDevice.getDicomDeviceId());
        System.out.println("DICOM DEVICE IMPL #################**********************"+dicomDevice.getDicomDirId());
        String fileDirId = dicomDevice.getDicomDirId();
        System.out.println("DICOM DEVICE IMPL #################**********************"+fileDirId);
        DICOMDirInfo dicomDirInfo = (DICOMDirInfo) dirDAO.getById(fileDirId);
        System.out.println("DICOM DEVICE IMPL #################**********************"+dicomDirInfo.getFilePath());
        String dicomDirectory = dicomDirInfo.getFilePath();
        System.out.println("DICOM DEVICE IMPL #################**********************"+dicomDirectory);

        try {
//            System.out.println("---------############# Opening the DICOM Folder: "+dcmDir.getFile().toString());
            dcmDir.open(new File(dicomDirectory));
            System.out.println("---------############# Opening the DICOM Folder: "+dcmDir.getFile().toString());

            if (dicomDevice.isLocal()){
                System.out.println("---------############# DCMQRSCP");
                dcmQRSCP.setApplicationEntity(new ApplicationEntity(dicomDevice.getAet()));
                dcmQRSCP.getConnection().setPort(dicomDevice.getDicomDevicePort());
                dcmQRSCP.setDicomDirectory(dcmDir.getFile());
                dcmQRSCP.setDevice(this.getDCMDicomDevice(dicomDevice));
                System.out.println("---------############# DCMQRSCP" + dcmQRSCP.getDevice().getDefaultAE());
                System.out.println("---------############# DCMQRSCP"+dcmQRSCP.getConnection().getDevice().getDefaultAE());
                System.out.println("---------############# DCMQRSCP"+dcmQRSCP.getDicomDirectory().toString());
                System.out.println("---------############# DCMQRSCP"+dcmQRSCP.getApplicationEntity().toString());

                String[] args = new String[4];
                args[0]="-b";
                args[1]=dicomDevice.getAet()+":"+dicomDevice.getDicomDevicePort();
                args[2]="--dicomdir";
                args[3]=dicomDirectory;


                DcmQRSCP.main(args);
//                dcmQRSCP.init();
//                ExecutorService executorService = Executors.newCachedThreadPool();
//                ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
//                dcmQRSCP.getDevice().setScheduledExecutor(scheduledExecutorService);
//                dcmQRSCP.getDevice().setExecutor(executorService);
//
//                try {
//                    dcmQRSCP.getDevice().bindConnections();
//                } catch (GeneralSecurityException e) {
//                    System.out.println("---------############# DCMQRSCP: Could not BIND" );
//                    e.printStackTrace();
//                }
////
//                System.out.println("---------############# DCMQRSCP");
            }



////            Device device = new Device();
////            ApplicationEntity ae = new ApplicationEntity(dicomDevice.getAet());
////            device.setDefaultAE(ae);
////            dcmQRSCP.setDevice(this.getDCMDicomDevice(dicomDevice));
//            dcmQRSCP.setApplicationEntity(new ApplicationEntity(dicomDevice.getAet()));
//            dcmQRSCP.getConnection().setPort(dicomDevice.getDicomDevicePort());
////            dcmQRSCP.getConnection().set
//            dcmQRSCP.setDicomDirectory(dcmDir.getFile());
//            System.out.println("Is It Writable? " + dcmQRSCP.isWriteable());
//            storeSCP.setStorageDirectory(dcmDir.getFile());
//
//
////            dcmQRSCP.setInstanceAvailability();
//            dcmQRSCP.setSendPendingCGet(true);
//            dcmQRSCP.setStgCmtOnSameAssoc(true);
////            System.out.println("------------+++++++++++++AE Title: "+dcmQRSCP.getApplicationEntity());
////            System.out.println("------------+++++++++++++AE Title: "+dcmQRSCP.getDevice().getDefaultAE());
////            System.out.println("--------+++++++++++++"+dcmQRSCP.getConnection().getDevice().getConnections().get(1));
//
//            dcmQRSCP.init();

        } catch (IOException e) {
            System.out.println("---------#############Tryyyyyyyÿyyyyyyyy22222");
            try {
                System.out.println("---------#############Tryyyyyyyÿyyyyyyyy22222 TRYYYYYYY");
                dcmDir.create(new File(dicomDirectory));
                System.out.println("---------############# Opening the DICOM Folder222222");
//                dcmDir.open(new File(dicomDirectory));

            } catch (IOException e1) {
                System.out.println("---------#############Tryyyyyyyÿyyyyyyyy333333333333333");
                if (!dcmDir.getFile().exists()) {
                    System.out.println("The FILE does not exists......");
                    File dir = new File(dicomDirectory);
                    dir.mkdir();
                    dir.setExecutable(true);
                    dir.setReadable(true);
                    try {
                        System.out.println("---------#############Creating a new Directory");
                        dcmDir.create(new File(dicomDirectory));
                        System.out.println("---------############# Opening the DICOM Folder3333333");
//                        dcmDir.open(new File(dicomDirectory));
                    } catch (IOException e2) {
                        System.out.println("---------#############Tryyyyyyyÿyyyyyyyy44444444444444444");
                        e2.printStackTrace();
                    }

                }
                else {
                    System.out.println("The FILE DOES exists......");
                    try {
                        System.out.println("---------#############Creating a new Directory");
                        dcmDir.create(new File(dicomDirectory));
                        System.out.println("---------############# Opening the DICOM Folder55555");
//                        dcmDir.open(new File(dicomDirectory));
                    } catch (IOException e2) {
                        System.out.println("---------#############Tryyyyyyyÿyyyyyyyy44444444444444444");
                        e2.printStackTrace();
                    }
                }
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

    }

    @Override
    public void stopDICOMDevice(String id) {

    }

    private Device getDCMDicomDevice(DICOMDevice dDevice){
        Device device = new Device();
        ApplicationEntity ae = new ApplicationEntity(dDevice.getAet());
        Connection connection = new Connection("DicomDevice",dDevice.getDicomDeviceIP(),dDevice.getDicomDevicePort());


        device.setDefaultAE(ae);
        device.addConnection(connection);
        return device;
    }
}
