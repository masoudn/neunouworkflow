package com.neunou.wi.Utility;

/**
 * Created by Masoud on 2/18/15.
 */
public class GetGroupFromCN {
    private String cn;

    public GetGroupFromCN(String cn) {
        this.cn = cn;
    }

    private String getGroup(){
        String group = null;
        String[] dnElements =getCn().split(",");
        for(String element:dnElements) {
            if (element.toLowerCase().contains("cn")) {
                group = element.substring(element.indexOf("=") + 1);
            }
        }

        return group;
    }

    public String getCn() {
        return cn;
    }

    public void setCn(String cn) {
        this.cn = cn;
    }
}
