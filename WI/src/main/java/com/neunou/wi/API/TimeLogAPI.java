package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.TimeLogManager;
import com.neunou.schema.TimeLog;
import com.neunou.schema.TimeLogStat;
import com.neunou.wi.Utility.GetAuthPerson;

import net.neoremind.sshxcute.core.ConnBean;
import net.neoremind.sshxcute.core.SSHExec;
import net.neoremind.sshxcute.exception.TaskExecFailException;
import net.neoremind.sshxcute.task.CustomTask;
import net.neoremind.sshxcute.task.impl.ExecCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.naming.NamingException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;

/**
 * Created by root on 6/9/15.
 */
@Controller
@RequestMapping(value="/api/timelog")
public class TimeLogAPI {
    private static final Logger LOG = Logger.getLogger(TimeLogAPI.class.getName());
    @Autowired
    private TimeLogManager man;
    @Autowired
    private GetAuthPerson authPerson;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TimeLog> getTimeLogs(){
        LOG.info("Getting all of the logs");
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.GET, value="/getPending",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TimeLog getPendingTimeLog(){
        List<TimeLog> timeLogList= man.getAllTimeLogsByPerson(authPerson.getPersonIdFromDB());
        TimeLog pendingTimeLog = null;
        for (TimeLog timeLog : timeLogList) {
            if (timeLog.getStartTime()!=null && timeLog.getEndTime()==null){
                pendingTimeLog = timeLog;
            }

        }
        return pendingTimeLog;
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String saveTimeLog(@RequestBody TimeLog timeLog){
        LOG.info("Saving timelog ...");
        timeLog.setPersonId(authPerson.getPersonIdFromDB());
        return man.add(timeLog);
    }

    @RequestMapping(method = RequestMethod.POST, value="/update",consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateTimeLog(@RequestBody TimeLog timeLog){
        System.out.println("TIME LOG ID is: "+timeLog.getTimeLogId());
        LOG.info("Updating timelog ...");
        man.update(timeLog);
    }

    @RequestMapping(method = RequestMethod.GET, value ="/getAllByPerson",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TimeLog> getAllTimeLogsByPerson() throws NamingException {
        LOG.info("Getting all the log for "+authPerson.getPersonFromLDAP().getPersonInfo().getPersonName().getPersonLastName());
        return man.getAllTimeLogsByPerson(authPerson.getPersonIdFromDB());
    }

    @RequestMapping(method = RequestMethod.GET, value="/runScript/{userId:.+}/{startDate:.+}/{endDate:.+}")
    public void runScript(@PathVariable("userId") String userId,@PathVariable("startDate") String startDate,@PathVariable("endDate") String endDate) throws IOException, TaskExecFailException, TaskExecFailException {
        LOG.info("Starting the script...");
        String[] startDateSplit = startDate.split("-");
        String newStartDate = startDateSplit[2]+"/"+startDateSplit[1]+"/"+startDateSplit[0];

        String[] endDateSplit = endDate.split("-");
        String newEndDate = endDateSplit[2]+"/"+endDateSplit[1]+"/"+endDateSplit[0];

        ConnBean cb = new ConnBean("50.200.165.106 ", "root","let'sPlayServer!");
        SSHExec ssh = SSHExec.getInstance(cb);
        ssh.connect();
        CustomTask sampleTask = new ExecCommand("Rscript /root/Documents/overtimeAverageTest.R "+userId+" "+newStartDate+" "+newEndDate);
        ssh.exec(sampleTask);
        ssh.disconnect();

    }

    @RequestMapping(method = RequestMethod.POST, value = "/stat/{userId:.+}/{startDate:.+}/{endDate:.+}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String getStats(@RequestBody TimeLogStat timeLogStat){
        LOG.info("AvgPersonalOTPerShift:  "+timeLogStat.getAvgPersonalOTPerShift());
        LOG.info("TotalPersonalOT: "+timeLogStat.getTotalPersonalOT());
        LOG.info("AvgOTPerShift:  "+timeLogStat.getAvgOTPerShift());
        LOG.info("AvgOTPerDoc: "+timeLogStat.getAvgOTPerDoc());
        LOG.info("TotalPracticeOT: "+timeLogStat.getTotalPracticeOT());

        return "dashboard";
    }
}
