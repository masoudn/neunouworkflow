package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.CashTransactionDAO;
import com.neunou.schema.CashTransaction;
import com.neunou.schema.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Created by Masoud on 10/12/15.
 */
@Repository
public class CashTransactionDAOImpl implements CashTransactionDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String CASHTRANS_COL = "cashTransactions";
    @Override
    public String add(Object object) {
        CashTransaction cashTransaction = (CashTransaction) object;
        if(cashTransaction.getCashTransactionId()==null){
            cashTransaction.setCashTransactionId(UUID.randomUUID().toString());
            if(!mongoTemplate.collectionExists(CASHTRANS_COL)){
                mongoTemplate.createCollection(CASHTRANS_COL);
                mongoTemplate.insert(cashTransaction,CASHTRANS_COL);
                return cashTransaction.getCashTransactionId();
            }
            else {
                mongoTemplate.insert(cashTransaction,CASHTRANS_COL);
                return cashTransaction.getCashTransactionId();
            }
        }
        else {
            this.update(object);
            return cashTransaction.getCashTransactionId();
        }
    }

    @Override
    public void delete(Object object) {
        CashTransaction cashTransaction = (CashTransaction) object;

        Query query = new Query();
        query.addCriteria(Criteria.where("cashTransactionId").is(cashTransaction.getCashTransactionId()));

        mongoTemplate.remove(query,CashTransaction.class,CASHTRANS_COL);
    }

    @Override
    public void update(Object object) {
        CashTransaction cashTransaction = (CashTransaction) object;

        Query query = new Query();
        query.addCriteria(Criteria.where("cashTransactionId").is(cashTransaction.getCashTransactionId()));

        Update update = new Update();
        update.set("patient",cashTransaction.getPatient());
        update.set("modifyingPersonMap",cashTransaction.getModifyingPersonMap());
        update.set("amount",cashTransaction.getAmount());
        update.set("posted",cashTransaction.getPosted());
        update.set("verified",cashTransaction.getVerified());

        mongoTemplate.findAndModify(query,update,CashTransaction.class,CASHTRANS_COL);
    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("cashTransactionId").is(id));
        return mongoTemplate.findOne(query,CashTransaction.class,CASHTRANS_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(CashTransaction.class,CASHTRANS_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,CashTransaction.class,CASHTRANS_COL);
        return exists;
    }
}
