package com.neunou.wi.API;

import com.neunou.hl7manager.interfaces.PhysicianManager;
import com.neunou.schema.Physician;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 4/1/15.
 */
@Controller
@RequestMapping(value="/api/physician")
public class PhysicianAPI {
    private static final Logger LOG = Logger.getLogger(PhysicianAPI.class.getName());
    @Autowired
    private PhysicianManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Physician> getAllPhysicians(){
        LOG.info("Getting all of the physicians");
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String savePhysician(@RequestBody Physician physician){
        return man.add(physician);
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{physicianId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Physician getPhysicianById(@PathVariable("physicianId") String physicianId){
        return (Physician)man.getById(physicianId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{physicianId:.+}")
    public void deletePhysicianById (@PathVariable("physicianId") String physicianId){
        man.delete(man.getById(physicianId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updatePhysician(@RequestBody Physician physician){
        this.man.update(physician);
    }

}
