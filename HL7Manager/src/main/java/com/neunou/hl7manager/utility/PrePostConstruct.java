package com.neunou.hl7manager.utility;

import com.neunou.hl7manager.interfaces.HL7DeviceManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.logging.Logger;

/**
 * Created by root on 4/9/15.
 */
@Service
public class PrePostConstruct {
    private static final Logger LOG = Logger.getLogger(PrePostConstruct.class.getName());
    @Autowired
    private HL7DeviceManager hl7DeviceManager;


    @PostConstruct
    public void StartServices(){

    }

    @PreDestroy
    public void StopServices(){

    }
}
