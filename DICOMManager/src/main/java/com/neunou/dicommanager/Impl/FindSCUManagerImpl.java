package com.neunou.dicommanager.Impl;

import com.neunou.dicommanager.Interfaces.FindSCUManager;
import org.dcm4che3.tool.findscu.FindSCU;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

/**
 * Created by root on 10/14/15.
 */
@Component
@ComponentScan(basePackages = {"org.dcm4che3.tool.findscu.FindSCU"})
public class FindSCUManagerImpl implements FindSCUManager {
    @Autowired
    private FindSCU findSCU;
    @Override
    public void findSCUByPatientName(String patientName) throws IOException, InterruptedException {
        findSCU.addLevel("-m PatientName=Masoud^Nourmohammadi");
        findSCU.query();
    }

    @Override
    public String add(Object object) {
        return null;
    }

    @Override
    public void delete(Object object) {

    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        return null;
    }

    @Override
    public List getAll() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return null;
    }
}
