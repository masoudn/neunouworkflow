package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.BillingTransactionsDAO;
import com.neunou.schema.BillingTransactions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 4/23/15.
 */
@Repository
public class BillingTransactionDAOImpl implements BillingTransactionsDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String TRANS_COL = "billingTransactions";
    @Override
    public String add(Object object) {
        BillingTransactions billingTransactions = (BillingTransactions) object;
        if(billingTransactions.getBillingTransactionId()==null){
            billingTransactions.setBillingTransactionId(UUID.randomUUID().toString());
            if(!mongoTemplate.collectionExists(TRANS_COL)){
                mongoTemplate.createCollection(TRANS_COL);
                mongoTemplate.insert(billingTransactions, TRANS_COL);

                return billingTransactions.getBillingTransactionId();
            }
            else {
                mongoTemplate.insert(billingTransactions,TRANS_COL);
                return billingTransactions.getBillingTransactionId();
            }
        }
        else {
            this.update(object);
            return billingTransactions.getBillingTransactionId();
        }
    }

    @Override
    public void delete(Object object) {
        BillingTransactions billingTransactions = (BillingTransactions) object;

        Query query = new Query();
        query.addCriteria(Criteria.where("billingTransactionId").is(billingTransactions.getBillingTransactionId()));

        mongoTemplate.remove(query,BillingTransactions.class,TRANS_COL);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("billingTransactionId").is(id));
        return mongoTemplate.findOne(query,BillingTransactions.class,TRANS_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(BillingTransactions.class,TRANS_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,BillingTransactions.class,TRANS_COL);
        return exists;
    }
}
