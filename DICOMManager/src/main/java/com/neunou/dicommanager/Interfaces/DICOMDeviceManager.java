package com.neunou.dicommanager.Interfaces;

import org.springframework.stereotype.Service;

/**
 * Created by root on 3/7/16.
 */
@Service
public interface DICOMDeviceManager extends GenericInterface {
    void deleteDicomDeviceById(String id);
    void startDICOMDevice(String id);
    void stopDICOMDevice(String id);
}
