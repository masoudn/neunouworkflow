package com.neunou.hl7manager.Impl;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.protocol.impl.HL7Server;
import ca.uhn.hl7v2.validation.builder.support.NoValidationBuilder;
import com.neunou.hl7manager.interfaces.HL7DeviceManager;
import com.neunou.hl7manager.utility.CreateHL7Server;
import com.neunou.hl7persistence.Interfaces.HL7DeviceDAO;
import com.neunou.schema.Groups;
import com.neunou.schema.HL7Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 9/17/14.
 */
@Component
public class HL7DeviceImpl implements HL7DeviceManager{

    @Autowired
    private HL7DeviceDAO dao;
    @Autowired
    private CreateHL7Server createHL7Server;
    private HL7Service hl7Service;
    private static final Logger LOG = Logger.getLogger(HL7DeviceImpl.class.getName());
    @Override
    public void startHL7Device(HL7Device hl7Device) throws InterruptedException, HL7Exception {
        LOG.info("Creating a new HL7Server ...");
        createHL7Server.setHl7Device(hl7Device);
        createHL7Server.setMessageType("*");
        createHL7Server.setTriggerEvent("*");
//        CreateHL7Server createHL7Server = new CreateHL7Server(hl7Device,"*","*");
        this.hl7Service = createHL7Server.createServer();
        hl7Service.startAndWait();
        hl7Device.setRunning(Boolean.TRUE);
    }

    @Override
    public void stopHL7Device(HL7Device hl7Device) {
        this.hl7Service.stopAndWait();
        hl7Device.setRunning(Boolean.FALSE);
    }

    @Override
    public String add(Object object) {
        return this.dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);

    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return this.dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return null;
    }
}
