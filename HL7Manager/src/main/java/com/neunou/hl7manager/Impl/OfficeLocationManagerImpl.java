package com.neunou.hl7manager.Impl;

import com.neunou.hl7manager.interfaces.OfficeLocationManager;
import com.neunou.hl7persistence.Interfaces.OfficeLocationDAO;
import com.neunou.schema.OfficeLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 4/1/15.
 */
@Component
public class OfficeLocationManagerImpl implements OfficeLocationManager {
    @Autowired
    private OfficeLocationDAO dao;
    private OfficeLocation officeLocation;
    private static final Logger LOG = Logger.getLogger(OfficeLocationManagerImpl.class.getName());
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return (OfficeLocation)dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }
}
