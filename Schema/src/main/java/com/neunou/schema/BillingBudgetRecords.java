package com.neunou.schema;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by root on 4/7/15.
 */
public class BillingBudgetRecords  {
    private String billingBudgetRecordsId;
    private String serviceCenter;
    private String clientId;
    private String locationId;
    private String providerId;
    private String carrierType;
    private String startingDate;
    private String endingDate;
    private String clientName;
    private BigDecimal charges;
    private int numberOfCharges;
    private BigDecimal payments;
    private int numberOfPayments;
    private BigDecimal writeOffs;
    private BigDecimal refunds;
    private BigDecimal badDebt;
    private BigDecimal credits;
    private BigDecimal debits;
    private BigDecimal arTotal;
    private BigDecimal ar0to60;
    private BigDecimal ar61to90;
    private BigDecimal ar91to120;
    private BigDecimal arOver120;
    private String locationName;
    private String providerName;
    private String carrierTypeDesc;
    private String providerLocalId;
    private String locationLocalId;
    private String reportingDate;
    private int doneFlag;
    private String creationDate;
    private Audit audit;


    public String getBillingBudgetRecordsId() {
        return billingBudgetRecordsId;
    }

    public void setBillingBudgetRecordsId(String billingBudgetRecordsId) {
        this.billingBudgetRecordsId = billingBudgetRecordsId;
    }

    public String getServiceCenter() {
        return serviceCenter;
    }

    public void setServiceCenter(String serviceCenter) {
        this.serviceCenter = serviceCenter;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String provideId) {
        this.providerId = provideId;
    }

    public String getCarrierType() {
        return carrierType;
    }

    public void setCarrierType(String carrierType) {
        this.carrierType = carrierType;
    }

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public BigDecimal getCharges() {
        return charges;
    }

    public void setCharges(BigDecimal charges) {
        this.charges = charges;
    }

    public int getNumberOfCharges() {
        return numberOfCharges;
    }

    public void setNumberOfCharges(int numberOfCharges) {
        this.numberOfCharges = numberOfCharges;
    }

    public BigDecimal getPayments() {
        return payments;
    }

    public void setPayments(BigDecimal payments) {
        this.payments = payments;
    }

    public int getNumberOfPayments() {
        return numberOfPayments;
    }

    public void setNumberOfPayments(int numberOfPayments) {
        this.numberOfPayments = numberOfPayments;
    }

    public BigDecimal getWriteOffs() {
        return writeOffs;
    }

    public void setWriteOffs(BigDecimal writeOffs) {
        this.writeOffs = writeOffs;
    }

    public BigDecimal getRefunds() {
        return refunds;
    }

    public void setRefunds(BigDecimal refunds) {
        this.refunds = refunds;
    }

    public BigDecimal getBadDebt() {
        return badDebt;
    }

    public void setBadDebt(BigDecimal badDebt) {
        this.badDebt = badDebt;
    }

    public BigDecimal getCredits() {
        return credits;
    }

    public void setCredits(BigDecimal credits) {
        this.credits = credits;
    }

    public BigDecimal getDebits() {
        return debits;
    }

    public void setDebits(BigDecimal debits) {
        this.debits = debits;
    }

    public BigDecimal getArTotal() {
        return arTotal;
    }

    public void setArTotal(BigDecimal arTotal) {
        this.arTotal = arTotal;
    }

    public BigDecimal getAr0to60() {
        return ar0to60;
    }

    public void setAr0to60(BigDecimal ar0to60) {
        this.ar0to60 = ar0to60;
    }

    public BigDecimal getAr61to90() {
        return ar61to90;
    }

    public void setAr61to90(BigDecimal ar61to90) {
        this.ar61to90 = ar61to90;
    }

    public BigDecimal getAr91to120() {
        return ar91to120;
    }

    public void setAr91to120(BigDecimal ar91to120) {
        this.ar91to120 = ar91to120;
    }

    public BigDecimal getArOver120() {
        return arOver120;
    }

    public void setArOver120(BigDecimal arOver120) {
        this.arOver120 = arOver120;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getCarrierTypeDesc() {
        return carrierTypeDesc;
    }

    public void setCarrierTypeDesc(String carrierTypeDesc) {
        this.carrierTypeDesc = carrierTypeDesc;
    }

    public String getLocationLocalId() {
        return locationLocalId;
    }

    public void setLocationLocalId(String locationLocalId) {
        this.locationLocalId = locationLocalId;
    }

    public String getReportingDate() {
        return reportingDate;
    }

    public void setReportingDate(String reportingDate) {
        this.reportingDate = reportingDate;
    }

    public int getDoneFlag() {
        return doneFlag;
    }

    public void setDoneFlag(int doneFlag) {
        this.doneFlag = doneFlag;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    public Audit getAudit() {
        return audit;
    }

    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getProviderLocalId() {
        return providerLocalId;
    }

    public void setProviderLocalId(String providerLocalId) {
        this.providerLocalId = providerLocalId;
    }
}
