package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.protocol.ReceivingApplicationException;
import com.neunou.hl7manager.interfaces.GetObjectsFromHL7;
import com.neunou.hl7manager.interfaces.HL7MessageManager;
import com.neunou.hl7manager.interfaces.PatientManager;
import com.neunou.hl7manager.interfaces.PhysicianManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by Masoud on 11/4/14.
 * This class will manage the received messages.
 */
@Service
public class ReceiverApplicationHandler implements ReceivingApplication {
    private static final int NTHREDS = 100;
    ExecutorService executor = Executors.newFixedThreadPool(NTHREDS);
    @Autowired
    private HL7MessageManager hl7Man;
    @Autowired
    private PatientManager patMan;
    @Autowired
    private GetObjectsFromHL7 getObjectsFromHL7;
    @Autowired
    private PhysicianManager physicianManager;
    @Override
    public Message processMessage(Message message, Map<String, Object> stringObjectMap) throws ReceivingApplicationException, HL7Exception {

        String encodedMes = new DefaultHapiContext().getPipeParser().encode(message);
        System.out.println("Received message:\n" + encodedMes + "\n\n");
        System.out.println("***Received message:\n" + message + "\n\n");
        stringObjectMap.forEach((key,value) -> System.out.println("Here is key and value: "+key+" "+value));



        Runnable work = new MessageHandlerThread(message,hl7Man,patMan, getObjectsFromHL7,physicianManager);
        this.executor.execute(work);

        try{
            return message.generateACK();
        } catch (IOException e){
            throw new HL7Exception(e);
        }
    }

    @Override
    public boolean canProcess(Message message) {
        return true;
    }
}
