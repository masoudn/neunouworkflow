package com.neunou.schema;

import java.util.Date;
import java.util.List;

/**
 * Created by Masoud on 9/12/14.
 */
public class Audit {
    private String auditId;
    private String changedByPersonId;
    private Date createdOn;
    private List<Date> changedOn;


    public String getAuditId() {
        return auditId;
    }

    public void setAuditId(String auditId) {
        this.auditId = auditId;
    }

    public String getChangedByPersonId() {
        return changedByPersonId;
    }

    public void setChangedByPersonId(String changedByPersonId) {
        this.changedByPersonId = changedByPersonId;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }


    public List<Date> getChangedOn() {
        return changedOn;
    }

    public void setChangedOn(List<Date> changedOn) {
        this.changedOn = changedOn;
    }
}
