package com.neunou.dicommanager.Interfaces;

import org.springframework.stereotype.Service;

import java.io.IOException;

/**
 * Created by root on 10/14/15.
 */
@Service
public interface FindSCUManager extends GenericInterface {
    void findSCUByPatientName(String patientName) throws IOException, InterruptedException;
}
