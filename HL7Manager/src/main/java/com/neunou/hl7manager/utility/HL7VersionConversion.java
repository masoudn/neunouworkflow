package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.message.ORM_O01;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import ca.uhn.hl7v2.parser.CanonicalModelClassFactory;
import ca.uhn.hl7v2.parser.Parser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.validation.impl.NoValidation;

import java.util.logging.Logger;

/**
 * Created by root on 5/4/15.
 */
public class HL7VersionConversion {
    private String hl7String;
    private static final Logger LOG = Logger.getLogger(HL7VersionConversion.class.getName());

    public HL7VersionConversion(String hl7String) {
        System.out.println("$$$$$$Conversion constructor...");
        this.hl7String = hl7String;
    }

    public Message convertTo25() throws HL7Exception {
        System.out.println("$$$$$$Conversion method....");
        HapiContext context = new DefaultHapiContext();
        context.setValidationContext(new NoValidation());
        Parser p = context.getGenericParser();
        ca.uhn.hl7v2.model.Message hapiMsg;
        hapiMsg = p.parse(hl7String);
        System.out.println("$$$$$$Conversion method, sending to if....");

        if (hapiMsg.getName().contains("ORM")&&!hapiMsg.getVersion().contains("2.5")){
            LOG.info("An ORM not 2.5 message was requested to dissect!");
            CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.5");
            context.setModelClassFactory(mcf);
            PipeParser parser = context.getPipeParser();
            ORM_O01 msg = (ORM_O01) parser.parse(hl7String.toString());
            return msg;
        }
        else if (hapiMsg.getName().contains("ORM")&&hapiMsg.getVersion().contains("2.5")){
            LOG.info("An ORM 2.5 message was requested to dissect!");
            return hapiMsg;
        }
        else if(hapiMsg.getName().contains("ORU")&&!hapiMsg.getVersion().contains("2.5")){
            System.out.println("$$$$$$Conversion method, first ORU if....");
            LOG.info("An ORU not 2.5 message was requested to dissect!");
            CanonicalModelClassFactory mcf = new CanonicalModelClassFactory("2.5");
            context.setModelClassFactory(mcf);
            PipeParser parser = context.getPipeParser();
            System.out.println("$$$$$$Conversion method, getting the message....");
            ORU_R01 msg = (ORU_R01) parser.parse(hl7String.toString());
            return msg;
        }
        else if(hapiMsg.getName().contains("ORU")&&hapiMsg.getVersion().contains("2.5")){
            System.out.println("$$$$$$Conversion method, second ORU if....");
            LOG.info("An ORU 2.5 message was requested to dissect!");
            return hapiMsg;
        }
        else {
            return null;
        }
    }

}
