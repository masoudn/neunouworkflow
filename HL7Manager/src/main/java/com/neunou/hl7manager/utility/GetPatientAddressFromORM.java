package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.datatype.XAD;
import ca.uhn.hl7v2.model.v25.message.ORM_O01;
import com.neunou.schema.Address;

/**
 * Created by root on 2/24/16.
 */
public class GetPatientAddressFromORM {
    private ORM_O01 orm;
    private Address address;

    public GetPatientAddressFromORM(ORM_O01 orm) {
        this.orm = orm;
    }

    public ORM_O01 getOrm() {
        return orm;
    }

    public void setOrm(ORM_O01 orm) {
        this.orm = orm;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Address addressFactory(){
        setAddress(new Address());
        for (XAD xad : orm.getPATIENT().getPID().getPatientAddress()) {
            getAddress().setAddress1(xad.getXad1_StreetAddress().getStreetOrMailingAddress().toString());
            getAddress().setCity(xad.getCity().toString());
            getAddress().setCountry(xad.getCountry().toString());
            getAddress().setState(xad.getStateOrProvince().toString());
            getAddress().setCity(xad.getCity().toString());
            getAddress().setZipCode(xad.getZipOrPostalCode().toString());
        }
        return getAddress();
    }
}
