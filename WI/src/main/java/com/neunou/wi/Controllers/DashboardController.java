package com.neunou.wi.Controllers;

import com.neunou.hl7persistence.Impl.LDAPUserImpl;
import com.neunou.schema.Person;
import com.neunou.schema.Role;
import com.neunou.wi.Utility.GetAuthPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.naming.NamingException;
import java.util.*;
import java.util.logging.Logger;

/**
 * Created by Masoud on 2/12/15.
 */
@Controller
public class DashboardController {
    @Autowired
    private GetAuthPerson getAuthPerson;

    private static final Logger LOG = Logger.getLogger(DashboardController.class.getName());

    @Resource(name="ldapTemplate")
    private LdapTemplate tmpl;

    @Autowired
    private LDAPUserImpl ldapUser;


    @RequestMapping(value="dashboard.htm",method= RequestMethod.GET)
    public String loadDashboardPage(Model model) throws NamingException {
        Person logedInUser = getAuthPerson.getPersonFromLDAP();
        String dn = logedInUser.getPersonInfo().getDn();
        List<Role> roles = logedInUser.getRoleList();
        System.out.println("Role size in dashboard is: "+roles.size());
        Boolean admin = Boolean.FALSE;
        for(Role role:roles){
            if (role.getRole().equalsIgnoreCase("ROLE_ADMIN")){
                admin = Boolean.TRUE;
            }
        }

//        Collection<? extends GrantedAuthority> roles = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
//
//        for (GrantedAuthority role:roles){
//            if (role.toString().equalsIgnoreCase("ROLE_ADMIN")){
//                admin = Boolean.TRUE;
//            }
//        }

        LOG.info(logedInUser.getPersonInfo().getPersonName().getPersonFullName());
        model.addAttribute("person",getAuthPerson.getPersonFromLDAP());
//        model.addAttribute("dn",dn);
        model.addAttribute("admin",admin);
        model.addAttribute("roleList",roles);
        return "dashboard";
    }
}
