package com.neunou.wi.Config;

import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.tool.dcmdir.DcmDir;
import org.dcm4che3.tool.dcmqrscp.DcmQRSCP;
import org.dcm4che3.tool.storescp.StoreSCP;
import org.dcm4che3.tool.storescu.StoreSCU;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.dcm4che3.tool.findscu.FindSCU;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 3/9/16.
 */
@Configuration
@ComponentScan(basePackages = {"org.dcm4che3.tool.*"})
public class BeanCreator {
    private String dicomFileAddress;
    private List<String> aeTitles;

    @Bean
    public FindSCU newFindSCU() throws IOException {
        FindSCU findSCU = new FindSCU();


        return findSCU;
    }

    @Bean
    public DcmDir newDicomDir() throws IOException {
        DcmDir dicomDir = new DcmDir();

        return dicomDir;
    }

    @Bean
    public DcmQRSCP newDCMQRSCP() throws IOException {
        DcmQRSCP dcmQRSCP = new DcmQRSCP();
        return dcmQRSCP;
    }

    @Bean
    public StoreSCP newStoreSCP() throws IOException {
        StoreSCP storeSCP = new StoreSCP();

        return storeSCP;
    }


//    @Bean
//    public List<StoreSCU> newStoreSCU() throws IOException {
//        List<StoreSCU> scuList = new ArrayList<StoreSCU>();
//        for (String ae : getAeTitles()) {
//            ApplicationEntity aeTitle = new ApplicationEntity(ae);
//            StoreSCU storeSCU = new StoreSCU(aeTitle);
//            scuList.add(storeSCU);
//        }
////        getAeTitles().forEach(ae -> );
//
//        return scuList;
//
//    }

    public String getDicomFileAddress() {
        return dicomFileAddress;
    }

    public void setDicomFileAddress(String dicomFileAddress) {
        this.dicomFileAddress = dicomFileAddress;
    }

    public List<String> getAeTitles() {
        return aeTitles;
    }

    public void setAeTitles(List<String> aeTitles) {
        this.aeTitles = aeTitles;
    }
}
