<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 2/20/15
  Time: 4:23 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="col-lg-12">
        <div class="widget-container fluid-height clearfix">
            <div class="heading">
                <i class="fa fa-toggle-down"></i>Select2 Dropdowns
            </div>
            <div class="widget-content padded">
                <form action="#" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Select2 Dropdown</label>
                        <div class="col-md-7">
                            <select class="select2able"><option value="Category 1">Option 1</option><option value="Category 2">Option 2</option><option value="Category 3">Option 3</option><option value="Category 4">Option 4</option></select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-2">Multi-Select2</label>
                        <div class="col-md-7">
                            <select class="select2able" multiple=""><option value="Category 1">Option 1</option><option value="Category 2">Option 2</option><option value="Category 3">Option 3</option><option value="Category 4">Option 4</option></select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

