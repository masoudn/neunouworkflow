package com.neunou.hl7persistence.Interfaces;

import com.neunou.schema.HL7Device;
import org.springframework.stereotype.Service;

/**
 * Created by Masoud on 9/17/14.
 */
@Service
public interface HL7DeviceDAO extends GenericInterface {
    void startHL7Device(HL7Device hl7Device);
    void stopHL7Device(HL7Device hl7Device);
}
