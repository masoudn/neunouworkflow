<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 2/12/15
  Time: 12:49 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!-- Login Screen -->
<div class="login-wrapper">
    <div class="login-container">
        <a href="./"><img width="200" height="40" src="template/images/logo.png" /></a>
        <form action="j_spring_security_check" method="post" role="form">
            <div class="form-group">
                <input class="form-control" placeholder="Username or Email" type="text" id="j_username" name="j_username">
            </div>
            <div class="form-group">
                <input class="form-control" placeholder="Password" id="j_password" name="j_password" type="password"><input type="submit" value="&#xf054;" >
            </div>
            <div class="form-options clearfix">
                <a class="pull-right" href="#">Forgot password?</a>
                <div class="text-left">
                    <label class="checkbox"><input type="checkbox"><span>Remember me</span></label>
                </div>
            </div>
        </form>

        <p class="signup">
            Don't have an account yet? <a href="signup1.html">Sign up now</a>
        </p>
    </div>
</div>
<!-- End Login Screen -->
