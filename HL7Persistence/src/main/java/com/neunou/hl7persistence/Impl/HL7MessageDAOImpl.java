package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.HL7MessageDAO;
import com.neunou.schema.HL7Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 4/15/15.
 */
@Repository
public class HL7MessageDAOImpl implements HL7MessageDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String Message_COL = "hl7Document";
    @Override
    public String add(Object object) {
        HL7Document hl7Document = (HL7Document) object;
        if(hl7Document.gethL7DocumentId()==null){
            hl7Document.sethL7DocumentId(UUID.randomUUID().toString());
            hl7Document.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(Message_COL)){
                mongoTemplate.createCollection(Message_COL);
                mongoTemplate.insert(hl7Document,Message_COL);
                return hl7Document.gethL7DocumentId();
            }
            else {
                mongoTemplate.insert(hl7Document,Message_COL);
                return hl7Document.gethL7DocumentId();
            }
        }
        else {
            this.update(object);
            return hl7Document.gethL7DocumentId();
        }
    }

    @Override
    public void delete(Object object) {
        HL7Document hl7Document = (HL7Document) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("hL7DocumentId").is(hl7Document.gethL7DocumentId()));

        mongoTemplate.remove(query,HL7Document.class,Message_COL);
    }

    @Override
    public void update(Object object) {
        HL7Document hl7Document = (HL7Document) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("hL7DocumentId").is(hl7Document.gethL7DocumentId()));

        hl7Document.setModifiedDate(new Date());
        Update update = new Update();
        update.set("hl7MessageTypes.hl7Type",hl7Document.getHl7MessageTypes().getHl7Type());
        update.set("hl7MessageTypes.hl7Version",hl7Document.getHl7MessageTypes().getHl7Version());
        update.set("hl7MessageTypes.hl7TypeDesc",hl7Document.getHl7MessageTypes().getHl7TypeDesc());
        update.set("patientId",hl7Document.getPatientId());
        update.set("referringPhysicianId",hl7Document.getReferringPhysicianId());
        update.set("providerId",hl7Document.getProviderId());
        update.set("hl7MessageContent",hl7Document.getHl7MessageContent());
        update.set("modifiedDate",hl7Document.getModifiedDate());

        mongoTemplate.findAndModify(query,update,HL7Document.class,Message_COL);

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("hL7DocumentId").is(id));
        return mongoTemplate.findOne(query,HL7Document.class,Message_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(HL7Document.class,Message_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,HL7Document.class,Message_COL);
        return exists;
    }
}
