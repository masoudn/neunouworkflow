package com.neunou.romanager.Interfaces;

import com.neunou.schema.TimeLog;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by root on 6/8/15.
 */
@Service
public interface TimeLogManager extends GenericInterface {
    List<TimeLog> getAllTimeLogsByPerson(String personId);
}
