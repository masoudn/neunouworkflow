package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.v25.datatype.CX;
import ca.uhn.hl7v2.model.v25.datatype.TS;
import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.message.ORM_O01;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import com.neunou.hl7manager.interfaces.GetObjectsFromHL7;
import com.neunou.hl7manager.interfaces.HL7MessageManager;
import com.neunou.hl7manager.interfaces.PatientManager;
import com.neunou.hl7manager.interfaces.PhysicianManager;
import com.neunou.schema.*;

import java.text.ParseException;
import java.util.*;
import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by Masoud on 11/6/14.
 * This handler only works for HL7 v25
 * This thread is being called from ReceiverApplicationHandler class
 */

public class MessageHandlerThread implements Runnable {

    private Message message;
    private HL7Document hl7Document;
    private HL7MessageManager hl7Man;
    private PatientManager patMan;
    private PhysicianManager physicianManager;
    private GetObjectsFromHL7 getObjectsFromHL7;
    Physician physician = new Physician();
    private static final Logger LOG = Logger.getLogger(MessageHandlerThread.class.getName());

    public MessageHandlerThread(Message message, HL7MessageManager hl7Man, PatientManager patMan, GetObjectsFromHL7 getObjectsFromHL7, PhysicianManager physicianManager) {
        this.message = message;
        this.hl7Man = hl7Man;
        this.patMan = patMan;
        this.getObjectsFromHL7 = getObjectsFromHL7;
        this.physicianManager = physicianManager;
    }

    @Override
    public void run() {
        System.out.println("Message name is: " + message.getName());
        this.hl7Document = new HL7Document();
        Patient patient = new Patient();
        PersonInfo personInfo = new PersonInfo();
        PersonName personName = new PersonName();


        HL7MessageTypes messageTypes = new HL7MessageTypes();


        if (message.getName().contains("ORU")) {
            messageTypes.setHl7Type("ORU");
            messageTypes.setHl7Version(message.getVersion().toString());
            HL7VersionConversion conversion = new HL7VersionConversion(message.toString());
            Visit visit = new Visit();
            ORU_R01 oruMsg = null;
            try {
                oruMsg = (ORU_R01) conversion.convertTo25();
                String dobString = oruMsg.getPATIENT_RESULT().getPATIENT().getPID().getPid7_DateTimeOfBirth().getTs1_Time().toString();
                LOG.info("DOB String in ORU is: "+dobString);
                //Get Referring Physician
                this.physician = getObjectsFromHL7.getPhysician(oruMsg.toString());
                physician.setPhysicianId(physicianManager.add(physician));
                System.out.println("**********************Refer*********************************"+physician.getPhysicianId());

                //Get Dictating Physician
//                Physician dictatingPhysician = new Physician();
//                dictatingPhysician = getObjectsFromHL7.getReadingPhysician(oruMsg.toString());

                String visitNumber = oruMsg.getPATIENT_RESULT().getPATIENT().getVISIT().getPV1().getVisitNumber().getCx1_IDNumber().toString();
                System.out.println("Visit Number is: "+visitNumber);
                Patient pat = patMan.findPatientByVisitNumber(visitNumber);
                if (pat !=null){
                    LOG.info("Patient exists in the DB, retrieving it..."+pat.getPatientId()+"  "+pat.getPersonInfo().getDOB()+"  "+pat.getPersonInfo().getPersonName().getPersonLastName());
                    List<Visit> visits = new ArrayList<>();
                    visits = pat.getVisits();
                    visit = visits.stream().filter(v -> v.getVisitNumber().contains(visitNumber)).findFirst().get();
                    Visit visitFromMsg = getObjectsFromHL7.getVisit(oruMsg.toString());
                    HL7Document hl7Doc = new HL7Document();
                    hl7Doc.setCreationDate(new Date());
                    List<HL7Document> hl7s = visit.getHl7Documents();
                    System.out.println("Getting HL7 Docsss");
                    hl7s.add(getObjectsFromHL7.getHL7Doc(message.toString()));
                    patMan.update(pat);


                }
                else {
                    LOG.info("Patient does not exist in the DB ....");
                    System.out.println("---------Getting the MRNs----------");
                    List<String> mrns = new ArrayList<>();
                    GetMRNFromORU getMRN = new GetMRNFromORU(oruMsg);
                    mrns.add(getMRN.mrnFactory());
                    System.out.println("---------Getting the VISIT----------");
                    visit = getObjectsFromHL7.getVisit(oruMsg.toString());
                    System.out.println("---------Getting the PatInfo----------");
                    PersonInfo patInfo = getObjectsFromHL7.getPersonInfo(oruMsg.toString());
                    List<Visit> visits = new ArrayList<>();
                    visits.add(visit);
                    patient.setVisits(visits);
                    patient.setPersonInfo(patInfo);
                    patient.setMrnNumbers(mrns);

                    LOG.info("ADDING PATIENT TO DB...............................");
                    patMan.add(patient);
                }
            } catch (HL7Exception e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }

        if (message.getName().contains("ORM")) {
            messageTypes.setHl7Type("ORM");
            messageTypes.setHl7Version(message.getVersion().toString());
            System.out.println("Version is: " + message.getVersion().toString());

            HL7VersionConversion conversion = new HL7VersionConversion(message.toString());
            try {
                List<Insurance> insuranceList = new ArrayList<>();
                ORM_O01 ormMsg = (ORM_O01) conversion.convertTo25();

                //Get Referring Physician
                this.physician = getObjectsFromHL7.getPhysician(ormMsg.toString());
                physician.setPhysicianId(physicianManager.add(physician));

                //Get patient visit
                Visit visit = getObjectsFromHL7.getVisit(ormMsg.toString());
                if (this.physician.getPhysicianId()!=null){
                    visit.setPhysicianId(this.physician.getPhysicianId());
                }

                List<Visit> visitList = new ArrayList<>();
                visitList.add(visit);
                patient.setVisits(visitList);

                XPN[] patientName = ormMsg.getPATIENT().getPID().getPatientName();
                TS dob = ormMsg.getPATIENT().getPID().getDateTimeOfBirth();

                String dobString = dob.getTime().toString();
                personInfo.setDOB(dobString);
                List<String> ids = new ArrayList<>();
                ids.add(ormMsg.getPATIENT().getPID().getPatientID().getIDNumber().toString());

                for (XPN entry : patientName) {
                    personName.setPersonFirstName(entry.getGivenName().getValue().toString());
                    personName.setPersonLastName(entry.getFamilyName().getSurname().toString());
                    personInfo.setPersonName(personName);
                    patient.setPersonInfo(personInfo);

                    System.out.println("Family Name " + entry.getFamilyName().getSurname().toString());
                    System.out.println("First Name " + entry.getGivenName().getValue().toString());

                }
                if (patient.getMrnNumbers() != null) {
                    for (String id : ids) {
                        patient.getMrnNumbers().add(id);
                    }
                } else {
                    patient.setMrnNumbers(ids);
                }
                try {

                    insuranceList = getObjectsFromHL7.getInsurance(message.toString());
                } catch (HL7Exception e) {
                    e.printStackTrace();
                }
                patient.setInsuranceList(insuranceList);

                if (dobString!=null){
                    if (patMan.findPatientByNameAndDOB(personName,dobString)!=null){
                        LOG.info("Patient already exist");
                        String patientId = patMan.findPatientByNameAndDOB(personName,dobString).getPatientId();
                        patMan.update(patient);
                        hl7Document.setPatientId(patientId);
                    }
                    else {
                        System.out.println("We are in Message Handler, in else....");
                        hl7Document.setHl7MessageContent(ormMsg.toString());
                        String patientId = patMan.add(patient);
                        hl7Document.setPatientId(patientId);
                    }
                }
                else {
                    System.out.println("Could not run the dobstring, it is null");
                }


                hl7Document.setPrintStructure(ormMsg.printStructure().toString());

            } catch (HL7Exception e) {
                e.printStackTrace();
            } catch (ParseException e) {
                e.printStackTrace();
            }


        }
    }

    public HL7Document getHl7Document() {
        return hl7Document;
    }

    public void setHl7Document(HL7Document hl7Document) {
        this.hl7Document = hl7Document;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }


}
