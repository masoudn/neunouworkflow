package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.app.Connection;
import ca.uhn.hl7v2.app.ConnectionListener;
import ca.uhn.hl7v2.app.HL7Service;
import ca.uhn.hl7v2.app.Initiator;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.protocol.ReceivingApplication;
import ca.uhn.hl7v2.protocol.ReceivingApplicationExceptionHandler;
import ca.uhn.hl7v2.protocol.impl.HL7Server;
import ca.uhn.hl7v2.validation.impl.NoValidation;
import com.neunou.schema.HL7Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by Masoud on 11/4/14.
 */
@Service
public class CreateHL7Server {

    private String messageType;
    private String triggerEvent;
    private HL7Device hl7Device;
    private HL7Service hl7Service;
    @Autowired
    private ReceiverApplicationHandler handler;

    private static final Logger LOG = Logger.getLogger(CreateHL7Server.class.getName());


    public HL7Service createServer() throws InterruptedException, HL7Exception {
        Boolean inbound = this.hl7Device.getInbound();
        System.out.println("Inbound: "+inbound);
//        System.out.println("Outbound "+this.hl7Device.getOutbound());
        LOG.info("Port is: "+this.hl7Device.getPort());
        LOG.info("TLS is: "+this.hl7Device.getUseTLS());
        LOG.info("Starting the HL7Server: "+hl7Device.getDeviceName());
        HapiContext ctx = new DefaultHapiContext();
        ctx.setValidationContext(new NoValidation());
        HL7Service server = ctx.newServer(this.hl7Device.getPort(),this.hl7Device.getUseTLS());


        if(this.hl7Device.getInbound()){
//            ReceivingApplication handler = new ReceiverApplicationHandler();
            server.registerApplication(messageType,triggerEvent,handler);
            server.registerConnectionListener(new MyConnectionListener());
            server.setExceptionHandler(new MyExceptionHandler());

            setHl7Service(server);
        }
        if(this.hl7Device.getOutbound()){
            Connection connection = ctx.newClient(this.hl7Device.getHost(),this.hl7Device.getPort(),this.hl7Device.getUseTLS());
            Initiator initiator = connection.getInitiator();
//            Message response = initiator.sendAndReceive();
        }
        else {
            LOG.info("Could not start or initiate the HL7Server. There is no Inbound or Outbound server defined");
        }


        return getHl7Service();
    }

    public HL7Service getHl7Service() {
        return hl7Service;
    }

    public void setHl7Service(HL7Service hl7Service) {
        this.hl7Service = hl7Service;
    }


    public static class MyConnectionListener implements ConnectionListener{

        @Override
        public void connectionReceived(Connection connection) {
            System.out.println("New connection received: " + connection.getRemoteAddress().toString());
        }

        @Override
        public void connectionDiscarded(Connection connection) {
            System.out.println("Lost connection from: " + connection.getRemoteAddress().toString());
        }
    }

    public static class MyExceptionHandler implements ReceivingApplicationExceptionHandler {

        @Override
        public String processException(String s, Map<String, Object> stringObjectMap, String s2, Exception e) throws HL7Exception {
            System.out.println("This is the exception!");
            return s2;
        }
    }


    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getTriggerEvent() {
        return triggerEvent;
    }

    public void setTriggerEvent(String triggerEvent) {
        this.triggerEvent = triggerEvent;
    }

    public HL7Device getHl7Device() {
        return hl7Device;
    }

    public void setHl7Device(HL7Device hl7Device) {
        this.hl7Device = hl7Device;
    }
}
