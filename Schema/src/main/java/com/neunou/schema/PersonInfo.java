package com.neunou.schema;

import java.util.Date;
import java.util.List;

/**
 * Created by Masoud on 2/17/15.
 */
public class PersonInfo {
    private String personInfoId;
    private String description;
    private PhoneNumber phoneNumber;
    private Address address;
    private String emailAddress;
    private String uid;
    private String employeeNumber;
    private PersonName personName;
    private List<String> ou;
    private List<String> dc;
    private String dn;
    private String managerPersonId;
    private String managerDN;
    private String title;
    private String distinguishedName;
    private String displayName;
    private List<String> memberOf;
    private String department;
    private String username;
    private String pictureURL;
    private String dob;
    private String gender;
    private String employmentStatus;
    private String ssn;

    /**
     * @return the phoneNumber
     */
    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    /**
     * @param phoneNumber the phoneNumber to set
     */
    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    /**
     * @return the emailAddress
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * @param emailAddress the emailAddress to set
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress.toLowerCase();
    }

    /**
     * @return the personInfoId
     */
    public String getPersonInfoId() {
        return personInfoId;
    }

    /**
     * @param personInfoId the personInfoId to set
     */
    public void setPersonInfoId(String personInfoId) {
        this.personInfoId = personInfoId;

    }

    /**
     * @return the uid
     */
    public String getUid() {
        return uid;
    }

    /**
     * @param uid the uid to set
     */
    public void setUid(String uid) {
        this.uid = uid.toLowerCase();
    }

    /**
     * @return the employeeNumber
     */
    public String getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * @param employeeNumber the employeeNumber to set
     */
    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    /**
     * @return the personName
     */
    public PersonName getPersonName() {
        return personName;
    }

    /**
     * @param personName the personName to set
     */
    public void setPersonName(PersonName personName) {
        this.personName = personName;
    }

    /**
     * @return the ou
     */
    public List<String> getOu() {
        return ou;
    }

    /**
     * @param ou the ou to set
     */
    public void setOu(List<String> ou) {
        this.ou = ou;
    }

    /**
     * @return the dc
     */
    public List<String> getDc() {
        return dc;
    }

    /**
     * @param dc the dc to set
     */
    public void setDc(List<String> dc) {
        this.dc = dc;
    }

    /**
     * @return the dn
     */
    public String getDn() {
        return dn;
    }

    /**
     * @param dn the dn to set
     */
    public void setDn(String dn) {
        this.dn = dn.toLowerCase();
    }

    /**
     * @return the managerPersonId
     */
    public String getManagerPersonId() {
        return managerPersonId;
    }

    /**
     * @param managerPersonId the managerPersonId to set
     */
    public void setManagerPersonId(String managerPersonId) {
        this.managerPersonId = managerPersonId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description.toLowerCase();
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title.toLowerCase();
    }


    public String getDistinguishedName() {
        return distinguishedName;
    }

    public void setDistinguishedName(String distinguishedName) {
        this.distinguishedName = distinguishedName.toLowerCase();
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName.toLowerCase();
    }


    public List<String> getMemberOf() {
        return memberOf;
    }

    public void setMemberOf(List<String> memberOf) {
        this.memberOf = memberOf;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department.toLowerCase();
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username.toLowerCase();
    }

    public String getManagerDN() {
        return managerDN;
    }

    public void setManagerDN(String managerDN) {
        this.managerDN = managerDN.toLowerCase();
    }


    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getDOB() {
        return dob;
    }

    public void setDOB(String dob) {
        this.dob = dob;
    }


    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmploymentStatus() {
        return employmentStatus;
    }

    public void setEmploymentStatus(String employmentStatus) {
        this.employmentStatus = employmentStatus;
    }

    public String getSsn() {
        return ssn;
    }

    public void setSsn(String ssn) {
        this.ssn = ssn;
    }
}

