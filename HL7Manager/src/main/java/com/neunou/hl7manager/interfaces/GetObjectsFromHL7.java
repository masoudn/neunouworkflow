package com.neunou.hl7manager.interfaces;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Message;
import com.neunou.schema.*;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

/**
 * Created by root on 5/1/15.
 */
@Service
public interface GetObjectsFromHL7 {
    List<Insurance> getInsurance(String hl7String) throws HL7Exception;
    Visit getVisit(String hl7String) throws HL7Exception, ParseException;
    PersonInfo getPersonInfo(String hl7String) throws HL7Exception;
    Physician getPhysician(String hl7String) throws HL7Exception;
    Physician getReadingPhysician(String hl7String) throws HL7Exception;
    HL7Document getHL7Doc(String hl7String) throws HL7Exception, ParseException;
//    Message changeVto25(String hl7String) throws HL7Exception;
}
