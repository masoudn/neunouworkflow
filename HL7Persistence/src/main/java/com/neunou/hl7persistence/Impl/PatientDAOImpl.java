package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.PatientDAO;
import com.neunou.hl7persistence.Utility.VisitStatusUtility;
import com.neunou.schema.HL7Document;
import com.neunou.schema.Patient;
import com.neunou.schema.PersonName;
import com.neunou.schema.Visit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.function.Predicate;

/**
 * Created by root on 4/16/15.
 */
@Repository
public class PatientDAOImpl implements PatientDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String Patient_COL = "patient";
    @Override
    public String add(Object object) {
        Patient patient = (Patient) object;
        System.out.println(""+existance("personInfo.personName.personLastName",patient.getPersonInfo().getPersonName().getPersonLastName()));

        if(patient.getPatientId()==null){
            patient.setPatientId(UUID.randomUUID().toString());
            patient.setCreationDate(new Date());

            if(existance("personInfo.personName.personLastName",patient.getPersonInfo().getPersonName().getPersonLastName())){
                System.out.print("Checking the first name...");
                if(existance("personInfo.personName.personFirstName",patient.getPersonInfo().getPersonName().getPersonFirstName())){
                    System.out.print("Checking the DOB...");
                    if (existance("personInfo.dob",patient.getPersonInfo().getDOB())){
                        System.out.print("\n This patient exists, updating..."+patient.getPatientId()+"  "+patient.getPersonInfo().getDOB()+" "+patient.getPersonInfo().getPersonName().getPersonLastName());
                        this.update(object);

                    }
                }
                return patient.getPatientId();
            }
            else {
                if(!mongoTemplate.collectionExists(Patient_COL)){
                    mongoTemplate.createCollection(Patient_COL);
                    mongoTemplate.insert(patient,Patient_COL);
                    return patient.getPatientId();
                }
                else {
                    mongoTemplate.insert(patient,Patient_COL);
                    return patient.getPatientId();
                }
            }

        }
        else {
            this.update(object);
            return patient.getPatientId();
        }
    }

    @Override
    public void delete(Object object) {
        Patient patient = (Patient) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("patientId").is(patient.getPatientId()));

        mongoTemplate.remove(query,Patient.class,Patient_COL);
    }

    @Override
    public void update(Object object) {
        System.out.println("-----------------------------");
        Patient patient = (Patient) object;
        System.out.println("Updating Patient INFO"+patient.getPatientId());
        Query query = new Query();
        if (patient.getPatientId()!=null || this.getById(patient.getPatientId())!=null) {
            System.out.println("\n Updating Patient NOT NULL");
            query.addCriteria(Criteria.where("patientId").is(patient.getPatientId()));
            patient.setModifiedDate(new Date());
            Update update = new Update();
            Patient patientFromDB = mongoTemplate.findOne(query,Patient.class,Patient_COL);
            final List<String> mrns = patientFromDB.getMrnNumbers();
            final List<Visit> visits = patientFromDB.getVisits();

            System.out.println("%%%%%Updating MRN numbers%%%%%%%");
            patient.getMrnNumbers().parallelStream().filter(mrn -> !mrns.contains(mrn)).forEach(mrn -> mrns.add(mrn));

            //TODO finish this
            update.set("mrnNumbers",patient.getMrnNumbers());
//            update.push("insuranceList",patient.getInsuranceList());
//            update.push("visits",patient.getVisits());

            mongoTemplate.findAndModify(query,update,Patient.class,Patient_COL);
        }
        else {
            System.out.println("\n Updating Patient NULL");
            if(existance("personInfo.personName.personLastName",patient.getPersonInfo().getPersonName().getPersonLastName())){
                System.out.print("Checking the first name...");
                if(existance("personInfo.personName.personFirstName",patient.getPersonInfo().getPersonName().getPersonFirstName())){
                    System.out.print("Checking the DOB...");
                    if (existance("personInfo.dob",patient.getPersonInfo().getDOB())){
                        System.out.print("This patient exists, updating...");
                        System.out.println("222%%%%%Updating MRN numbers%%%%%%%");
                        query.addCriteria(Criteria.where("personInfo.personName.personLastName").is(patient.getPersonInfo().getPersonName().getPersonLastName()).and("personInfo.personName.personFirstName").is(patient.getPersonInfo().getPersonName().getPersonFirstName())
                                .and("personInfo.dob").is(patient.getPersonInfo().getDOB()));
                        patient.setModifiedDate(new Date());
                        Update update = new Update();
                        Patient patientFromDB = mongoTemplate.findOne(query,Patient.class,Patient_COL);
                        final List<String> mrns = patientFromDB.getMrnNumbers();
                        final List<Visit> visits = patientFromDB.getVisits();

                        //Updating the MRN numbers
                        patient.getMrnNumbers().parallelStream().filter(mrn -> !mrns.contains(mrn)).forEach(mrn -> mrns.add(mrn));
//                        for (String mrn : mrns) {
//                            String newMRN = patient.getMrnNumbers().parallelStream().filter(number -> !mrn.contains(number)).findFirst().get();
//                            mrns.add(newMRN)
//                        }

                        //Updating the visit status
                        System.out.println("**--**--We are trying to update the order status ....");
                        patientFromDB.getVisits().parallelStream().forEach(v -> System.out.println("From DB: " + v.getVisitStatus()));
                        patientFromDB.getVisits().parallelStream().forEach(v -> System.out.println("From DB: " + v.getVisitStatus().getStatusCode()));
                        patient.getVisits().parallelStream().forEach(v -> System.out.println("From Message: "+v.getVisitStatus()));
                        patient.getVisits().parallelStream().forEach(v -> System.out.println("From Message: "+v.getVisitStatus().getStatusCode()));


                        System.out.println("**--**--**--**--**--**--**--**--**--**--**--**--**--**--");

                        update.set("mrnNumbers",mrns);

                        //Updating the visits
                        for (Visit visit:patient.getVisits()){
                            System.out.println("222%%%%%This is the for looooop%%%%%%%");


                            //Checking the visit number and accession number
                            Optional foundVisit = patientFromDB.getVisits().parallelStream().filter(vis -> vis.getVisitNumber().contains(visit.getVisitNumber())).findFirst();
                            if (foundVisit.isPresent()){
                                Visit dbVisit = (Visit) foundVisit.get();
                                System.out.println("111%%%%%Second This is IF statement%%%%%%%"+visit.getVisitNumber());
                                System.out.println("222%%%%%Second This is IF statement%%%%%%%"+dbVisit.getVisitNumber());

                                //Checking the visit status
//                                VisitStatusUtility visitStatusUtility = new VisitStatusUtility(visit.getVisitStatus(),dbVisit.getVisitStatus());
//                                visitStatusUtility.newVisitStatus();

                                //Updating the Accession Number
                                if(visit.getAccessionNumber().contentEquals(dbVisit.getAccessionNumber())&&visit.getVisitNumber().contentEquals(dbVisit.getVisitNumber())){
                                    System.out.println("333%%%%Accession number and visit numbers are the same%%%%%%%");
                                    List<HL7Document> dbHL7s = dbVisit.getHl7Documents();
                                    dbHL7s.forEach(doc -> visit.getHl7Documents().add(doc));
                                    visits.removeIf(v -> v.getVisitNumber().contains(visit.getVisitNumber())&&v.getAccessionNumber().contains(visit.getAccessionNumber()));
                                    visits.add(visit);
                                    System.out.println("333%%%%%Size of visit%%%%%%%"+visits.size());
                                    update.set("visits",visits);

                                    mongoTemplate.findAndModify(query,update,Patient.class,Patient_COL);
                                }
                                //Updating the Accession Number
                                else if(visit.getVisitNumber().contentEquals(dbVisit.getVisitNumber())&&!visit.getAccessionNumber().contentEquals(dbVisit.getAccessionNumber())){
                                    System.out.println("333%%%%Accession number and visit numbers are NOT the same%%%%%%%");
                                    query.addCriteria(Criteria.where("visits.visitNumber").is(dbVisit.getVisitNumber()).and("visits.accessionNumber").is(dbVisit.getAccessionNumber()));
                                    String newAcc = dbVisit.getAccessionNumber();
//                                    newAcc.concat(",").concat(visit.getAccessionNumber());
                                    newAcc = newAcc +","+visit.getAccessionNumber();
                                    System.out.println("New Acc is: "+newAcc);
                                    visit.setAccessionNumber(newAcc);
                                    List<HL7Document> dbHL7s = dbVisit.getHl7Documents();
                                    dbHL7s.forEach(doc -> visit.getHl7Documents().add(doc));
                                    visits.add(visit);
                                    update.set("visits",visits);

                                    mongoTemplate.findAndModify(query,update,Patient.class,Patient_COL);
                                }


                            }
                            else {
                                System.out.println("222%%%%%Second This is the ELSE statement%%%%%%%"+visit.getVisitNumber());
                                visits.add(visit);
                                this.addVisitPerPatient(patient);
                                update.set("visits",visits);

                                mongoTemplate.findAndModify(query,update,Patient.class,Patient_COL);
                            }
                        }

//                        update.push("insuranceList",patient.getInsuranceList());
//                        update.push("visits",visits);

                    }
                }
            }
        }


    }



    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("patientId").is(id));
        return mongoTemplate.findOne(query,Patient.class,Patient_COL);
    }

    @Override
    public List getAll() {

        return mongoTemplate.findAll(Patient.class,Patient_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,Patient.class,Patient_COL);
        return exists;
    }

    @Override
    public Patient findPatientByNameAndDOB(PersonName name, String DOB) {
        System.out.println("This is the DAO, find patient by name and DOB...");
        Query query = new Query();
        query.addCriteria(Criteria.where("personInfo.personName.personFirstName").is(name.getPersonFirstName()).andOperator(Criteria.where("personInfo.personName.personLastName").is(name.getPersonLastName()),(Criteria.where("personInfo.dob").is(DOB))));
        return mongoTemplate.findOne(query,Patient.class,Patient_COL);
    }

    @Override
    public Patient findPatientByVisitNumber(String visitNum) {
        Query query = new Query();
        query.addCriteria(Criteria.where("visits.visitNumber").is(visitNum));

        return mongoTemplate.findOne(query,Patient.class,Patient_COL);
    }

    private static  Predicate<String> checkVisitNumber(final String visitNumber){
        return visNum -> visNum.contains(visitNumber);
    }

//    public void updateVisit(Visit visit){
//        System.out.println("Updating Visit....."+visit.getVisitNumber());
//    }
//
//    public void addVisit (Visit visit){
//        System.out.println("Adding Visit....."+visit.getVisitNumber());
//    }

    public void updateVisitPerPatient(Patient patient) {

        System.out.println("Updating Visit....." + patient.getVisits().size());

    }

    public void addVisitPerPatient (Patient patient){
        System.out.println("Adding Visit....."+patient.getVisits().size());
    }
}
