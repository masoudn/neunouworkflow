package com.neunou.schema;

import java.util.Date;

/**
 * Created by root on 3/7/16.
 */
public class DICOMDevice {
    private String dicomDeviceId;
    private String dicomDeviceName;
    private String aet;
    private String dicomDeviceIP;
    private int dicomDevicePort;
    private boolean local;
    private boolean remote;
    private Boolean running;
    private Boolean useTLS;
    private String dicomDirId;
    private Date creationDate;
    private String createdByUserId;
    private Date modifiedDate;


    public String getDicomDeviceId() {
        return dicomDeviceId;
    }

    public void setDicomDeviceId(String dicomDeviceId) {
        this.dicomDeviceId = dicomDeviceId;
    }

    public String getDicomDeviceName() {
        return dicomDeviceName;
    }

    public void setDicomDeviceName(String dicomDeviceName) {
        this.dicomDeviceName = dicomDeviceName;
    }

    public String getAet() {
        return aet;
    }

    public void setAet(String aet) {
        this.aet = aet;
    }

    public String getDicomDeviceIP() {
        return dicomDeviceIP;
    }

    public void setDicomDeviceIP(String dicomDeviceIP) {
        this.dicomDeviceIP = dicomDeviceIP;
    }

    public int getDicomDevicePort() {
        return dicomDevicePort;
    }

    public void setDicomDevicePort(int dicomDevicePort) {
        this.dicomDevicePort = dicomDevicePort;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreatedByUserId() {
        return createdByUserId;
    }

    public void setCreatedByUserId(String createdByUserId) {
        this.createdByUserId = createdByUserId;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getDicomDirId() {
        return dicomDirId;
    }

    public void setDicomDirId(String dicomDirId) {
        this.dicomDirId = dicomDirId;
    }

    public Boolean getRunning() {
        return running;
    }

    public void setRunning(Boolean running) {
        this.running = running;
    }

    public Boolean getUseTLS() {
        return useTLS;
    }

    public void setUseTLS(Boolean useTLS) {
        this.useTLS = useTLS;
    }

    public boolean isLocal() {
        return local;
    }

    public void setLocal(boolean local) {
        this.local = local;
    }

    public boolean isRemote() {
        return remote;
    }

    public void setRemote(boolean remote) {
        this.remote = remote;
    }
}
