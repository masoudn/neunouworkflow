package com.neunou.schema;

import java.util.Date;

/**
 * Created by Masoud on 3/30/15.
 */
public class PhysicianSchedule {
    private String physicianScheduleId;
    private String physicianId;
    private Date scheduleDate;
    private String startTime;
    private String endTime;
    private String officeLocationId;
    private String shiftName;
    private String credit;
    private Date createdDate;
    private Date modifiedDate;


    public String getPhysicianScheduleId() {
        return physicianScheduleId;
    }

    public void setPhysicianScheduleId(String physicianScheduleId) {
        this.physicianScheduleId = physicianScheduleId;
    }

    public String getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }

    public Date getScheduleDate() {
        return scheduleDate;
    }

    public void setScheduleDate(Date scheduleDate) {
        this.scheduleDate = scheduleDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOfficeLocationId() {
        return officeLocationId;
    }

    public void setOfficeLocationId(String officeLocation) {
        this.officeLocationId = officeLocation;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getShiftName() {
        return shiftName;
    }

    public void setShiftName(String shiftName) {
        this.shiftName = shiftName;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }
}
