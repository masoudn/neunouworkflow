package com.neunou.hl7persistence.Interfaces;

import java.util.List;

/**
 * Created by Masoud on 9/17/14.
 */
public interface GenericInterface<T> {
    String add(T object);

    void delete(T object);

    void update(T object);

    T getById(String id);

    List<T> getAll();

    int count();

    Boolean existance(String element, String elementValue);
}
