package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.BillingBudgetRecordsManager;
import com.neunou.schema.BillingBudgetRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 4/9/15.
 */
@Controller
@RequestMapping(value="/api/billingBudget")
public class BillingBudgetRecordAPI {
    private static final Logger LOG = Logger.getLogger(BillingBudgetRecordAPI.class.getName());
    @Autowired
    private BillingBudgetRecordsManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BillingBudgetRecords> getAll(){
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String saveBillingBudget(@RequestBody BillingBudgetRecords billingBudgetRecords){
        man.add(billingBudgetRecords);
        return "dashboard";
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{billingBudgetRecordsId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BillingBudgetRecords getBillingBudgetRecordsById(@PathVariable("billingBudgetRecordsId") String billingBudgetRecordsId){
        return (BillingBudgetRecords)man.getById(billingBudgetRecordsId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{billingBudgetRecordsId:.+}")
    public void deleteBillingBudgetById (@PathVariable("billingBudgetRecordsId") String billingBudgetRecordsId){
        man.delete(man.getById(billingBudgetRecordsId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateBillingBudget(@RequestBody BillingBudgetRecords billingBudgetRecords){
        this.man.update(billingBudgetRecords);
    }
}
