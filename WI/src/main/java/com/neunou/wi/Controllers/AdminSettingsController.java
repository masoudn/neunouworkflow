package com.neunou.wi.Controllers;

import com.neunou.hl7persistence.Impl.LDAPUserImpl;
import com.neunou.schema.Person;
import com.neunou.schema.Role;
import com.neunou.wi.Utility.GetAuthPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.naming.NamingException;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 2/20/15.
 */
@Controller
public class AdminSettingsController {
    @Autowired
    private GetAuthPerson authPerson;
    @Autowired
    private LDAPUserImpl ldapUser;
    private static final Logger LOG = Logger.getLogger(AdminSettingsController.class.getName());

    @RequestMapping(value="/admin/settings",method= RequestMethod.GET)
    public String loadAdminSettingsPage(Model model) throws NamingException {

        model.addAttribute("person",authPerson.getPersonFromLDAP());
        model.addAttribute("admin",getAdmin());
        return "settings";
    }

    @RequestMapping(value="/admin/devices",method= RequestMethod.GET)
    public String loadDeviceSettingsPage(Model model) throws NamingException {

        model.addAttribute("person",authPerson.getPersonFromLDAP());
        model.addAttribute("admin",getAdmin());
        return "devices";
    }

    @RequestMapping(value="/admin/roles",method= RequestMethod.GET)
    public String loadRolesSettingsPage(Model model) throws NamingException {

        List<String> groups = ldapUser.getAllGroups();

        for(String group:ldapUser.getAllGroups()){
            System.out.println("Group is: "+group);
        }

        model.addAttribute("groups",groups);
        model.addAttribute("person",authPerson.getPersonFromLDAP());
        model.addAttribute("admin",getAdmin());
        return "roles";
    }

    private Boolean getAdmin() throws NamingException {
        Person person = authPerson.getPersonFromLDAP();
        List<Role> roles = person.getRoleList();
        Boolean admin = Boolean.FALSE;
        for(Role role:roles){
            if (role.getRole().equalsIgnoreCase("ROLE_ADMIN")){
                admin = Boolean.TRUE;
            }
        }
        return admin;
    }
}
