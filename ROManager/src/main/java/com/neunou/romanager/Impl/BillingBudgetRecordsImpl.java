package com.neunou.romanager.Impl;

import com.neunou.romanager.Interfaces.BillingBudgetRecordsManager;
import com.neunou.ropersistence.Interfaces.BillingBudgetRecordsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by root on 4/8/15.
 */
@Component
public class BillingBudgetRecordsImpl implements BillingBudgetRecordsManager {
    @Autowired
    private BillingBudgetRecordsDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }
}
