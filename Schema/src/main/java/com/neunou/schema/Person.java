package com.neunou.schema;

import java.util.Date;
import java.util.List;

/**
 * Created by Masoud on 9/12/14.
 */
public class Person {
    private String personId;
    private PersonInfo personInfo;
    private List<Role> roleList;
    private List<Date> webLoginDates;
    private List<Date> mobileLoginDates;

    public String getPersonId() {
        return personId;
    }

    public void setPersonId(String personId) {
        this.personId = personId;
    }

    public PersonInfo getPersonInfo() {
        return personInfo;
    }

    public void setPersonInfo(PersonInfo personInfo) {
        this.personInfo = personInfo;
    }

    public List<Date> getWebLoginDates() {
        return webLoginDates;
    }

    public void setWebLoginDates(List<Date> webLoginDates) {
        this.webLoginDates = webLoginDates;
    }

    public List<Date> getMobileLoginDates() {
        return mobileLoginDates;
    }

    public void setMobileLoginDates(List<Date> mobileLoginDates) {
        this.mobileLoginDates = mobileLoginDates;
    }

    public List<Role> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<Role> roleList) {
        this.roleList = roleList;
    }
}
