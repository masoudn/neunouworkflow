package com.neunou.romanager.Interfaces;

import com.neunou.schema.Person;
import org.springframework.stereotype.Service;

/**
 * Created by root on 6/15/15.
 */
@Service
public interface UsersManager extends GenericInterface {
    Person getByUsername (String username);
}
