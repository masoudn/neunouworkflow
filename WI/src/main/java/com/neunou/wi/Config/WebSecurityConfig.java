package com.neunou.wi.Config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import java.util.logging.Logger;

/**
 * Created by Masoud on 2/12/15.
 */
@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOG = Logger.getLogger(WebSecurityConfig.class.getName());
    @Autowired
    private MyAuthenticationSuccessHandler authenticationSuccessHandler;
    @Autowired
    private CustomLdapAuthority customLdapAuthority;

    @Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        authManagerBuilder
                .ldapAuthentication()
                .groupRoleAttribute("cn")
                .groupSearchBase("ou=MMPS,dc=mmic,dc=local").groupRoleAttribute("ou").ldapAuthoritiesPopulator(this.customLdapAuthority).rolePrefix("ROLE_")
                .groupSearchFilter("member={0}")
                .userSearchFilter("sAMAccountName={0}")
                .userSearchBase("ou=mmps, dc=mmic, dc=local")
                .contextSource()
//                .url("ldap://10.1.13.50:389")
                .url("ldap://50.200.165.70:389")
                .managerDn("cn=ldapauth,ou=ServiceAccounts,ou=MMPS,dc=mmic,dc=local")
                .managerPassword("ITacc3ss");

        System.out.println("Loading the LDAP Setting...");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()

                .authorizeRequests().antMatchers("/admin/**").hasAnyRole("ADMIN").anyRequest().authenticated().and()
                                // Possibly more configuration ...
                .formLogin() // enable form based log in
                        // set permitAll for all URLs associated with Form Login
                .usernameParameter("j_username")
                        .passwordParameter("j_password")
                        .loginProcessingUrl("/j_spring_security_check")
                        .loginPage("/login.htm")
                        .failureUrl("/login.htm?error=true")
                        .defaultSuccessUrl("/private/dashboard.htm")
                        .permitAll().successHandler(this.authenticationSuccessHandler)
                        .and()
                        .logout()
                        .logoutUrl("/logout.htm")
                        .logoutSuccessUrl("/login.htm")
                        .invalidateHttpSession(true)
                        .permitAll()
                        .and()

                        .rememberMe();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring().antMatchers("/template/**");
    }

}
