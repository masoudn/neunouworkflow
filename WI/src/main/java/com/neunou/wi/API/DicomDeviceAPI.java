package com.neunou.wi.API;

import com.neunou.dicommanager.Interfaces.DICOMDeviceManager;
import com.neunou.schema.DICOMDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 3/8/16.
 */
@Controller
@RequestMapping(value="/api/dicomDevice")
public class DicomDeviceAPI {
    private static final Logger LOG = Logger.getLogger(DicomDeviceAPI.class.getName());
    @Autowired
    private DICOMDeviceManager dicomMan;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DICOMDevice> getAllDicomDevices(){
        return dicomMan.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DICOMDevice saveDevice(@RequestBody DICOMDevice dicomDevice){
        System.out.println("Adding the DICOMDevice....");
        LOG.info("Adding new DICOMDevice...");
        dicomMan.add(dicomDevice);

        return dicomDevice;
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{deviceId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DICOMDevice getDeviceById(@PathVariable("deviceId") String deviceId){
        return (DICOMDevice)dicomMan.getById(deviceId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{deviceId:.+}")
    public void deleteDeviceById (@PathVariable("deviceId") String deviceId){
        dicomMan.deleteDicomDeviceById(deviceId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateDevice(@RequestBody DICOMDevice dicomDevice){
        LOG.info("Updating the DICOM Device...");
        this.dicomMan.update(dicomDevice);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/start/{deviceId:.+}")
    public void startDeviceById (@PathVariable("deviceId") String deviceId) throws InterruptedException {
        System.out.println("#################**********************");
        LOG.info("Starting DICOM Device, getting the ID..."+deviceId);
        dicomMan.startDICOMDevice(deviceId);

    }
    @RequestMapping(method = RequestMethod.POST, value = "/stop/{deviceId:.+}")
    public void stopDeviceById (@PathVariable("deviceId") String deviceId) throws InterruptedException{
        LOG.info("Stoping DICOM Device...");
        dicomMan.stopDICOMDevice(deviceId);
    }

}
