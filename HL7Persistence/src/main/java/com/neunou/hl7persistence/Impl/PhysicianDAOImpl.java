package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.PhysicianDAO;
import com.neunou.schema.HL7Device;
import com.neunou.schema.Physician;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Masoud on 3/31/15.
 */
@Repository
public class PhysicianDAOImpl implements PhysicianDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String Physician_COL = "physician";

    @Override
    public String add(Object object) {
        Physician physician = (Physician) object;
        if(physician.getPhysicianId()==null){
            System.out.println("Adding the physician....");
            physician.setPhysicianId(UUID.randomUUID().toString());
            physician.setCreatedDate(new Date());
            if(!mongoTemplate.collectionExists(Physician_COL)){
                mongoTemplate.createCollection(Physician_COL);
                mongoTemplate.insert(physician,Physician_COL);
                return physician.getPhysicianId();
            }
            else {
                mongoTemplate.insert(physician,Physician_COL);
                System.out.println("Adding the physician...."+physician.getPhysicianId());
                return physician.getPhysicianId();
            }
        }
        else {
            this.update(object);
            return physician.getPhysicianId();
        }
    }

    @Override
    public void delete(Object object) {
        Physician physician = (Physician) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianId").is(physician.getPhysicianId()));

        mongoTemplate.remove(query,Physician.class,Physician_COL);
    }

    @Override
    public void update(Object object) {
        System.out.println("Updating the physician....");
        Physician physician = (Physician) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianId").is(physician.getPhysicianId()));

        physician.setModifiedDate(new Date());
        Update update = new Update();
        update.set("physicianNPI",physician.getPhysicianNPI());

        update.set("person.personInfo.description",physician.getPersonInfo().getDescription());
        update.set("person.personInfo.phoneNumber.cellPhoneNumber",physician.getPersonInfo().getPhoneNumber().getCellPhoneNumber());
        update.set("person.personInfo.phoneNumber.homePhoneNumber",physician.getPersonInfo().getPhoneNumber().getHomePhoneNumber());
        update.set("person.personInfo.phoneNumber.officePhoneNumber",physician.getPersonInfo().getPhoneNumber().getOfficePhoneNumber());
        update.set("person.personInfo.phoneNumber.faxNumber",physician.getPersonInfo().getPhoneNumber().getFaxNumber());
        update.set("person.personInfo.address.address1",physician.getPersonInfo().getAddress().getAddress1());
        update.set("person.personInfo.address.address2",physician.getPersonInfo().getAddress().getAddress2());
        update.set("person.personInfo.address.city",physician.getPersonInfo().getAddress().getCity());
        update.set("person.personInfo.address.state",physician.getPersonInfo().getAddress().getState());
        update.set("person.personInfo.address.country",physician.getPersonInfo().getAddress().getCountry());
        update.set("person.personInfo.address.zipCode",physician.getPersonInfo().getAddress().getZipCode());
        update.set("person.personInfo.personName.personFirstName",physician.getPersonInfo().getPersonName().getPersonFirstName());
        update.set("person.personInfo.personName.personMiddleName",physician.getPersonInfo().getPersonName().getPersonMiddleName());
        update.set("person.personInfo.personName.personLastName",physician.getPersonInfo().getPersonName().getPersonLastName());
        update.set("person.personInfo.personName.personFullName",physician.getPersonInfo().getPersonName().getPersonFullName());
        update.set("person.personInfo.emailAddress",physician.getPersonInfo().getEmailAddress());
        update.set("person.personInfo.uid",physician.getPersonInfo().getUid());
        update.set("person.personInfo.employeeNumber",physician.getPersonInfo().getEmployeeNumber());
        update.set("person.personInfo.ou",physician.getPersonInfo().getOu());
        update.set("person.personInfo.dc",physician.getPersonInfo().getDc());
        update.set("person.personInfo.dn",physician.getPersonInfo().getDn());
        update.set("person.personInfo.managerPersonId",physician.getPersonInfo().getManagerPersonId());
        update.set("person.personInfo.managerDN",physician.getPersonInfo().getManagerDN());
        update.set("person.personInfo.title",physician.getPersonInfo().getTitle());
        update.set("person.personInfo.distinguishedName",physician.getPersonInfo().getDistinguishedName());
        update.set("person.personInfo.displayName",physician.getPersonInfo().getDisplayName());
        update.set("person.personInfo.memberOf",physician.getPersonInfo().getMemberOf());
        update.set("person.personInfo.department",physician.getPersonInfo().getDepartment());
        update.set("person.personInfo.username",physician.getPersonInfo().getUsername());
        update.set("person.personInfo.pictureURL",physician.getPersonInfo().getPictureURL());

        update.set("person.roleList",physician.getRoleList());
        update.set("person.webLoginDates",physician.getWebLoginDates());
        update.set("person.mobileLoginDates",physician.getMobileLoginDates());

        physician.setModifiedDate(new Date());
        update.set("modifiedDate",physician.getModifiedDate());
        mongoTemplate.findAndModify(query,update,Physician.class,Physician_COL);
    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianId").is(id));
        return mongoTemplate.findOne(query,Physician.class,Physician_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(Physician.class,Physician_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,Physician.class,Physician_COL);
        return exists;
    }

    @Override
    public Physician getPhysicianByNPI(String npi) {
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianNPI").is(npi));
        return mongoTemplate.findOne(query,Physician.class,Physician_COL);
    }

    @Override
    public Physician getPhysicianByName(String fName, String lName) {
        Query query = new Query();
        query.addCriteria(Criteria.where("person.personInfo.personName.personFirstName").is(fName).and("person.personInfo.personName.personLastName").is(lName));

        return mongoTemplate.findOne(query,Physician.class,Physician_COL);
    }
}
