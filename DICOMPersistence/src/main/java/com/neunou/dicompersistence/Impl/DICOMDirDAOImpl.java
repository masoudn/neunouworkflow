package com.neunou.dicompersistence.Impl;

import com.neunou.dicompersistence.Interfaces.DICOMDirDAO;
import com.neunou.schema.DICOMDirInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by root on 3/7/16.
 */
@Repository
public class DICOMDirDAOImpl implements DICOMDirDAO{
    private static final Logger LOG = Logger.getLogger(DICOMDirDAOImpl.class.getName());
    @Autowired
    private MongoTemplate mongoTemplate;
    private String DICOM_DIR_COL = "dicomDirInfo";
    @Override
    public String add(Object object) {
        DICOMDirInfo DICOMDirInfo = (DICOMDirInfo) object;
        if (DICOMDirInfo.getDicomDirId()==null){
            DICOMDirInfo.setDicomDirId(UUID.randomUUID().toString());
            DICOMDirInfo.setCreatedDate(new Date());
            if (!mongoTemplate.collectionExists(DICOM_DIR_COL)){
                mongoTemplate.createCollection(DICOM_DIR_COL);
                mongoTemplate.insert(DICOMDirInfo,DICOM_DIR_COL);
                return DICOMDirInfo.getDicomDirId();
            }
            else {
                mongoTemplate.insert(DICOMDirInfo,DICOM_DIR_COL);
                return DICOMDirInfo.getDicomDirId();
            }
        }
        else {
            this.update(object);
            return DICOMDirInfo.getDicomDirId();
        }

    }

    @Override
    public void delete(Object object) {
        DICOMDirInfo DICOMDirInfo = (DICOMDirInfo) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDirId").is(DICOMDirInfo.getDicomDirId()));
        mongoTemplate.remove(query,DICOMDirInfo.class,DICOM_DIR_COL);
    }

    @Override
    public void update(Object object) {
        DICOMDirInfo DICOMDirInfo = (DICOMDirInfo) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDirId").is(DICOMDirInfo.getDicomDirId()));

        Map<String,Boolean> targetFromDB = DICOMDirInfo.getTargetServer();
        targetFromDB.putAll(DICOMDirInfo.getTargetServer());
        DICOMDirInfo.setModifiedDate(new Date());
        Update update = new Update();

        update.set("filePath", DICOMDirInfo.getFilePath());
        update.set("modifiedDate", DICOMDirInfo.getModifiedDate());
        update.set("targetServer", DICOMDirInfo.getTargetServer());

        mongoTemplate.findAndModify(query,update,DICOMDirInfo.class,DICOM_DIR_COL);
    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDirId").is(id));
        return mongoTemplate.findOne(query,DICOMDirInfo.class,DICOM_DIR_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(DICOMDirInfo.class,DICOM_DIR_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,DICOMDirInfo.class,DICOM_DIR_COL);
        return exists;
    }

    @Override
    public void deleteDicomDirById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("dicomDirId").is(id));
        mongoTemplate.remove(query,DICOMDirInfo.class,DICOM_DIR_COL);
    }
}
