package com.neunou.hl7manager.interfaces;

import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import org.springframework.stereotype.Service;

/**
 * Created by root on 4/9/15.
 */
@Service
public interface HL7Services {
    void StartHL7Service(String hl7DeviceId);

    void StartAllHl7Services();

    void StopHL7Service(String hl7DeviceId);

    void StopAllHL7Services();

    void ParsMessage(HapiContext ctx, Message message);

    Boolean CheckRunningState(String hl7DeviceId);

}
