package com.neunou.wi.API;

import com.neunou.dicommanager.Interfaces.DICOMDirManager;
import com.neunou.schema.DICOMDirInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 3/16/16.
 */
@Controller
@RequestMapping(value="/api/dicomDirInfo")
public class DicomDirInfoAPI {
    private static final Logger LOG = Logger.getLogger(DicomDirInfoAPI.class.getName());
    @Autowired
    private DICOMDirManager dirManager;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DICOMDirInfo> getAllDicomDirInfo(){
        System.out.println("____________________________________________");

        return dirManager.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public DICOMDirInfo saveDir(@RequestBody DICOMDirInfo dicomDirInfo){
        System.out.println("____________________________________________");
        LOG.info("Adding new DICOMDir...");
        dirManager.add(dicomDirInfo);

        return dicomDirInfo;
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{dicomDirId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DICOMDirInfo getDirById(@PathVariable("dicomDirId") String dicomDirId){
        return (DICOMDirInfo)dirManager.getById(dicomDirId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{dicomDirId:.+}")
    public void deleteDirById (@PathVariable("dicomDirId") String dicomDirId){
        dirManager.deleteDicomDirById(dicomDirId);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateDevice(@RequestBody DICOMDirInfo dicomDirInfo){
        LOG.info("Updating the DICOM Directory...");
        this.dirManager.update(dicomDirInfo);
    }
}
