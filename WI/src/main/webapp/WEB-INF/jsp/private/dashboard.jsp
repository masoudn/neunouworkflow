<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 2/12/15
  Time: 10:32 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html ng-app="NeuNouApp">
<div >

    <c:forEach var="role" items="${roleList}">
        <%--<li><c:out value="${role.role}" /></li>--%>
        <c:if test="${role.role=='ROLE_ADMIN'}">
            <h4> Welcome Admin Youuuu3...!!!</h4>
        </c:if>

        <c:if test="${role.role=='ROLE_RAD'}">
            <div class="container-fluid main-content">

                <div class="row" ng-controller="timeLogController">
                    <div class="col-lg-6">
                        <div class="widget-container fluid-height">
                            <div class="heading tabs">
                                <i class="fa fa-sitemap"></i>Rad Time Log
                                <ul class="nav nav-tabs pull-right" data-tabs="tabs" id="tabs">
                                    <li class="active">
                                        <a data-toggle="tab" href="#tab1"><i class="fa fa-magic"></i><span>Auto</span></a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab2"><i class="fa fa-pencil-square-o"></i><span>Manual</span></a>
                                    </li>
                                    <li>
                                        <a data-toggle="tab" href="#tab3"><i class="fa fa-tachometer"></i><span>Stats</span></a>
                                    </li>
                                </ul>
                            </div>

                            <%--Tab One--%>
                            <div class="tab-content padded" id="my-tab-content">
                                <div class="tab-pane active" id="tab1" data-ng-init="getPending()">

                                    <div class="alert alert-success animate-show" ng-show="successNotification.show">
                                        <button class="close" data-dismiss="alert" type="button">&times;</button>{{successMessage}}
                                    </div>

                                    <div>
                                        <h3>
                                            Current Time: <span my-current-time='format'></span>
                                        </h3>

                                        <ng-switch  on="pendingTimeLog" >
                                            <div ng-switch-when="true">
                                                <div class="alert alert-info" ng-show="infoNotification.show">
                                                    <button class="close" data-dismiss="alert" type="button">&times;</button>You clocked in at {{timeLog.startTime | date:'medium'}}
                                                </div>
                                                <h4>Notes: {{timeLog.notes}}</h4>
                                                <p >
                                                    <button type="submit"  ng-click="clockout(timeLog.timeLogId)" class="btn btn-primary">End</button>
                                                </p>

                                            </div>

                                            <div ng-switch-when="false">
                                                <div class="pager form-group">
                                                    <button class="btn btn-info" ng-click="showForm('Long List')">Long List</button>
                                                    <span></span>
                                                    <button class="btn btn-info" ng-click="showForm('Angio Work')">Angio Work</button>
                                                    <span></span>
                                                    <button class="btn btn-info" ng-click="showForm('Called In')">Called In</button>
                                                </div>

                                                    <div class="animate-show fluid-height clearfix" ng-show="showFormBool.show">
                                                        <label class="col-md-2 form-group">Notes</label>
                                                        <div class="col-md-6 form-group">
                                                            <input class="form-control" type="text" ng-model="timeLog.notes">
                                                        </div>
                                                        <div class="form-group" >
                                                            <button type="submit"  class="btn btn-primary form-group" ng-click="clockin(timeLog.notes)">Start</button>
                                                        </div>

                                                    </div>

                                            </div>

                                        </ng-switch>
                                        <a  data-toggle="modal" href="#historyModal" ng-click="getAllTimeLogs()">History</a>
                                    </div>

                                </div>
                                <%--End Of Tab One--%>

                                <%--Tab Two--%>
                                <div class="tab-pane" id="tab2">
                                    <div class="alert alert-success animate-show" ng-show="successNotification.show">
                                        <button class="close" data-dismiss="alert" type="button">&times;</button>{{successMessage}}
                                    </div>

                                    <p class="pager form-group">
                                        <button class="btn btn-info" ng-click="showFormManual('Long List')">Long List</button>
                                        <span></span>
                                        <button class="btn btn-info" ng-click="showFormManual('Angio Work')">Angio Work</button>
                                        <span></span>
                                        <button class="btn btn-info" ng-click="showFormManual('Called In')">Called In</button>
                                    </p>

                                    <div class="animate-show form-group" ng-show="showFormManualBool.show">

                                        <div class=" fluid-height clearfix">
                                            <label class="control-label col-md-3">Clock In Time/Date</label>
                                            <div class="col-sm-5  input-group date datepicker" data-date-autoclose="true" data-date-format="dd-mm-yyyy">
                                                <input class="form-control" type="text" ng-model="manual.startDate"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></input>
                                            </div>
                                            <div class="col-sm-4 input-group bootstrap-timepicker">
                                                <input class="form-control" id="timepicker-24h" type="text" ng-change="changeEndTime(manual.startTime)" ng-model="manual.startTime"><span class="input-group-addon"><i class="fa fa-clock-o"></i></span></input>
                                            </div>
                                        </div>

                                        <div class="form-group fluid-height clearfix">
                                            <label class="control-label col-md-3">Clock Out Time/Date</label>

                                            <div class="col-sm-4">
                                                <button type="submit"  ng-click="calculateElapsedHour(manual.startTime)" class="btn btn-primary">+1H</button>
                                                <button type="submit"  ng-click="calculateElapsedMinute(manual.startTime)" class="btn btn-primary">+15 Min</button>
                                            </div>

                                        </div>

                                        <div class="form-group fluid-height clearfix">
                                            <label class="control-label col-md-3">Notes</label>
                                            <div class="col-md-9">
                                                <input class="form-control" type="text" ng-model="manualTime.notes">
                                            </div>
                                        </div>
                                        <p >
                                            <button type="submit"  class="btn btn-primary pull-right" ng-click="manualClock()">Submit</button>
                                        </p>

                                    </div>

                                    <a data-toggle="modal" data-target="#historyModal" ng-click="getAllTimeLogs()">History</a>

                                </div>
                                <%--End of Tab Two--%>


                                <%--Tab Three--%>
                                <div class="tab-pane" id="tab3">
                                    <div class=" fluid-height clearfix">
                                        <label class="control-label col-md-3">Stat Period</label>
                                        <div class="col-sm-5  input-group date datepicker" data-date-autoclose="true" data-date-format="dd-mm-yyyy">
                                            <input class="form-control" type="text" ng-model="dateRange.startDateRange"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></input>
                                        </div>
                                        <div class="col-sm-5  input-group date datepicker" data-date-autoclose="true" data-date-format="dd-mm-yyyy">
                                            <input class="form-control" type="text" ng-model="dateRange.endDateRange"><span class="input-group-addon"><i class="fa fa-calendar"></i></span></input>
                                        </div>
                                    </div>

                                    <div class=" fluid-height clearfix">
                                        <button type="submit"  class="btn btn-info pull-right" ng-click="getStats(dateRange)" href="/indexPage.htm" data-toggle="modal"  data-target="#graphModal">Get Stat</button>
                                    </div>
                                    <a data-toggle="modal" href="/indexPage.htm" data-target="#graphModal" >Click me !</a>


                                </div>
                                <%--End of Tab Three--%>

                                <div class="modal fade" id="historyModal">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button aria-hidden="true" class="close" data-dismiss="modal" type="button">&times;</button>
                                                <h4 class="modal-title">
                                                    Time Log History
                                                </h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="widget-content padded clearfix">
                                                    <table class="table">
                                                        <thead>
                                                        <th>
                                                            Note
                                                        </th>
                                                        <th>
                                                            Clock In
                                                        </th>
                                                        <th>
                                                            Clock Out
                                                        </th>
                                                        </thead>
                                                        <tbody>
                                                        <tr ng-repeat="log in timeLogs">
                                                            <td>
                                                                {{log.notes}}
                                                            </td>
                                                            <td>
                                                                {{log.startTime | date:'medium'}}
                                                            </td>
                                                            <td>
                                                                {{log.endTime | date:'medium'}}
                                                            </td>

                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-default-outline" data-dismiss="modal" type="button">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Modal -->
                                <div class="modal fade" id="graphModal" >
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">Modal title</h4>

                                            </div>
                                            <div class="modal-body"><div class="te"></div></div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                <button type="button" class="btn btn-primary">Save changes</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->
                                </div>
                                <!-- /.modal -->

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </c:if>
    </c:forEach>
</div>
</html>