<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 3/18/15
  Time: 4:31 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    var app = angular.module("NeuNouApp",["ngAnimate",'ngRoute']);
    app.service("dataService",function($http){
        this.queryData = function(apiURL){
            return $http({
                method : 'GET',
                url: apiURL,
                headers: {'Content-Type': 'application/json'}
            })
                    .then(function(result){
                        console.log(result);
                        return result.data;

                    });
        }
        this.saveData = function(apiURL,element){
            console.log("saving the data ...");

            return $http({
                method: 'POST',
                url: apiURL,
                data: element,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result) {
                console.log(result);
            }, function(error) {
                console.log(error);
            });

        }
        this.deleteData = function(apiURL,element){
            return $http({
                method: 'DELETE',
                url: apiURL,
                data: element,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result){
                console.log("Result: "+result);
            }, function(error){
                console.log("Error: "+error);
            });
        }
        this.saveGroupsToDB = function(apiURL){
            return $http({
                method: 'GET',
                url: apiURL,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result) {
                console.log(result);
            }, function(error) {
                console.log(error);
            });
        }
    });

    app.controller("groupRolesController",function(dataService,$scope){
        $scope.groupMaster = {};
        $scope.groupState = {show: false};

        $scope.showGroups = function(){
            $scope.groupState.show = !$scope.groupState.show;
        };
        $scope.getGroupsData = function(){
            dataService.queryData('/api/groups/getAll').then(function(dataResponse){
                $scope.groups = dataResponse;
                console.log(dataResponse);
                if(dataResponse.length >0){
                    $scope.groupsListBool = true;
                }
                else{
                    $scope.groupsListBool = false;
                }
            })
        }
        $scope.getGroupsData();
        $scope.addGroup = function(groupEl){
            console.log(groupEl);
            $scope.ldapMaster = angular.copy(groupEl);
            dataService.saveData("/api/groups/save/",groupEl).then(function(dataResponse){
                $scope.showGroups();
                $scope.getGroupsData();
            });
        }
        $scope.reset = function(){
            $scope.groupEl = angular.copy($scope.groupMaster);
        }
        $scope.editGroup = function(index){
            console.log("Index is..."+index);
            $scope.groupEl= $scope.groups[index];
            $scope.showGroups();
        }

        $scope.deleteGroup = function(index){
            console.log("Index is..."+index);
            $scope.groupToDelete = $scope.groups[index];
            dataService.deleteData('/api/groups/delete/'+$scope.groupToDelete.groupId,$scope.groupToDelete.groupId).then(function(){
                $scope.getGroupsData();
            });
        }

        $scope.getAllGroups = function(){
            console.log("Getting al groups from LDAP...");
            dataService.saveGroupsToDB('/api/groups/getAllGroups').then(function(){
                $scope.getGroupsData();
            });


        }

        $scope.getGroupById = function(index){
            console.log($scope.groupState.show);
            if ($scope.groupState.show = false){
                $scope.groupState.show = true
            }
            else{
                $scope.groupState.show = !$scope.groupState.show;
            }
//
            console.log("Index is: "+index);
            $scope.groupToShow = $scope.groups[index];
            dataService.queryData('/api/groups/get/'+$scope.groupToShow.groupId).then(function(dataResponse){
                $scope.selectedGroup = dataResponse;
                console.log(dataResponse);
            })
        }

        $scope.excludeGroup = function(index){
            console.log("deactivateing ..."+index);
            $scope.groupToExclude = $scope.groups[index];
            console.log("This group is getting excluded... "+$scope.groupToExclude.groupName);
            console.log("This group is getting excluded... "+$scope.groupToExclude.excluded);
            $scope.groupToExclude.excluded=true;
            console.log("This group is getting excluded... "+$scope.groupToExclude.excluded);
            dataService.saveData("/api/groups/save/",$scope.groupToExclude).then(function(){
                $scope.getGroupsData();
            });

        }




    });
</script>
