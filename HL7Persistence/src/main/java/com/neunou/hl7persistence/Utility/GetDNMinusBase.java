package com.neunou.hl7persistence.Utility;

import org.springframework.ldap.core.LdapTemplate;

import javax.naming.NamingException;

/**
 * Created by Masoud on 2/17/15.
 */
public class GetDNMinusBase {
    private LdapTemplate ldapTemplate;
    private String dn;

    public GetDNMinusBase(LdapTemplate ldapTemplate, String dn) {
        this.ldapTemplate = ldapTemplate;
        this.dn = dn;
    }

    public String getDNWithNoBase() throws NamingException {
        String DNMinusBase;
        DNMinusBase = this.dn.toLowerCase().replaceAll(getBase(),"");
        return DNMinusBase;
    }

    private String getBase() throws NamingException {
        String base = ","+this.ldapTemplate.getContextSource().getReadOnlyContext().getNameInNamespace().toLowerCase().toLowerCase();
        return base;
    }
}
