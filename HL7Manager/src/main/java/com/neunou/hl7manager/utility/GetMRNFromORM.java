package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.message.ORM_O01;

/**
 * Created by root on 2/25/16.
 */
public class GetMRNFromORM {
    private ORM_O01 orm;
    private String mrn;

    public GetMRNFromORM(ORM_O01 orm) {
        this.setOrm(orm);
    }

    public String mrnFactory(){
        String mrn;
        mrn = getOrm().getPATIENT().getPID().getPatientID().getCx1_IDNumber().toString();

        return mrn;
    }

    public ORM_O01 getOrm() {
        return orm;
    }

    public void setOrm(ORM_O01 orm) {
        this.orm = orm;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }
}
