package com.neunou.schema;

/**
 * Created by Masoud on 9/16/14.
 */

//TODO implement this class
public class Insurance {
    private String insuranceId;
    private String insurancePlanId;
    private String insuranceCompanyId;
    private String insuranceCompanyIdCheckDigit;
    private String insuranceCompanyName;
    private Address insuranceCompanyAddress;
    private PhoneNumber insurancePhoneNumber;
    private String groupNumber;
    private String groupName;
    private String authInformation;
    private String planType;
    private Person insuredInfo;
    private String insuredRelationToPatient;
    private String policyNumber;
    private String insuredEmployeeStatus;
    private Boolean primaryInsurance;
    private Boolean secondaryInsurance;
    private Boolean tertiaryInsurance;

    public String getInsuranceId() {
        return insuranceId;
    }

    public void setInsuranceId(String insuranceId) {
        this.insuranceId = insuranceId;
    }

    public String getInsurancePlanId() {
        return insurancePlanId;
    }

    public void setInsurancePlanId(String insurancePlanId) {
        this.insurancePlanId = insurancePlanId;
    }

    public String getInsuranceCompanyId() {
        return insuranceCompanyId;
    }

    public void setInsuranceCompanyId(String insuranceCompanyId) {
        this.insuranceCompanyId = insuranceCompanyId;
    }

    public String getInsuranceCompanyName() {
        return insuranceCompanyName;
    }

    public void setInsuranceCompanyName(String insuranceCompanyName) {
        this.insuranceCompanyName = insuranceCompanyName;
    }

    public Address getInsuranceCompanyAddress() {
        return insuranceCompanyAddress;
    }

    public String getInsuranceCompanyIdCheckDigit() {
        return insuranceCompanyIdCheckDigit;
    }

    public void setInsuranceCompanyIdCheckDigit(String insuranceCompanyIdCheckDigit) {
        this.insuranceCompanyIdCheckDigit = insuranceCompanyIdCheckDigit;
    }

    public void setInsuranceCompanyAddress(Address insuranceCompanyAddress) {
        this.insuranceCompanyAddress = insuranceCompanyAddress;
    }

    public PhoneNumber getInsurancePhoneNumber() {
        return insurancePhoneNumber;
    }

    public void setInsurancePhoneNumber(PhoneNumber insurancePhoneNumber) {
        this.insurancePhoneNumber = insurancePhoneNumber;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getAuthInformation() {
        return authInformation;
    }

    public void setAuthInformation(String authInformation) {
        this.authInformation = authInformation;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public Person getInsuredInfo() {
        return insuredInfo;
    }

    public void setInsuredInfo(Person insuredInfo) {
        this.insuredInfo = insuredInfo;
    }

    public String getInsuredRelationToPatient() {
        return insuredRelationToPatient;
    }

    public void setInsuredRelationToPatient(String insuredRelationToPatient) {
        this.insuredRelationToPatient = insuredRelationToPatient;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getInsuredEmployeeStatus() {
        return insuredEmployeeStatus;
    }

    public void setInsuredEmployeeStatus(String insuredEmployeeStatus) {
        this.insuredEmployeeStatus = insuredEmployeeStatus;
    }

    public Boolean getPrimaryInsurance() {
        return primaryInsurance;
    }

    public void setPrimaryInsurance(Boolean primaryInsurance) {
        this.primaryInsurance = primaryInsurance;
    }

    public Boolean getSecondaryInsurance() {
        return secondaryInsurance;
    }

    public void setSecondaryInsurance(Boolean secondaryInsurance) {
        this.secondaryInsurance = secondaryInsurance;
    }

    public Boolean getTertiaryInsurance() {
        return tertiaryInsurance;
    }

    public void setTertiaryInsurance(Boolean tertiaryInsurance) {
        this.tertiaryInsurance = tertiaryInsurance;
    }
}
