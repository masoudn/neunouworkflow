package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.ChargeActiveRecordsManager;
import com.neunou.schema.ChargeActiveRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 5/7/15.
 */
@Controller
@RequestMapping(value="/api/chargeActive")
public class ChargeActiveRecordsAPI {
    private static final Logger LOG = Logger.getLogger(ChargeActiveRecordsAPI.class.getName());
    @Autowired
    private ChargeActiveRecordsManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ChargeActiveRecords> getAll(){
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String saveBillingTransaction(@RequestBody ChargeActiveRecords chargeActiveRecords){
        LOG.info("*********Saving to ChargeActive...");
        man.add(chargeActiveRecords);
        return "dashboard";
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{chargeActiveRecordsId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ChargeActiveRecords getChargeActiveById(@PathVariable("chargeActiveRecordsId") String chargeActiveRecordsId){
        return (ChargeActiveRecords)man.getById(chargeActiveRecordsId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{chargeActiveRecordsId:.+}")
    public void deleteChargeActiveById (@PathVariable("chargeActiveRecordsId") String chargeActiveRecordsId){
        man.delete(man.getById(chargeActiveRecordsId));
    }
    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateChargeActive(@RequestBody ChargeActiveRecords chargeActiveRecords){
        this.man.update(chargeActiveRecords);
    }
}
