package com.neunou.hl7persistence.Impl;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;

import com.neunou.hl7persistence.Utility.GetDNMinusBase;
import com.neunou.schema.Person;
import com.neunou.schema.PersonInfo;
import com.neunou.schema.PersonName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.ldap.core.AttributesMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.stereotype.Component;

import static org.springframework.ldap.query.LdapQueryBuilder.query;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Masoud on 2/17/15.
 */
@Component
public class LDAPUserImpl {
    @Resource(name="ldapTemplate")
    private LdapTemplate tmpl;

    public Person getPersonByDN(String dn) throws NamingException {
        Person person;
        GetDNMinusBase dnWithNoBase = new GetDNMinusBase(tmpl,dn);
        person = tmpl.lookup(dnWithNoBase.getDNWithNoBase(),new PersonAttributesMapper());
        return person;
    }

    public List<String> getAllGroups(){
        return tmpl.search(query().where("objectclass").is("group"),new AttributesMapper<String>() {
            @Override
            public String mapFromAttributes(Attributes attributes) throws NamingException {
                return attributes.get("cn").get().toString();
            }
        });
    }


    private class PersonAttributesMapper implements AttributesMapper<Person> {
        public Person mapFromAttributes(Attributes attrs) throws NamingException {
            ArrayList<String> ouList = new ArrayList<String>();
            Person person = new Person();
            PersonInfo personInfo = new PersonInfo();
            PersonName personName = new PersonName();


            personName.setPersonFullName((String) attrs.get("cn").get());

            if ((nullChecker(attrs,"sn"))) {
                personName.setPersonLastName((String) attrs.get("sn").get());
            }
            if((nullChecker(attrs,"givenName"))){
                personName.setPersonFirstName((String) attrs.get("givenName").get());
            }
            if((nullChecker(attrs,"middleName"))){
                personName.setPersonMiddleName((String) attrs.get("middleName").get());
            }
            if((nullChecker(attrs,"mail"))){
                personInfo.setEmailAddress((String) attrs.get("mail").get());
            }
            if((nullChecker(attrs,"employeeID"))){
                personInfo.setEmployeeNumber((String) attrs.get("employeeID").get());
            }

            if((nullChecker(attrs,"ou"))){
                System.out.println("OU is: "+attrs.get("ou").get());
                ouList.add((String) attrs.get("ou").get());
                personInfo.setOu(ouList);
            }
            if((nullChecker(attrs,"title"))){
                personInfo.setTitle((String) attrs.get("title").get());
            }
            if((nullChecker(attrs,"description"))){
                personInfo.setDescription((String) attrs.get("description").get());
            }

            if((nullChecker(attrs,"department"))){
                personInfo.setDepartment((String)attrs.get("department").get());
            }

            if((nullChecker(attrs,"distinguishedName"))){
                personInfo.setDistinguishedName((String)attrs.get("distinguishedName").get());
            }

            if((nullChecker(attrs,"memberOf"))){
                List<String> membersList = new ArrayList<>();
                NamingEnumeration<?> membersOf= attrs.get("memberOf").getAll();
                while(membersOf.hasMore()){
                    membersList.add((String) membersOf.next());
                }
                personInfo.setMemberOf(membersList);
            }
            if((nullChecker(attrs,"displayName"))){
                personInfo.setDisplayName((String)attrs.get("displayName").get());
            }
            if((nullChecker(attrs,"manager"))){
                personInfo.setManagerDN((String) attrs.get("manager").get());
            }
            if((nullChecker(attrs,"distinguishedName"))){
                personInfo.setDn((String)attrs.get("distinguishedName").get());
            }
            if((nullChecker(attrs,"sAMAccountName"))){
                personInfo.setUsername((String)attrs.get("sAMAccountName").get());
            }

            personInfo.setPersonName(personName);
//            personInfo.setManagerPersonId(getManager(personInfo.getDistinguishedName()).getPersonId());

            person.setPersonInfo(personInfo);

            return person;
        }

        public Boolean nullChecker(Attributes attrs, String element){
            if(attrs.get(element)!=null){
                return true;
            }
            else{
                return false;
            }
        }
    }
}
