package com.neunou.hl7manager.Impl;

import com.neunou.hl7manager.interfaces.HL7MessageManager;
import com.neunou.hl7persistence.Interfaces.HL7MessageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by root on 4/15/15.
 */
@Component
public class HL7MessageManagerImpl implements HL7MessageManager {
    @Autowired
    private HL7MessageDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }
}
