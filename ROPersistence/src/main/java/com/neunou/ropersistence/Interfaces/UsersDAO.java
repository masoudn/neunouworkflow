package com.neunou.ropersistence.Interfaces;

import com.neunou.schema.Person;
import org.springframework.stereotype.Service;

/**
 * Created by root on 6/12/15.
 */
@Service
public interface UsersDAO extends GenericInterface {
    Person getByUsername(String username);
}
