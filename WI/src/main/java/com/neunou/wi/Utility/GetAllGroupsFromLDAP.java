package com.neunou.wi.Utility;

import com.neunou.hl7manager.interfaces.GroupsManager;
import com.neunou.hl7persistence.Impl.LDAPUserImpl;
import com.neunou.schema.Groups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Masoud on 3/20/15.
 */
@Service
public class GetAllGroupsFromLDAP {
    @Autowired
    private LDAPUserImpl ldapUser;
    @Autowired
    private GroupsManager man;

    public List<String> getAllGroupsFromLDAP(){
        List<String> groups = ldapUser.getAllGroups();


        return groups;
    }

    public void saveGroupsToDB(){
        for (String group:getAllGroupsFromLDAP()){
            Groups groupSchema = new Groups();
            groupSchema.setCompanyName("Mountain Medical");
            groupSchema.setGroupName(group);
            man.add(groupSchema);
        }
    }

}
