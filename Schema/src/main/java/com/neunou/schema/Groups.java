package com.neunou.schema;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Masoud on 3/3/15.
 */
public class Groups implements Comparable<Groups>{
    private String groupId;
    private String groupName;
    private String groupDesc;
    private String companyName;
    private String department;
    private List<Role> roles;
    private Boolean excluded;
    private Date modifiedDate;
    private Date creationDate;


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupDesc() {
        return groupDesc;
    }

    public void setGroupDesc(String groupDesc) {
        this.groupDesc = groupDesc;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }
    public Boolean getExcluded() {
        return excluded;
    }

    public void setExcluded(Boolean excluded) {
        this.excluded = excluded;
    }

    public static Comparator<Groups> GroupsNameComparator =new Comparator<Groups>() {
        @Override
        public int compare(Groups group1, Groups group2) {
            String group1Name = group1.getGroupName().toLowerCase();
            String group2Name = group2.getGroupName().toLowerCase();

            return group1Name.compareTo(group2Name);
        }
    };

    @Override
    public int compareTo(Groups o) {
        return 0;
    }


}
