package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.PhoneCallRecordsManager;
import com.neunou.schema.PhoneCallRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 3/17/15.
 */
@Controller
@RequestMapping(value="/api/phoneCallRecords")
public class PhoneCallRecordsAPI {
    private static final Logger LOG = Logger.getLogger(PhoneCallRecordsAPI.class.getName());
    @Autowired
    private PhoneCallRecordsManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PhoneCallRecords> getAllPhoneCalls(){

        return man.getAll();
    }


    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String savePhoneCallRecord(@RequestBody PhoneCallRecords phoneCallRecords){
        System.out.println("************************** id is:"+phoneCallRecords.getRecordId());
        System.out.println("Adding the PhoneCall....");
        LOG.info("Adding new PhoneCall...");
        man.add(phoneCallRecords);
        return "dashboard";
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{callId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhoneCallRecords getCompanyById(@PathVariable("callId") String deviceId){
        return (PhoneCallRecords)man.getById(deviceId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{callId:.+}")
    public void deleteCompanyById (@PathVariable("callId") String callId){
        LOG.info("Deleting CallRecord...");
        man.delete(man.getById(callId));
    }
}
