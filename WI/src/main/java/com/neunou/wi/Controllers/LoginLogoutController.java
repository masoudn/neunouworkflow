package com.neunou.wi.Controllers;

import com.neunou.wi.Utility.GetAuthPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.logging.Logger;

/**
 * Created by Masoud on 2/12/15.
 */
@Controller
public class LoginLogoutController {
    @Autowired
    private GetAuthPerson getAuthPerson;
    private static final Logger LOG = Logger.getLogger(LoginLogoutController.class.getName());
//    @RequestMapping(value="login.htm",method= RequestMethod.GET)
//    public String loadLoginPage(Model model){
//        return "login";
//    }


    @RequestMapping(value="login", method=RequestMethod.GET)
    public String getLoginPage(@RequestParam(value="error", required=false) boolean error,
                               ModelMap model){
        LOG.info("Received request to show login page");

        // Add an error message to the model if login is unsuccessful
        // The 'error' parameter is set to true based on the when the authentication has failed.
        // We declared this under the authentication-failure-url attribute inside the spring-security.xml
        /* See below:
         <form-login
                        login-page="/krams/auth/login"
                        authentication-failure-url="/krams/auth/login?error=true"
                        default-target-url="/krams/main/common"/>
         */
        if (error == true) {
            // Assign an error message
            model.put("error", "You have entered an invalid username or password!");
        } else {
            model.put("error", "");
        }

        return "login";
    }

    @RequestMapping(value = "/deniedAccess.htm", method = RequestMethod.GET)
    public String getDeniedPage() {
        LOG.info("Received request to show denied page");

        // This will resolve to /WEB-INF/jsp/deniedpage.jsp
        return "deniedAccess";
    }

    @RequestMapping(value="logout.htm",method = RequestMethod.GET)
    public String getLogout(){
        getAuthPerson.setPerson(null);
        return "logout";
    }

    @RequestMapping(value="loginerror.htm", method=RequestMethod.GET)
    private String getLoginErrorPage(){

        return "loginerror";
    }

}
