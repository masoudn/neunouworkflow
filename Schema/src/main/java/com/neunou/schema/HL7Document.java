package com.neunou.schema;

import java.util.Date;
import ca.uhn.hl7v2.model.Message;

/**
 * Created by Masoud on 9/12/14.
 */
public class HL7Document {
    private String hL7DocumentId;
    private HL7MessageTypes hl7MessageTypes;
    private String patientId;
    private String referringPhysicianId;
    private String providerId;
    private String hl7MessageContent;
    private String printStructure;
    private Date hl7Date;
    private Date creationDate;
    private Date modifiedDate;


    public String gethL7DocumentId() {
        return hL7DocumentId;
    }

    public void sethL7DocumentId(String hL7DocumentId) {
        this.hL7DocumentId = hL7DocumentId;
    }

    public HL7MessageTypes getHl7MessageTypes() {
        return hl7MessageTypes;
    }

    public void setHl7MessageTypes(HL7MessageTypes hl7MessageTypes) {
        this.hl7MessageTypes = hl7MessageTypes;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getReferringPhysicianId() {
        return referringPhysicianId;
    }

    public void setReferringPhysicianId(String referringPhysicianId) {
        this.referringPhysicianId = referringPhysicianId;
    }

    public String getProviderId() {
        return providerId;
    }

    public void setProviderId(String providerId) {
        this.providerId = providerId;
    }

    public String getHl7MessageContent() {
        return hl7MessageContent;
    }

    public void setHl7MessageContent(String hl7MessageContent) {
        this.hl7MessageContent = hl7MessageContent;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getPrintStructure() {
        return printStructure;
    }

    public void setPrintStructure(String printStructure) {
        this.printStructure = printStructure;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Date getHl7Date() {
        return hl7Date;
    }

    public void setHl7Date(Date hl7Date) {
        this.hl7Date = hl7Date;
    }
}
