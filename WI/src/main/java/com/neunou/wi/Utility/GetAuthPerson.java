package com.neunou.wi.Utility;

import com.neunou.hl7persistence.Impl.LDAPUserImpl;
import com.neunou.romanager.Interfaces.UsersManager;
import com.neunou.schema.Person;
import com.neunou.schema.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.stereotype.Service;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 2/26/15.
 */
@Service
public class GetAuthPerson {
    @Autowired
    private LDAPUserImpl ldapUser;
    @Autowired
    private UsersManager userMan;
    private Person person;
    private List<String> dc;
    private static final Logger LOG = Logger.getLogger(GetAuthPerson.class.getName());
    private Boolean admin = Boolean.FALSE;

    /**
     * @return the person
     */
    public Person getPersonFromLDAP() throws NamingException {
        this.person = new Person();
        LdapUserDetailsImpl loggedinUser = (LdapUserDetailsImpl) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Role> rolesSchema = new ArrayList<Role>();

        setPerson(ldapUser.getPersonByDN(loggedinUser.getDn()));
        getPerson().getPersonInfo().getPersonName().setPersonFullName(getFullName(loggedinUser.getDn()));
        getPerson().getPersonInfo().setDc(getDc());

        Collection<? extends GrantedAuthority> roles = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority role:roles){
            Role roleSchema = new Role();
            roleSchema.setRole(role.toString());
            rolesSchema.add(roleSchema);
        }
        getPerson().setRoleList(rolesSchema);
        System.out.println("Role List has: "+getPerson().getRoleList().size());

        return person;
    }

    /**
     * @param person the person to set
     */
    public void setPerson(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }

    public String getFullName(String dn) {
        String fullName = null;
        String[] dnElements = dn.split(",");
        List<String> dcList = new ArrayList<String>();
        for (String element : dnElements) {
            if (element.contains("cn")) {
                fullName = element.substring(element.indexOf("=") + 1);
            }
            if (element.contains("dc")) {
                dcList.add(element.substring(element.indexOf("=") + 1));
            }
        }
        setDc(dcList);
        return fullName;
    }

    public List<String> getDc() {
        return dc;
    }

    public void setDc(List<String> dc) {

        this.dc = dc;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

    public String getPersonIdFromDB(){
        String id;
        if (userMan.getByUsername(this.person.getPersonInfo().getUsername())!=null){
            id = userMan.getByUsername(this.person.getPersonInfo().getUsername()).getPersonId();
            return id;
        }
        else {
            LOG.info("Person does not exist in the DB");
            return  null;
        }

    }

}
