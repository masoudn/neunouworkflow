package com.neunou.schema;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

/**
 * Created by Masoud on 10/12/15.
 */
public class CashTransaction {
    private String cashTransactionId;
    private Patient patient;
    private Map<Date,Person> modifyingPersonMap;
    private BigDecimal amount;
    private Boolean posted;
    private Boolean verified;


    public String getCashTransactionId() {
        return cashTransactionId;
    }

    public void setCashTransactionId(String cashTransactionId) {
        this.cashTransactionId = cashTransactionId;
    }

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }

    public Map<Date, Person> getModifyingPersonMap() {
        return modifyingPersonMap;
    }

    public void setModifyingPersonMap(Map<Date, Person> modifyinPersonMap) {
        this.modifyingPersonMap = modifyinPersonMap;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Boolean getPosted() {
        return posted;
    }

    public void setPosted(Boolean posted) {
        this.posted = posted;
    }

    public Boolean getVerified() {
        return verified;
    }

    public void setVerified(Boolean verified) {
        this.verified = verified;
    }
}
