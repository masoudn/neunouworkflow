package com.neunou.wi.Controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by root on 7/28/15.
 */
@Controller
public class IndexController {
    @RequestMapping(value="indexPage.htm",method= RequestMethod.GET)
    public String getIndexPage(){
        System.out.println("****************Index Controller*******************");

        return "indexPage";
    }
}
