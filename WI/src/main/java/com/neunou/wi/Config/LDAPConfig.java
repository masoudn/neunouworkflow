package com.neunou.wi.Config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.LdapContextSource;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Masoud on 2/16/15.
 */
@Configuration
public class LDAPConfig {
    private Map<String,String> contextList = new HashMap<String,String>();

    @Bean
    public LdapTemplate ldapTemplate(){
        LdapContextSource lctx = new LdapContextSource();
//        String[] urls = new String[2];
//        urls[0]="ldap://50.200.165.70:389";
//        urls[1]="ldap://10.1.13.50:389";

//        lctx.setUrls(urls);
//        lctx.setUrl("ldap://10.1.13.50:389");
        lctx.setUrl("ldap://50.200.165.70:389");
//        lctx.setUrl("ldap://10.1.13.50:389 ldap://50.200.165.70:389");
        lctx.setUserDn("cn=ldapauth,ou=ServiceAccounts,ou=MMPS,dc=mmic,dc=local");
        lctx.setBase("ou=mmps,dc=mmic,dc=local");
        lctx.setPassword("ITacc3ss");
        lctx.afterPropertiesSet();

        LdapTemplate tmpl = new LdapTemplate(lctx);

        return tmpl;
    }
}
