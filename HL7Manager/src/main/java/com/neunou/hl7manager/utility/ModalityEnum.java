package com.neunou.hl7manager.utility;

import com.neunou.schema.Modality;

/**
 * Created by root on 12/7/15.
 */
public class ModalityEnum {
    private String modalityName;
    private Modality modality;

    public ModalityEnum(String modalityName) {
        this.modalityName = modalityName;
    }

    public Modality transferToModality(){
        if (this.modalityName.equals("CE")){
            modality = Modality.CE;
        }
        if (this.modalityName.equals("CR")){
            modality = Modality.CR;
        }
        if (this.modalityName.equals("CT")){
            modality = Modality.CT;
        }
        if (this.modalityName.equals("CTA")){
            modality = Modality.CTA;
        }
        if (this.modalityName.equals("DR")){
            modality = Modality.DR;
        }
        if (this.modalityName.equals("DD")){
            modality = Modality.DD;
        }
        if (this.modalityName.equals("IR")){
            modality = Modality.IR;
        }
        if (this.modalityName.equals("MR")){
            modality = Modality.MR;
        }
        if (this.modalityName.equals("MRA")){
            modality = Modality.MRA;
        }
        if (this.modalityName.equals("Mammo")){
            modality = Modality.Mammo;
        }
        if (this.modalityName.equals("NM")){
            modality = Modality.NM;
        }
        if (this.modalityName.equals("Other")){
            modality = Modality.Other;
        }
        if (this.modalityName.equals("PET")){
            modality = Modality.PET;
        }
        if (this.modalityName.equals("RF")){
            modality = Modality.RF;
        }
        if (this.modalityName.equals("RI")){
            modality = Modality.RI;
        }
        if (this.modalityName.equals("US")){
            modality = Modality.US;
        }
        return modality;
    }
}
