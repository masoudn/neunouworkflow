package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.PhoneCallRecordsDAO;
import com.neunou.schema.HL7Device;
import com.neunou.schema.PhoneCallRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Masoud on 3/17/15.
 */
@Repository
public class PhoneCallRecordsDAOImpl implements PhoneCallRecordsDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String DEVICE_COL = "phoneCallRecords";

    @Override
    public String add(Object object) {
        PhoneCallRecords phoneCallRecords = (PhoneCallRecords) object;
        if (phoneCallRecords.getPhoneCallRecordId()==null){
            phoneCallRecords.setPhoneCallRecordId(UUID.randomUUID().toString());
            phoneCallRecords.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(DEVICE_COL)){
                mongoTemplate.createCollection(DEVICE_COL);
                mongoTemplate.insert(phoneCallRecords,DEVICE_COL);
                return phoneCallRecords.getPhoneCallRecordId();
            }
            else {
                mongoTemplate.insert(phoneCallRecords,DEVICE_COL);
                return phoneCallRecords.getPhoneCallRecordId();
            }

        }
        else {
            this.update(object);
            return phoneCallRecords.getPhoneCallRecordId();
        }
    }

    @Override
    public void delete(Object object) {
        PhoneCallRecords phoneCallRecords = (PhoneCallRecords) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("phoneCallRecordId").is(phoneCallRecords.getPhoneCallRecordId()));

        mongoTemplate.remove(query,PhoneCallRecords.class,DEVICE_COL);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("phoneCallRecordId").is(id));
        return mongoTemplate.findOne(query,PhoneCallRecords.class,DEVICE_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(PhoneCallRecords.class,DEVICE_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,PhoneCallRecords.class,DEVICE_COL);
        return exists;
    }
}
