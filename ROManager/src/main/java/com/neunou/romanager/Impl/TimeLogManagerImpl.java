package com.neunou.romanager.Impl;

import com.neunou.romanager.Interfaces.TimeLogManager;
import com.neunou.ropersistence.Interfaces.TimeLogDAO;
import com.neunou.schema.TimeLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by root on 6/8/15.
 */
@Component
public class TimeLogManagerImpl implements TimeLogManager {
    @Autowired
    private TimeLogDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }

    @Override
    public List<TimeLog> getAllTimeLogsByPerson(String personId) {
        return dao.getAllTimeLogsByPerson(personId);
    }
}
