package com.neunou.hl7manager.utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by root on 4/29/15.
 */
public class SlicingText {
    private String text;

    public SlicingText(String text) {
        this.text = text;
    }

    public HashMap<String,List<String>> disect(){
        List<String> values = new ArrayList<>();
        HashMap<String,List<String>> slicedData= new HashMap<>();
        String[] datas = this.text.split("|");
        for (String data:datas){
            System.out.println("Data is: "+data);
        }

        return slicedData;
    }

    public HashMap<Integer,String> dissectForChar(String element){
        HashMap<Integer,String> dataMap = new HashMap<>();
        Integer count =1;
        String newText = this.text.replaceAll("\\[","").replaceAll("\\]","");
        if (element.equals("^")){
            String[] datas = newText.split("\\^");
            System.out.println("-------------------------");
            System.out.println("Size of the data is: "+datas.length);
            for (String data : datas) {
                if (count==1){
                    System.out.println("Escaping the data");
                }
                else {
                    dataMap.put(count-1,data);
                }
                count +=1;
            }
            return dataMap;
        }
        else {
            String[] datas = this.text.split(element);
            for (String data:datas){
                if(data.contains("[")){
                    data = data.substring(data.indexOf("[")+1);
                }
                else if (data.contains("]")){
                    data = data.replace("]","");
                }
                dataMap.put(count,data);
                count+=1;
            }
            return dataMap;
        }
    }

}
