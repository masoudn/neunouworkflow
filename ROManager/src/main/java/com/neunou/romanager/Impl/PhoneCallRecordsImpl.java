package com.neunou.romanager.Impl;

import com.neunou.romanager.Interfaces.PhoneCallRecordsManager;
import com.neunou.ropersistence.Interfaces.PhoneCallRecordsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created by Masoud on 3/17/15.
 */
@Component
public class PhoneCallRecordsImpl implements PhoneCallRecordsManager {

    @Autowired
    private PhoneCallRecordsDAO dao;

    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return null;
    }
}
