package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.UsersDAO;
import com.neunou.schema.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 6/12/15.
 */
@Repository
public class UsersDAOImpl implements UsersDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String USERS_COL = "users";
    @Override
    public String add(Object object) {
        Person user = (Person) object;
        List loginList = new ArrayList<>();
        if (user.getPersonId()==null){
            user.setPersonId(UUID.randomUUID().toString());
            loginList.add(new Date());
            user.setWebLoginDates(loginList);
            if(!mongoTemplate.collectionExists(USERS_COL)){
                mongoTemplate.createCollection(USERS_COL);
                mongoTemplate.insert(user,USERS_COL);
                return user.getPersonId();
            }
            else {
                mongoTemplate.insert(user,USERS_COL);
                return user.getPersonId();
            }
        }
        else {
            this.update(object);
            return user.getPersonId();
        }

    }

    @Override
    public void delete(Object object) {

    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("personId").is(id));
        return mongoTemplate.findOne(query,Person.class,USERS_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(Person.class,USERS_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return null;
    }

    @Override
    public Person getByUsername(String username) {
        Query query = new Query();
        query.addCriteria(Criteria.where("personInfo.username").is(username));
        return mongoTemplate.findOne(query,Person.class,USERS_COL);
    }
}
