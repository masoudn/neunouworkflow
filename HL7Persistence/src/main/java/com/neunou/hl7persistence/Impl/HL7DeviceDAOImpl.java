package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.HL7DeviceDAO;
import com.neunou.schema.HL7Device;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.data.mongodb.core.query.Query;

import javax.management.QueryEval;
import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Masoud on 9/17/14.
 */
@Repository
public class HL7DeviceDAOImpl implements HL7DeviceDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String DEVICE_COL = "hl7Device";
    @Override
    public String add(Object object) {
        HL7Device hl7Device = (HL7Device) object;
        if (hl7Device.getHl7DeviceId()==null){
            hl7Device.setHl7DeviceId(UUID.randomUUID().toString());
            hl7Device.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(DEVICE_COL)){
                mongoTemplate.createCollection(DEVICE_COL);
                mongoTemplate.insert(hl7Device,DEVICE_COL);
                return hl7Device.getHl7DeviceId();
            }
            else {
                mongoTemplate.insert(hl7Device,DEVICE_COL);
                return hl7Device.getHl7DeviceId();
            }

        }
        else {
            this.update(object);
            return hl7Device.getHl7DeviceId();
        }
    }

    @Override
    public void delete(Object object) {

        HL7Device hl7Device = (HL7Device) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("hl7DeviceId").is(hl7Device.getHl7DeviceId()));

        mongoTemplate.remove(query,HL7Device.class,DEVICE_COL);
    }

    @Override
    public void update(Object object) {
        HL7Device hl7Device = (HL7Device) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("hl7DeviceId").is(hl7Device.getHl7DeviceId()));

        hl7Device.setModificationDate(new Date());
        Update update = new Update();
        update.set("deviceName",hl7Device.getDeviceName());
        update.set("host",hl7Device.getHost());
        update.set("port",hl7Device.getPort());
        update.set("inbound",hl7Device.getInbound());
        update.set("outbound",hl7Device.getOutbound());
        update.set("running",hl7Device.getRunning());
        update.set("useTLS",hl7Device.getUseTLS());
        update.set("modificationDate",hl7Device.getModificationDate());

        mongoTemplate.findAndModify(query,update,HL7Device.class,DEVICE_COL);

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("hl7DeviceId").is(id));
        HL7Device device = mongoTemplate.findOne(query,HL7Device.class,DEVICE_COL);

        return device;
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(HL7Device.class,DEVICE_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,HL7Device.class,DEVICE_COL);
        return exists;
    }

    @Override
    public void startHL7Device(HL7Device hl7Device) {

    }

    @Override
    public void stopHL7Device(HL7Device hl7Device) {

    }
}
