package com.neunou.wi.API;

import com.neunou.romanager.Interfaces.UsersManager;
import com.neunou.schema.Person;
import com.neunou.wi.Utility.GetAuthPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.logging.Logger;

/**
 * Created by root on 7/13/15.
 */
@Controller
@RequestMapping(value="/api/person")
public class PersonAPI {
    private static final Logger LOG = Logger.getLogger(PersonAPI.class.getName());
    @Autowired
    private UsersManager userMan;
    @Autowired
    private GetAuthPerson authPerson;

    @RequestMapping(method = RequestMethod.GET, value="/getLoggedIn",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Person getLoggedInPerson(){
        return userMan.getByUsername(authPerson.getPerson().getPersonInfo().getUsername());
    }

}
