<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 3/5/15
  Time: 5:16 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    var app = angular.module("NeuNouApp",["ngAnimate",'ngRoute']);
    app.service("dataService",function($http){
        this.queryData = function(apiURL){
            return $http({
                method : 'GET',
                url: apiURL,
                headers: {'Content-Type': 'application/json'}
            })
                    .then(function(result){
                        console.log(result);
                        return result.data;

                    });
        }
        this.saveData = function(apiURL,element){
            console.log("saving the data ...");

            return $http({
                method: 'POST',
                url: apiURL,
                data: element,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result) {
                console.log(result);
            }, function(error) {
                console.log(error);
            });

        }
        this.deleteData = function(apiURL,element){
            return $http({
                method: 'DELETE',
                url: apiURL,
                data: element,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result){
                console.log("Result: "+result);
            }, function(error){
                console.log("Error: "+error);
            });
        }


        this.statusChange = function(apiURL){
            return $http({
                method: 'POST',
                url: apiURL,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result){
                console.log("Result: "+result);
            }, function(error){
                console.log("Error: "+error);
            });
        }
    });

    app.controller("hl7Controller",function(dataService,$scope){
        $scope.hl7Master = {};
        $scope.hl7State = {show: false};
        $scope.showHL7Device = function(){
            $scope.hl7State.show = !$scope.hl7State.show;
        };
        $scope.getHL7Data = function(){
            dataService.queryData('/api/hl7Device/getAll').then(function(dataResponse){
                $scope.hl7Devices = dataResponse;
                console.log(dataResponse);
                if(dataResponse.length >0){
                    $scope.hl7ListBool = true;
                }
                else{
                    $scope.hl7ListBool = false;
                }
            })
        }
        $scope.getHL7Data();
        $scope.addHL7 = function(hl7El){
            console.log(hl7El);
            $scope.hl7Master = angular.copy(hl7El);


            if(typeof hl7El.useTLS !== 'null'){

                hl7El.useTLS=false;
                console.log("TLs is: "+$scope.hl7Master.useTLS);
                dataService.saveData("/api/hl7Device/save/",hl7El).then(function(dataResponse){
                    $scope.showHL7Device();
                    $scope.getHL7Data();
                });
            }
            else{
                dataService.saveData("/api/hl7Device/save/",hl7El).then(function(dataResponse){
                    $scope.showHL7Device();
                    $scope.getHL7Data();
                });
            }

        }
        $scope.reset = function(){
            $scope.hl7El = angular.copy($scope.hl7Master);
        }
        $scope.editHL7 = function(index){
            console.log("Index is..."+index);
            $scope.hl7El= $scope.hl7Devices[index];
            $scope.showHL7Device();
        }

        $scope.deleteHL7 = function(index){
            console.log("Index is..."+index);
            $scope.hl7ToDelete = $scope.hl7Devices[index];
            dataService.deleteData('/api/hl7Device/delete/'+$scope.hl7ToDelete.hl7DeviceId,$scope.hl7ToDelete.hl7DeviceId).then(function(){
                $scope.getHL7Data();
            });
        }

        $scope.stopHL7Device = function(index){
            $scope.hl7ToStop = $scope.hl7Devices[index];
            $scope.hl7ToStop.running=false;
            console.log("Stopping the device ..."+$scope.hl7ToStop.deviceName);
            dataService.statusChange('/api/hl7Device/stop/'+$scope.hl7ToStop.hl7DeviceId).then(function(dataResponse){

                $scope.getHL7Data();
            });

        }

        $scope.startHL7Device = function(index){
            $scope.hl7ToStart = $scope.hl7Devices[index];
            $scope.hl7ToStart.running=true;
            console.log("Starting the device ..."+$scope.hl7ToStart.deviceName);
            dataService.statusChange('/api/hl7Device/start/'+$scope.hl7ToStart.hl7DeviceId).then(function(dataResponse){

                $scope.getHL7Data();
            });

        }


    });

    app.controller("dicomController",function(dataService,$scope){
        $scope.dicomMaster = {};
        $scope.dicomState = {show: false};
        $scope.dicomDirState = {show: false};
        $scope.dicomDirMaster = {};
        $scope.showDicomDevice = function(){
            console.log("Adding the new DICOM device...."+$scope.dicomState.show);
            $scope.dicomState.show = !$scope.dicomState.show;
        };
        $scope.getDicomData = function(){
            dataService.queryData('/api/dicomDevice/getAll').then(function(dataResponse){
                $scope.dicomDevices = dataResponse;
                console.log(dataResponse);
                if(dataResponse.length >0){
                    $scope.dicomListBool = true;
                }
                else{
                    $scope.dicomListBool = false;
                }
            })
        }
        $scope.getDicomData();
        $scope.addDicom = function(dicomEl){
            console.log(dicomEl);
            console.log(dicomEl.dicomDirId);
            $scope.dicomMaster = angular.copy(dicomEl);
            console.log($scope.dicomMaster.dicomDirId);


            if(typeof dicomEl.useTLS !== 'null'){

                dicomEl.useTLS=false;
                console.log("TLs is: "+$scope.dicomMaster.useTLS);
                dataService.saveData("/api/dicomDevice/save/",dicomEl).then(function(dataResponse){
                    $scope.showDicomDevice();
                    $scope.getDicomData();
                });
            }
            else{
                dataService.saveData("/api/dicomDevice/save/",dicomEl).then(function(dataResponse){
                    $scope.showDicomDevice();
                    $scope.getDicomData();
                });
            }

        }
        $scope.reset = function(){
            $scope.dicomEl = angular.copy($scope.dicomMaster);
        }
        $scope.editDicom = function(index){
            console.log("DICOM Index is..."+index);
            $scope.dicomEl= $scope.dicomDevices[index];
            $scope.showDicomDevice();
        }

        $scope.deleteDicom = function(index){
            console.log("Dicom Index is..."+index);
            $scope.dicomToDelete = $scope.dicomDevices[index];
            dataService.deleteData('/api/dicomDevice/delete/'+$scope.dicomToDelete.dicomDeviceId,$scope.dicomToDelete.dicomDeviceId).then(function(){
                $scope.getDicomData();
            });
        }

        $scope.stopDicomDevice = function(index){
            $scope.dicomToStop = $scope.dicomDevices[index];
            $scope.dicomToStop.running=false;
            console.log("Stopping the device ..."+$scope.dicomToStop.deviceName);
            dataService.statusChange('/api/dicomDevice/stop/'+$scope.dicomToStop.hl7DeviceId).then(function(dataResponse){

                $scope.getDicomData();
            });

        }

        $scope.startDicomDevice = function(index){
            $scope.dicomToStart = $scope.dicomDevices[index];
            $scope.dicomToStart.running=true;
            console.log("Starting the device ..."+$scope.dicomToStart.deviceName);
            dataService.statusChange('/api/dicomDevice/start/'+$scope.dicomToStart.dicomDeviceId).then(function(dataResponse){

                $scope.getDicomData();
            });

        }
        $scope.showDicomDir = function(){
            console.log("Adding the new File Address...."+$scope.dicomDirState.show);
            $scope.dicomDirState.show = !$scope.dicomDirState.show;
        }
        $scope.getDicomDirData = function(){
            dataService.queryData('/api/dicomDirInfo/getAll').then(function(dataResponse){
                $scope.dicomDir = dataResponse;
                console.log("Calling for all the Directories.....");
                console.log(dataResponse);
                if(dataResponse.length >0){
                    $scope.dicomDirBool = true;
                }
                else{
                    $scope.dicomDirBool = false;
                }
            })
        }
        $scope.getDicomDirData();
        $scope.addDicomDir = function(dicomDirEl){
            console.log(dicomDirEl);
            $scope.dicomDirMaster = angular.copy(dicomDirEl);

            dataService.saveData("/api/dicomDirInfo/save/",dicomDirEl).then(function(dataResponse){
                $scope.showDicomDir();
                $scope.getDicomDirData();
                });

        }
        $scope.reset = function(){
            $scope.dicomDirEl = angular.copy($scope.dicomDirMaster);
        }
        $scope.editDicomDir = function(index){
            console.log("DICOM DIR Index is..."+index);
            $scope.dicomDirEl= $scope.dicomDir[index];
            $scope.showDicomDir();
        }

        $scope.deleteDicomDir = function(index){
            console.log("DICOM DIR Index is..."+index);
            $scope.dicomDirToDelete = $scope.dicomDir[index];
            dataService.deleteData('/api/dicomDirInfo/delete/'+$scope.dicomDirToDelete.dicomDirId,$scope.dicomDirToDelete.dicomDirId).then(function(){
                $scope.getDicomDirData();
            });
        }

    });


</script>
