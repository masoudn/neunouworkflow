package com.neunou.hl7manager.interfaces;

import com.neunou.schema.Physician;
import org.springframework.stereotype.Service;

/**
 * Created by Masoud on 4/1/15.
 */
@Service
public interface PhysicianManager extends GenericInterface {
    Physician getPhysicianByNPI(String npi);
    Physician getPhysicianByName (String fName, String lName);
}
