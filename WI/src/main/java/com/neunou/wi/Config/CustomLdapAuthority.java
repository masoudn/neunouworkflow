package com.neunou.wi.Config;

import com.neunou.hl7persistence.Impl.LDAPUserImpl;
import com.neunou.romanager.Interfaces.UsersManager;
import com.neunou.schema.Person;
import com.neunou.schema.PersonInfo;
import com.neunou.schema.Role;
import com.neunou.wi.Utility.GetAuthPerson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ldap.core.DirContextOperations;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.ldap.userdetails.LdapAuthoritiesPopulator;
import org.springframework.stereotype.Component;

import javax.naming.NamingException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 2/24/15.
 */
@Component
public class CustomLdapAuthority implements LdapAuthoritiesPopulator {
    @Autowired
    private LDAPUserImpl ldapUser;
    @Autowired
    private UsersManager userMan;

    private static final Logger LOG = Logger.getLogger(CustomLdapAuthority.class.getName());
    @Override
    public Collection<? extends GrantedAuthority> getGrantedAuthorities(DirContextOperations userData, String username) {
        System.out.printf("User %s has logged in @ %s", username, new Date());
        System.out.println("in getGrantedAuthorities");
        List<Role> roles = new ArrayList<Role>();

        List<GrantedAuthority> userPermission = new ArrayList<GrantedAuthority>();
        LOG.info("Person is loging into the system, we are in custom Authority and the person is: "+userData.getDn());
        try {
            Person person = ldapUser.getPersonByDN(String.valueOf(userData.getDn()));
            LOG.info("+++Name is: "+person.getPersonInfo().getPersonName().getPersonFullName());

            List<String> groups = person.getPersonInfo().getMemberOf();
            for(String cn:groups){
                System.out.println("Groups is: "+cn);
                if (cn.contains("Administrators")){
                    LOG.info("This user is a part of Administrator");
                    userPermission.add(new SimpleGrantedAuthority("ADMIN"));
                    Role role = new Role();
                    role.setRole("ADMIN");
                    role.setRoleDesc("Administrators");
                    roles.add(role);
                }
                else if (cn.contains("Radiologists")){
                    LOG.info("This user is a RAD...");
                    userPermission.add(new SimpleGrantedAuthority("RAD"));
                    Role role = new Role();
                    role.setRole("RAD");
                    role.setRoleDesc("Radiologists");
                    roles.add(role);
                }
                else {
                    LOG.info("This user is not Administrator..."+username);

                }
            }
            if (userMan.getByUsername(username)!=null){
                System.out.println("This user exists in the DB, we are updating ...");
                List<Date> loginDates = new ArrayList<Date>();
                loginDates.add(new Date());
                person.setRoleList(roles);
                person.setWebLoginDates(loginDates);

                userMan.update(person);
            }
            else {
                System.out.println("This user does not exists in the DB, we are inserting ...");
                List<Date> loginDates = new ArrayList<Date>();
                loginDates.add(new Date());
                person.setRoleList(roles);
                person.setWebLoginDates(loginDates);

                userMan.add(person);
            }

        } catch (NamingException e) {
            e.printStackTrace();
        }

        return userPermission;
    }


}
