package com.neunou.wi.API;

import com.neunou.hl7manager.interfaces.GroupsManager;
import com.neunou.schema.Groups;
import com.neunou.wi.Utility.GetAllGroupsFromLDAP;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

/**
 * Created by Masoud on 3/19/15.
 */
@Controller
@RequestMapping(value="/api/groups")
public class GroupsAPI {
    private static final Logger LOG = Logger.getLogger(GroupsAPI.class.getName());
    private List<Groups> groupsList;
    @Autowired
    private GroupsManager man;
    @Autowired
    private GetAllGroupsFromLDAP getAllGroupsFromLDAP;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Groups> getAllGroups(){
        LOG.info("Getting all groups in API...");
        groupsList = new CopyOnWriteArrayList<Groups>();
        groupsList = man.getAll();
        List<Groups> finalGroup = new ArrayList<Groups>();

        Iterator<Groups> it = groupsList.iterator();
        Collections.sort(groupsList,Groups.GroupsNameComparator);
        while (it.hasNext()){
            Groups group = it.next();
            if(group.getExcluded()!=true){
                finalGroup.add(group);

            }
        }
        return finalGroup;
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String saveGroup(@RequestBody Groups group){
        return man.add(group);
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{groupId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Groups getGroupById(@PathVariable("groupId") String groupId){
        return (Groups)man.getById(groupId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{groupId:.+}")
    public void deleteGroupById (@PathVariable("groupId") String groupId){
        man.delete(man.getById(groupId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateGroup(@RequestBody Groups group){
        this.man.update(group);
    }

    @RequestMapping(method = RequestMethod.GET, value="/getAllGroups")
    public void getAllTheGroupsFromLDAP(){
        getAllGroupsFromLDAP.saveGroupsToDB();
    }

}
