package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.datatype.XPN;
import ca.uhn.hl7v2.model.v25.message.ORM_O01;
import com.neunou.schema.PersonName;

/**
 * Created by root on 2/24/16.
 */
public class GetPersonNameFromORM {
    private ORM_O01 orm;
    private PersonName personName;

    public ORM_O01 getOrm() {
        return orm;
    }

    public void setOrm(ORM_O01 orm) {
        this.orm = orm;
    }

    public PersonName getPersonName() {

        return personName;
    }

    public void setPersonName(PersonName personName) {
        this.personName = personName;
    }
    public PersonName nameFactory(){
        setPersonName(new PersonName());
        XPN[] patientName = orm.getPATIENT().getPID().getPatientName();
        for (XPN entry : patientName) {
            String fNAme = entry.getGivenName().getValue().toString();
            String lName = entry.getFamilyName().getSurname().toString();
            getPersonName().setPersonFirstName(fNAme);
            getPersonName().setPersonLastName(lName);
            getPersonName().setPersonFullName(fNAme + " " + lName);

        }
        return getPersonName();
    }

    public GetPersonNameFromORM(ORM_O01 orm) {
        this.orm = orm;
    }
}
