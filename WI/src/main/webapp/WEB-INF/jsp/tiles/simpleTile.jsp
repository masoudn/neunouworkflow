<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 2/12/15
  Time: 11:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <tiles:insertAttribute name="title" />
    </title>
    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/isotope.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/jquery.fancybox.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/fullcalendar.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/wizard.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/select2.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/morris.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/datatables.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/datepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/timepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/colorpicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap-switch.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/daterange-picker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/typeahead.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/summernote.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/ladda-themeless.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/social-buttons.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/pygments.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/style.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/color/green.css" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="/template/css/color/orange.css" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="/template/css/color/magenta.css" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="/template/css/color/gray.css" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="/template/css/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/template/css/dropzone.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="http://code.jquery.com/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/template/js/raphael.min.js" type="text/javascript"></script>
    <script src="/template/js/selectivizr-min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="/template/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
    <script src="/template/js/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/template/js/gcal.js" type="text/javascript"></script>
    <script src="/template/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/template/js/datatable-editable.js" type="text/javascript"></script>
    <script src="/template/js/jquery.easy-pie-chart.js" type="text/javascript"></script>
    <script src="/template/js/excanvas.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="/template/js/isotope_extras.js" type="text/javascript"></script>
    <script src="/template/js/modernizr.custom.js" type="text/javascript"></script>
    <script src="/template/js/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="/template/js/select2.js" type="text/javascript"></script>
    <script src="/template/js/styleswitcher.js" type="text/javascript"></script>
    <script src="/template/js/wysiwyg.js" type="text/javascript"></script>
    <script src="/template/js/typeahead.js" type="text/javascript"></script>
    <script src="/template/js/summernote.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.validate.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-fileupload.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-timepicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-colorpicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/template/js/typeahead.js" type="text/javascript"></script>
    <script src="/template/js/spin.min.js" type="text/javascript"></script>
    <script src="/template/js/ladda.min.js" type="text/javascript"></script>
    <script src="/template/js/moment.js" type="text/javascript"></script>
    <script src="/template/js/mockjax.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-editable.min.js" type="text/javascript"></script>
    <script src="/template/js/xeditable-demo-mock.js" type="text/javascript"></script>
    <script src="/template/js/xeditable-demo.js" type="text/javascript"></script>
    <script src="/template/js/address.js" type="text/javascript"></script>
    <script src="/template/js/daterange-picker.js" type="text/javascript"></script>
    <script src="/template/js/date.js" type="text/javascript"></script>
    <script src="/template/js/morris.min.js" type="text/javascript"></script>
    <script src="/template/js/skycons.js" type="text/javascript"></script>
    <script src="/template/js/fitvids.js" type="text/javascript"></script>
    <script src="/template/js/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/template/js/dropzone.js" type="text/javascript"></script>
    <script src="/template/js/main.js" type="text/javascript"></script>
    <script src="/template/js/respond.js" type="text/javascript"></script><meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
    <%--d3 dependencies--%>
    <link href="/template/css/hexbin.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>

    <%--Google Analytics--%>
    <script src="/template/js/googleAnalytics.js" type="text/javascript"></script>
</head>
    <body class="login1">
        <tiles:insertAttribute name="body" />
    </body>
</html>
