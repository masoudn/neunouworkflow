package com.neunou.schema;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by root on 12/10/15.
 */
public enum VisitStatus {
    Scheduled (1),
    Canceled (0),
    Arrived (2),
    CheckedIn (3),
    Waiting (4),
    Ended (5),
    CheckedOut (6),
    Completed (7),
    Final (8),
    Changed (9);

    private int statusCode;


    private VisitStatus(int statusCode){
        this.statusCode = statusCode;
    }

    public int getStatusCode(){
        return this.statusCode;
    }

}
