package com.neunou.dicommanager.Interfaces;

import org.springframework.stereotype.Service;

/**
 * Created by root on 3/7/16.
 */
@Service
public interface DICOMDirManager extends GenericInterface{
    void deleteDicomDirById(String id);
}
