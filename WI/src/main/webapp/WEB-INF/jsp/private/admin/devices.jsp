<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 3/6/15
  Time: 11:43 AM
  To change this template use File | Settings | File Templates.
--%>
<html ng-app="NeuNouApp">
<div class="row" >
    <div class="col-md-6" ng-controller="hl7Controller">
        <div class="widget-container fluid-height">
            <div class="heading">
                <i class="fa fa-stack-exchange"></i>HL7 Servers
            </div>

            <div class="widget-content padded">
                <ng-switch on="hl7ListBool" >
                    <div class="text-center" ng-switch-when="false">
                        <div class="widget-content padded clearfix">
                            <a class="btn btn-sm btn-primary-outline pull-right" ng-click="showHL7Device()" id="add-row"><i class="fa fa-plus"></i>Add New HL7 Device</a>
                        </div>
                    </div>
                    <div ng-switch-when="true">
                        <div class="widget-content padded clearfix">
                            <table class="table">
                                <thead>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Host
                                </th>

                                <th class="hidden-xs">
                                    Status
                                </th>
                                <th>
                                    Actions
                                </th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="hl7 in hl7Devices">
                                    <td>
                                        {{hl7.deviceName}}
                                    </td>
                                    <td>
                                        {{hl7.host}}
                                    </td>

                                    <td class="hidden-xs">
                                        <button ng-show={{hl7.running}} class="btn btn-sm btn-danger" ng-click="stopHL7Device($index)">Stop</button>
                                        <button ng-hide={{hl7.running}} class="btn btn-sm btn-primary" ng-click="startHL7Device($index)">Start</button>

                                    </td>


                                    <td class="actions">
                                        <div class="action-buttons">
                                            <a class="table-actions" ng-click="showHL7Device()"><i class="fa fa-eye"></i></a>
                                            <a class="table-actions" ng-click="editHL7($index)"><i class="fa fa-pencil"></i></a>
                                            <a class="table-actions" ng-click="deleteHL7($index)"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <a class="btn btn-sm btn-primary-outline pull-right" ng-click="showHL7Device()" id="add-row"><i class="fa fa-plus"></i>Add New HL7 Device</a>
                        </div>
                    </div>

                </ng-switch>
            </div>

            <div class="animate-show" ng-show="hl7State.show">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-2">Device Name</label>
                        <div class="col-md-7">
                            <input class="form-control" placeholder="HL7 Device Name" type="text" ng-model="hl7El.deviceName">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Host Address</label>
                        <div class="col-md-7">
                            <input class="form-control" placeholder="HL7 Server Host Address" type="text" ng-model="hl7El.host">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Port</label>
                        <div class="col-md-3">
                            <input class="form-control"  type="text" placeholder="HL7 Device Port Number"  ng-model="hl7El.port">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Server</label>
                        <div class="col-md-7">
                            <label class="radio-inline"><input name="optionsRadios2" type="radio" ng-model="hl7El.inbound" ng-value="true"><span>Inbound</span></label>
                            <label class="radio-inline"><input checked="" name="optionsRadios2" type="radio" ng-model="hl7El.outbound" ng-value="true"><span>Outbound</span></label>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-2">Server</label>
                        <div class="col-md-7">
                            <label class="checkbox-inline"><input name="optionsCheckbox" type="checkbox"  ng-model="hl7El.useTLS"><span>TLS Encryption</span></label>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" ng-click="addHL7(hl7El)" class="btn btn-primary">Save</button>
                            <button ng-click="showHL7Device()" class="btn btn-default-outline">Cancel</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div class="col-md-6" ng-controller="dicomController">
        <div class="widget-container fluid-height">
            <div class="heading">
                <i class="fa fa-stack-exchange"></i>DICOM Servers
            </div>

            <div class="widget-content padded">
                <ng-switch on="dicomListBool" >
                    <div class="text-center" ng-switch-when="false">
                        <div class="widget-content padded clearfix">
                            <a class="btn btn-sm btn-primary-outline pull-right" ng-click="showDicomDevice()" id="add-row"><i class="fa fa-plus"></i>Add New DICOM Device</a>
                        </div>
                    </div>
                    <div ng-switch-when="true">
                        <div class="widget-content padded clearfix">
                            <table class="table">
                                <thead>
                                <th>
                                    Name
                                </th>
                                <th>
                                    Host
                                </th>

                                <th class="hidden-xs">
                                    Status
                                </th>
                                <th>
                                    Actions
                                </th>
                                </thead>
                                <tbody>
                                <tr ng-repeat="dicom in dicomDevices">
                                    <td>
                                        {{dicom.dicomDeviceName}}
                                    </td>
                                    <td>
                                        {{dicom.dicomDeviceIP}}
                                    </td>

                                    <td class="hidden-xs">
                                        <button ng-show={{dicom.running}} class="btn btn-sm btn-danger" ng-click="stopDicomDevice($index)">Stop</button>
                                        <button ng-hide={{dicom.running}} class="btn btn-sm btn-primary" ng-click="startDicomDevice($index)">Start</button>

                                    </td>


                                    <td class="actions">
                                        <div class="action-buttons">
                                            <a class="table-actions" ng-click="showDicomDevice()"><i class="fa fa-eye"></i></a>
                                            <a class="table-actions" ng-click="editDicom($index)"><i class="fa fa-pencil"></i></a>
                                            <a class="table-actions" ng-click="deleteDicom($index)"><i class="fa fa-trash-o"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <a class="btn btn-sm btn-primary-outline pull-right" ng-click="showDicomDevice()" id="add-row"><i class="fa fa-plus"></i>Add New DICOM Device</a>
                        </div>
                    </div>

                </ng-switch>
            </div>

            <div class="animate-show" ng-show="dicomState.show">
                <form role="form" class="form-horizontal">
                    <div class="form-group">
                        <label class="control-label col-md-3">Device Name</label>
                        <div class="col-md-7">
                            <input class="form-control" placeholder="DICOM Device Name" type="text" ng-model="dicomEl.dicomDeviceName">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Host Address</label>
                        <div class="col-md-7">
                            <input class="form-control" placeholder="DICOM Server Host Address" type="text" ng-model="dicomEl.dicomDeviceIP">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">AE Title</label>
                        <div class="col-md-7">
                            <input class="form-control" placeholder="DICOM Server AE Title" type="text" ng-model="dicomEl.aet">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Port</label>
                        <div class="col-md-3">
                            <input class="form-control"  type="text" placeholder="DICOM Port"  ng-model="dicomEl.dicomDevicePort">
                        </div>
                    </div>

                    <%--
                        File Server Location
                    --%>
                    <div class="form-group">
                        <label class="control-label col-md-3">File Address</label>
                        <%--<div class="col-md-7">--%>
                            <%--<select class="select2able" ng-model="dicomEl.dicomDirId">--%>
                                <%--<option ng-repeat="dir in dicomDir track by $index" value="{{dicomEl.dicomDirId}}">{{dir.filePath}}</option>--%>
                            <%--</select>--%>
                        <%--</div>--%>
                        <div class="col-md-7">
                            <select class="select2able" ng-model="dicomEl.dicomDirId" ng-options="dir.dicomDirId as dir.filePath for dir in dicomDir ">
                                <option  value="">File Address</option>
                            </select>
                        </div>
                        <div class="btn-group mod">
                            <button class="btn btn-default-outline dropdown-toggle" data-toggle="dropdown">Action<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li>
                                    <a ng-click="showDicomDir()"><i class="fa fa-plus" ></i>New Item</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="form-group animate-show " ng-show="dicomDirState.show">
                        <label class="control-label col-md-3 "></label>
                        <div class="col-md-7 well" >
                            <strong>File Address Setting</strong>
                            <div class="widget-content padded clearfix">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <input class="form-control" placeholder="File Address" type="text" ng-model="dicomDirEl.filePath">
                                    </div>
                                </div>
                                <a class="btn btn-xs btn-primary pull-right" ng-click="addDicomDir(dicomDirEl)" id="add-row"><i class="fa fa-plus"></i>Add</a>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-2">Server Type</label>
                        <div class="col-md-7">
                            <label class="radio-inline"><input name="optionsRadios2" type="radio"  ng-model="dicomEl.local" ng-value="true"><span>Local</span></label>
                            <label class="radio-inline"><input checked="" name="optionsRadios2" type="radio"  ng-model="dicomEl.remote" ng-value="true"><span>Remote</span></label>
                        </div>
                    </div>

                    <%--
                        TLS Encryption
                    --%>
                    <div class="form-group">
                        <label class="control-label col-md-3">Server</label>
                        <div class="col-md-7">
                            <label class="checkbox-inline"><input name="optionsCheckbox" type="checkbox"  ng-model="dicom7El.useTLS"><span>TLS Encryption</span></label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <button type="submit" ng-click="addDicom(dicomEl)" class="btn btn-primary">Save</button>
                            <button ng-click="showDicomDevice()" class="btn btn-default-outline">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</html>
