package com.neunou.schema;

import java.util.List;

/**
 * Created by Masoud on 9/12/14.
 */
public class HL7MessageTypes {
    private String hl7TypeId;
    private String hl7Type;
    private String hl7Version;
    private String hl7TypeDesc;
    private List<String> auditId;


    public String getHl7TypeId() {
        return hl7TypeId;
    }

    public void setHl7TypeId(String hl7TypeId) {
        this.hl7TypeId = hl7TypeId;
    }

    public String getHl7Type() {
        return hl7Type;
    }

    public void setHl7Type(String hl7Type) {
        this.hl7Type = hl7Type;
    }

    public String getHl7TypeDesc() {
        return hl7TypeDesc;
    }

    public void setHl7TypeDesc(String hl7TypeDesc) {
        this.hl7TypeDesc = hl7TypeDesc;
    }

    public List<String> getAuditId() {
        return auditId;
    }

    public void setAuditId(List<String> auditId) {
        this.auditId = auditId;
    }

    public String getHl7Version() {
        return hl7Version;
    }

    public void setHl7Version(String hl7Version) {
        this.hl7Version = hl7Version;
    }
}
