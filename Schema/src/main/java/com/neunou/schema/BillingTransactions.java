package com.neunou.schema;

/**
 * Created by Masoud on 5/1/15.
 */

import java.math.BigDecimal;

public class BillingTransactions {
    private String billingTransactionId;
    private String transactionID;
    private int chargeID;
    private int  transactionType;
    private int statusID;
    private String clientID;
    private String batchID;
    private String patientID;
    private String guarantorID;
    private BigDecimal amount;
    private int position;
    private String userID;
    private String paymentID;
    private String reversalDate;
    private String reversalOperator;
    private String lastUpdateDate;
    private String creationDate;
    private String dateApplied;
    private String transTypeID;
    private String transactionCode;
    private String reportClass;
    private String checkdate;
    private String eOBID;
    private String scanPagenumber;
    private String remittanceBatchID;
    private String remittanceLineNumber;
    private String refundCheckNumber;
    private String batchChildImageName;
    private String deductibleAmount;
    private String allowedAmount;
    private String coinsuranceAmount;
    private String paymentVariancePercent;
    private String expectedpayment;
    private String patientPaymentID;
    private String keyInDateTime;
    private String denialReasonID;
    private String creditCardType;
    private String creditCardNumber;
    private String creditCardExpirationDate;
    private String transferred;
    private String transferuserid;
    private String transferdate;
    private String transferredToClientID;
    private String transferredToChargeID;
    private String depositDate;
    private String checkNumber;
    private String voucherNumber;
    private String lastUserID;
    private String eOMReportingDate;
    private String paymentSource;
    private String denialReasonCode;
    private String transferringTransactionID;
    private String paymentCaseID;
    private String denialGroupCode;
    private Insurance insurance;
    private Boolean mbx;

    public String getBillingTransactionId() {
        return billingTransactionId;
    }

    public void setBillingTransactionId(String billingTransactionId) {
        this.billingTransactionId = billingTransactionId;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public int getChargeID() {
        return chargeID;
    }

    public void setChargeID(int chargeID) {
        this.chargeID = chargeID;
    }

    public int getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(int transactionType) {
        this.transactionType = transactionType;
    }

    public int getStatusID() {
        return statusID;
    }

    public void setStatusID(int statusID) {
        this.statusID = statusID;
    }

    public String getClientID() {
        return clientID;
    }

    public void setClientID(String clientID) {
        this.clientID = clientID;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }

    public String getPatientID() {
        return patientID;
    }

    public void setPatientID(String patientID) {
        this.patientID = patientID;
    }

    public String getGuarantorID() {
        return guarantorID;
    }

    public void setGuarantorID(String guarantorID) {
        this.guarantorID = guarantorID;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getPaymentID() {
        return paymentID;
    }

    public void setPaymentID(String paymentID) {
        this.paymentID = paymentID;
    }

    public String getReversalOperator() {
        return reversalOperator;
    }

    public void setReversalOperator(String reversalOperator) {
        this.reversalOperator = reversalOperator;
    }

    public String getTransTypeID() {
        return transTypeID;
    }

    public void setTransTypeID(String transTypeID) {
        this.transTypeID = transTypeID;
    }

    public String getTransactionCode() {
        return transactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        this.transactionCode = transactionCode;
    }

    public String getReportClass() {
        return reportClass;
    }

    public void setReportClass(String reportClass) {
        this.reportClass = reportClass;
    }


    public String geteOBID() {
        return eOBID;
    }

    public void seteOBID(String eOBID) {
        this.eOBID = eOBID;
    }

    public String getScanPagenumber() {
        return scanPagenumber;
    }

    public void setScanPagenumber(String scanPagenumber) {
        this.scanPagenumber = scanPagenumber;
    }

    public String getRemittanceBatchID() {
        return remittanceBatchID;
    }

    public void setRemittanceBatchID(String remittanceBatchID) {
        this.remittanceBatchID = remittanceBatchID;
    }

    public String getRemittanceLineNumber() {
        return remittanceLineNumber;
    }

    public void setRemittanceLineNumber(String remittanceLineNumber) {
        this.remittanceLineNumber = remittanceLineNumber;
    }

    public String getRefundCheckNumber() {
        return refundCheckNumber;
    }

    public void setRefundCheckNumber(String refundCheckNumber) {
        this.refundCheckNumber = refundCheckNumber;
    }

    public String getBatchChildImageName() {
        return batchChildImageName;
    }

    public void setBatchChildImageName(String batchChildImageName) {
        this.batchChildImageName = batchChildImageName;
    }

    public String getDeductibleAmount() {
        return deductibleAmount;
    }

    public void setDeductibleAmount(String deductibleAmount) {
        this.deductibleAmount = deductibleAmount;
    }

    public String getAllowedAmount() {
        return allowedAmount;
    }

    public void setAllowedAmount(String allowedAmount) {
        this.allowedAmount = allowedAmount;
    }

    public String getCoinsuranceAmount() {
        return coinsuranceAmount;
    }

    public void setCoinsuranceAmount(String coinsuranceAmount) {
        this.coinsuranceAmount = coinsuranceAmount;
    }

    public String getPaymentVariancePercent() {
        return paymentVariancePercent;
    }

    public void setPaymentVariancePercent(String paymentVariancePercent) {
        this.paymentVariancePercent = paymentVariancePercent;
    }

    public String getExpectedpayment() {
        return expectedpayment;
    }

    public void setExpectedpayment(String expectedpayment) {
        this.expectedpayment = expectedpayment;
    }

    public String getPatientPaymentID() {
        return patientPaymentID;
    }

    public void setPatientPaymentID(String patientPaymentID) {
        this.patientPaymentID = patientPaymentID;
    }

    public String getKeyInDateTime() {
        return keyInDateTime;
    }

    public void setKeyInDateTime(String keyInDateTime) {
        this.keyInDateTime = keyInDateTime;
    }

    public String getDenialReasonID() {
        return denialReasonID;
    }

    public void setDenialReasonID(String denialReasonID) {
        this.denialReasonID = denialReasonID;
    }

    public String getCreditCardType() {
        return creditCardType;
    }

    public void setCreditCardType(String creditCardType) {
        this.creditCardType = creditCardType;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getCreditCardExpirationDate() {
        return creditCardExpirationDate;
    }

    public void setCreditCardExpirationDate(String creditCardExpirationDate) {
        this.creditCardExpirationDate = creditCardExpirationDate;
    }

    public String getTransferred() {
        return transferred;
    }

    public void setTransferred(String transferred) {
        this.transferred = transferred;
    }

    public String getTransferuserid() {
        return transferuserid;
    }

    public void setTransferuserid(String transferuserid) {
        this.transferuserid = transferuserid;
    }

    public String getTransferdate() {
        return transferdate;
    }

    public void setTransferdate(String transferdate) {
        this.transferdate = transferdate;
    }

    public String getTransferredToClientID() {
        return transferredToClientID;
    }

    public void setTransferredToClientID(String transferredToClientID) {
        this.transferredToClientID = transferredToClientID;
    }

    public String getTransferredToChargeID() {
        return transferredToChargeID;
    }

    public void setTransferredToChargeID(String transferredToChargeID) {
        this.transferredToChargeID = transferredToChargeID;
    }

    public String getDepositDate() {
        return depositDate;
    }

    public void setDepositDate(String depositDate) {
        this.depositDate = depositDate;
    }

    public String getCheckNumber() {
        return checkNumber;
    }

    public void setCheckNumber(String checkNumber) {
        this.checkNumber = checkNumber;
    }

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getLastUserID() {
        return lastUserID;
    }

    public void setLastUserID(String lastUserID) {
        this.lastUserID = lastUserID;
    }

    public String geteOMReportingDate() {
        return eOMReportingDate;
    }

    public void seteOMReportingDate(String eOMReportingDate) {
        this.eOMReportingDate = eOMReportingDate;
    }

    public String getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(String paymentSource) {
        this.paymentSource = paymentSource;
    }

    public String getDenialReasonCode() {
        return denialReasonCode;
    }

    public void setDenialReasonCode(String denialReasonCode) {
        this.denialReasonCode = denialReasonCode;
    }

    public String getTransferringTransactionID() {
        return transferringTransactionID;
    }

    public void setTransferringTransactionID(String transferringTransactionID) {
        this.transferringTransactionID = transferringTransactionID;
    }

    public String getPaymentCaseID() {
        return paymentCaseID;
    }

    public void setPaymentCaseID(String paymentCaseID) {
        this.paymentCaseID = paymentCaseID;
    }

    public String getDenialGroupCode() {
        return denialGroupCode;
    }

    public void setDenialGroupCode(String denialGroupCode) {
        this.denialGroupCode = denialGroupCode;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getDateApplied() {
        return dateApplied;
    }

    public void setDateApplied(String dateApplied) {
        this.dateApplied = dateApplied;
    }

    public String getCheckdate() {
        return checkdate;
    }

    public void setCheckdate(String checkdate) {
        this.checkdate = checkdate;
    }

    public String getReversalDate() {
        return reversalDate;
    }

    public void setReversalDate(String reversalDate) {
        this.reversalDate = reversalDate;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Insurance getInsurance() {
        return insurance;
    }

    public void setInsurance(Insurance insurance) {
        this.insurance = insurance;
    }

    public Boolean getMbx() {
        return mbx;
    }

    public void setMbx(Boolean mbx) {
        this.mbx = mbx;
    }
}

