<%--
  Created by IntelliJ IDEA.
  User: Masoud
  Date: 2/12/15
  Time: 10:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>
<head>
    <title>
        <tiles:insertAttribute name="title" />
    </title>

    <%--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular.js"></script>--%>
    <%--<script src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.0/angular-animate.js"></script>--%>
    <%--<script src="//angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.2.4.js"></script>--%>
    <%--<link href="//netdna.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">--%>

    <link href="http://fonts.googleapis.com/css?family=Lato:100,300,400,700" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/font-awesome.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/se7en-font.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/isotope.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/jquery.fancybox.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/fullcalendar.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/wizard.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/select2.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/morris.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/datatables.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/datepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/timepicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/colorpicker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap-switch.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/bootstrap-editable.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/daterange-picker.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/typeahead.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/summernote.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/ladda-themeless.min.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/social-buttons.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/pygments.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/style.css" media="all" rel="stylesheet" type="text/css" />
    <link href="/template/css/color/green.css" media="all" rel="alternate stylesheet" title="green-theme" type="text/css" />
    <link href="/template/css/color/orange.css" media="all" rel="alternate stylesheet" title="orange-theme" type="text/css" />
    <link href="/template/css/color/magenta.css" media="all" rel="alternate stylesheet" title="magenta-theme" type="text/css" />
    <link href="/template/css/color/gray.css" media="all" rel="alternate stylesheet" title="gray-theme" type="text/css" />
    <link href="/template/css/jquery.fileupload-ui.css" media="screen" rel="stylesheet" type="text/css" />
    <link href="/template/css/dropzone.css" media="screen" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Italianno" />
    <script src="http://code.jquery.com/jquery-1.10.2.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/template/js/raphael.min.js" type="text/javascript"></script>
    <script src="/template/js/selectivizr-min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.mousewheel.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.sampledata.js" type="text/javascript"></script>
    <script src="/template/js/jquery.vmap.world.js" type="text/javascript"></script>
    <script src="/template/js/jquery.bootstrap.wizard.js" type="text/javascript"></script>
    <script src="/template/js/fullcalendar.min.js" type="text/javascript"></script>
    <script src="/template/js/gcal.js" type="text/javascript"></script>
    <script src="/template/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="/template/js/datatable-editable.js" type="text/javascript"></script>
    <script src="/template/js/jquery.easy-pie-chart.js" type="text/javascript"></script>
    <script src="/template/js/excanvas.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.isotope.min.js" type="text/javascript"></script>
    <script src="/template/js/isotope_extras.js" type="text/javascript"></script>
    <script src="/template/js/modernizr.custom.js" type="text/javascript"></script>
    <script src="/template/js/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="/template/js/select2.js" type="text/javascript"></script>
    <script src="/template/js/styleswitcher.js" type="text/javascript"></script>
    <script src="/template/js/wysiwyg.js" type="text/javascript"></script>
    <script src="/template/js/typeahead.js" type="text/javascript"></script>
    <script src="/template/js/summernote.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.inputmask.min.js" type="text/javascript"></script>
    <script src="/template/js/jquery.validate.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-fileupload.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-timepicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-colorpicker.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <script src="/template/js/typeahead.js" type="text/javascript"></script>
    <script src="/template/js/spin.min.js" type="text/javascript"></script>
    <script src="/template/js/ladda.min.js" type="text/javascript"></script>
    <script src="/template/js/moment.js" type="text/javascript"></script>
    <script src="/template/js/mockjax.js" type="text/javascript"></script>
    <script src="/template/js/bootstrap-editable.min.js" type="text/javascript"></script>
    <script src="/template/js/xeditable-demo-mock.js" type="text/javascript"></script>
    <script src="/template/js/xeditable-demo.js" type="text/javascript"></script>
    <script src="/template/js/address.js" type="text/javascript"></script>
    <script src="/template/js/daterange-picker.js" type="text/javascript"></script>
    <script src="/template/js/date.js" type="text/javascript"></script>
    <script src="/template/js/morris.min.js" type="text/javascript"></script>
    <script src="/template/js/skycons.js" type="text/javascript"></script>
    <script src="/template/js/fitvids.js" type="text/javascript"></script>
    <script src="/template/js/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="/template/js/dropzone.js" type="text/javascript"></script>
    <script src="/template/js/main.js" type="text/javascript"></script>
    <script src="/template/js/respond.js" type="text/javascript"></script>
    <%--d3 dependencies--%>
    <link href="/template/css/hexbin.css" media="screen" rel="stylesheet" type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
    <%--Google Analytics--%>
    <script src="/template/js/googleAnalytics.js" type="text/javascript"></script>
    <%--Angular Declaration --%>
    <script src="/template/js/angular.js"></script>
    <script src="/template/js/angular-round-progress-directive.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-animate.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-route.min.js"></script>
    <link rel="stylesheet" href="/template/css/animations.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css">
    <tiles:insertAttribute name="customCssAndJsInclude" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport">
</head>
<body class="page-header-fixed bg-1">
<tiles:insertAttribute name="body" />
<div class="modal-shiftfix">
<!-- Navigation -->
<div class="navbar navbar-fixed-top scroll-hide">
<div class="container-fluid top-bar">
    <div class="pull-right">
        <ul class="nav navbar-nav pull-right">
            <li class="dropdown notifications hidden-xs">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="se7en-flag"></span>
                    <div class="sr-only">
                        Notifications
                    </div>
                    <p class="counter">
                        4
                    </p>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="#">
                        <div class="notifications label label-info">
                            New
                        </div>
                        <p>
                            New user added: Jane Smith
                        </p></a>

                    </li>
                    <li><a href="#">
                        <div class="notifications label label-info">
                            New
                        </div>
                        <p>
                            Sales targets available
                        </p></a>

                    </li>
                    <li><a href="#">
                        <div class="notifications label label-info">
                            New
                        </div>
                        <p>
                            New performance metric added
                        </p></a>

                    </li>
                    <li><a href="#">
                        <div class="notifications label label-info">
                            New
                        </div>
                        <p>
                            New growth data available
                        </p></a>

                    </li>
                </ul>
            </li>
            <li class="dropdown messages hidden-xs">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#"><span aria-hidden="true" class="se7en-envelope"></span>
                    <div class="sr-only">
                        Messages
                    </div>
                    <p class="counter">
                        3
                    </p>
                </a>
                <ul class="dropdown-menu messages">
                    <li><a href="#">
                        <img width="34" height="34" src="/template/images/avatar-male2.png" />Could we meet today? I wanted...</a>
                    </li>
                    <li><a href="#">
                        <img width="34" height="34" src="/template/images/avatar-female.png" />Important data needs your analysis...</a>
                    </li>
                    <li><a href="#">
                        <img width="34" height="34" src="/template/images/avatar-male2.png" />Buy Se7en today, it's a great theme...</a>
                    </li>
                </ul>
            </li>
            <li class="dropdown user hidden-xs"><a data-toggle="dropdown" class="dropdown-toggle" href="#">
                <img width="34" height="34" src="/template/images/avatar-male.jpg" /><c:out value="${person.personInfo.personName.personFullName}" /><b class="caret"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="#">
                        <i class="fa fa-user"></i>My Account</a>
                    </li>
                    <li><a href="#">
                        <i class="fa fa-gear"></i>Account Settings</a>
                    </li>
                    <li><a href="/logout.htm">
                        <i class="fa fa-sign-out"></i>Logout</a>
                    </li>

                </ul>
            </li>
        </ul>
    </div>
    <button class="navbar-toggle"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="logo" href="index.html">se7en</a>
    <form class="navbar-form form-inline col-lg-2 hidden-xs">
        <input class="form-control" placeholder="Search" type="text">
    </form>
</div>
<div class="container-fluid main-nav clearfix">
<div class="nav-collapse">
<ul class="nav">
    <li>
        <a  href="/dashboard.htm"><span aria-hidden="true" class="se7en-home"></span>Dashboard</a>
    </li>

    <c:if test="${admin}">
        <li class="dropdown"><a data-toggle="dropdown" class="current" href="#">
            <span aria-hidden="true" class="fa fa-gear"></span>Settings<b class="caret"></b></a>
            <ul class="dropdown-menu">
                <li><a href="/admin/devices.htm">
                    <p>
                        Devices
                    </p></a>

                </li>
                <li><a href="/admin/roles.htm">
                    <p>
                        Users and Groups
                    </p></a>
                </li>
            </ul>
        </li>
    </c:if>

    <%--<c:forEach var="role" items="${roleList}">--%>
        <%--&lt;%&ndash;<h4>${role.role}</h4>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<li><c:out value="${role.role}" /></li>&ndash;%&gt;--%>
        <%--&lt;%&ndash;<c:choose>&ndash;%&gt;--%>
            <%--<c:if test="${role.role=='ROLE_ADMIN'}">--%>


            <%--<li class="dropdown"><a data-toggle="dropdown" href="#">--%>
                <%--<span aria-hidden="true" class="fa fa-gear"></span>Settings<b class="caret"></b></a>--%>
                <%--<ul class="dropdown-menu">--%>
                    <%--<li><a href="/admin/devices.htm">--%>
                        <%--<p>--%>
                            <%--Devices--%>
                        <%--</p></a>--%>

                    <%--</li>--%>
                    <%--<li><a href="/admin/roles.htm">--%>
                        <%--<p>--%>
                            <%--Users and Groups--%>
                        <%--</p></a>--%>
                    <%--</li>--%>
                <%--</ul>--%>
            <%--</li>--%>
            <%--</c:if>--%>
            <%--<c:otherwise>--%>

            <%--</c:otherwise>--%>
        <%--</c:choose>--%>
    <%--</c:forEach>--%>

    <li><a href="social.html">
        <span aria-hidden="true" class="se7en-feed"></span>Social Feed</a>
    </li>
    <li class="dropdown"><a data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="se7en-star"></span>UI Features<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="buttons.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Buttons
                </p></a>

            </li>
            <li><a href="fontawesome.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Font Awesome Icons
                </p></a>

            </li>
            <li><a href="glyphicons.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Glyphicons
                </p></a>

            </li>
            <li>
                <a href="components.html">Components</a>
            </li>
            <li>
                <a href="widgets.html">Widgets</a>
            </li>
            <li>
                <a href="typo.html">Typography</a>
            </li>
            <li>
                <a href="grid.html">Grid Layout</a>
            </li>
        </ul>
    </li>
    <li class="dropdown"><a data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="se7en-forms"></span>Forms<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="form-components.html">Form Components</a>
            </li>
            <li>
                <a href="form-advanced.html">Advanced Forms</a>
            </li>
            <li><a href="xeditable.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    X-Editable
                </p></a>

            </li>
            <li><a href="file-upload.html">
                <div class="notifications label label-warning">
                    New
                </div>
                <p>
                    Multiple File Upload
                </p></a>

            </li>
            <li><a href="dropzone-file-upload.html">
                <div class="notifications label label-warning">
                    New
                </div>
                <p>
                    Dropzone File Upload
                </p></a>

            </li>
        </ul>
    </li>
    <li class="dropdown"><a data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="se7en-tables"></span>Tables<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li>
                <a href="tables.html">Basic tables</a>
            </li>
            <li>
                <a href="datatables.html">DataTables</a>
            </li>
            <li><a href="datatables-editable.html">
                <div class="notifications label label-warning">
                    New
                </div>
                <p>
                    Editable DataTables
                </p></a>

            </li>
        </ul>
    </li>
    <li><a href="charts.html">
        <span aria-hidden="true" class="se7en-charts"></span>Charts</a>
    </li>
    <li class="dropdown"><a data-toggle="dropdown" href="#">
        <span aria-hidden="true" class="se7en-pages"></span>Pages<b class="caret"></b></a>
        <ul class="dropdown-menu">
            <li><a href="chat.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Chat
                </p></a>

            </li>
            <li>
                <a href="calendar.html">Calendar</a>
            </li>
            <li><a href="timeline.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Timeline
                </p></a>

            </li>
            <li><a href="login1.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Login 1
                </p></a>

            </li>
            <li>
                <a href="login2.html">Login 2</a>
            </li>
            <li><a href="signup1.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Sign Up 1
                </p></a>

            </li>
            <li><a href="messages.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Messages/Inbox
                </p></a>

            </li>
            <li><a href="pricing.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Pricing Tables
                </p></a>

            </li>
            <li>
                <a href="signup2.html">Sign Up 2</a>
            </li>
            <li><a href="invoice.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    Invoice
                </p></a>

            </li>
            <li><a href="faq.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    FAQ
                </p></a>

            </li>
            <li>
                <a href="filters.html">Filter Results</a>
            </li>
            <li>
                <a href="404-page.html">404 Page</a>
            </li>
            <li><a href="500-page.html">
                <span class="notifications label label-warning">New</span>
                <p>
                    500 Error
                </p></a>

            </li>
        </ul>
    </li>
    <li><a href="gallery.html">
        <span aria-hidden="true" class="se7en-gallery"></span>Gallery</a>
    </li>
</ul>
</div>
</div>
</div>
<!-- End Navigation -->





</div>
<div class="style-selector">
    <div class="style-selector-container">
        <h2>
            Layout Style
        </h2>
        <select name="layout"><option value="fluid">Fluid</option><option value="boxed">Boxed</option></select>
        <h2>
            Navigation Style
        </h2>
        <select name="nav"><option value="top">Top</option><option value="left">Left</option></select>
        <h2>
            Color Options
        </h2>
        <ul class="color-options clearfix">
            <li>
                <a class="blue" href="javascript:chooseStyle('none', 30)"></a>
            </li>
            <li>
                <a class="green" href="javascript:chooseStyle('green-theme', 30)"></a>
            </li>
            <li>
                <a class="orange" href="javascript:chooseStyle('orange-theme', 30)"></a>
            </li>
            <li>
                <a class="magenta" href="javascript:chooseStyle('magenta-theme', 30)"></a>
            </li>
            <li>
                <a class="gray" href="javascript:chooseStyle('gray-theme', 30)"></a>
            </li>
        </ul>
        <h2>
            Background Patterns
        </h2>
        <ul class="pattern-options clearfix">
            <li>
                <a class="active" href="#" id="bg-1"></a>
            </li>
            <li>
                <a href="#" id="bg-2"></a>
            </li>
            <li>
                <a href="#" id="bg-3"></a>
            </li>
            <li>
                <a href="#" id="bg-4"></a>
            </li>
            <li>
                <a href="#" id="bg-5"></a>
            </li>
        </ul>
        <div class="style-toggle closed">
            <span aria-hidden="true" class="se7en-gear"></span>
        </div>
    </div>
</div>
</body>
</html>
