package com.neunou.wi.Config;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.ldap.userdetails.LdapUserDetailsImpl;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Logger;

/**
 * Created by Masoud on 2/12/15.
 */
@Component
public class MyAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler{
    private static final Logger LOG = Logger.getLogger(MyAuthenticationSuccessHandler.class.getName());



    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {

        setDefaultTargetUrl("/dashboard.htm");
        super.onAuthenticationSuccess(request, response, authentication);
        System.out.println("Login Success...");

        LdapUserDetailsImpl ldapUser = (LdapUserDetailsImpl)  SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userDN = ldapUser.getDn();
        String username = ldapUser.getUsername();

        System.out.println("DC is: "+userDN);
        LOG.info("Authenticating "+ldapUser.getUsername());


        }



//        Person person = new GetAuthPerson().getPerson();
//        Person personFromDB = personManager.getPersonByDN(person.getPersonInfo().getDn());
//        LOG.info(personFromDB.getPersonInfo().getPersonName().getFullName()+" is login to the application at "+new Date());
//        List<Date> webLogins  = personFromDB.getWebLoginDates();
//        webLogins.add(new Date());
//        personFromDB.setWebLoginDates(webLogins);
//        this.personManager.update(personFromDB);
    }

