package com.neunou.wi.API;

import com.neunou.hl7manager.interfaces.PhysicianScheduleManager;
import com.neunou.schema.PhysicianSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 4/1/15.
 */
@Controller
@RequestMapping(value="/api/physicianSchedule")
public class PhysicianScheduleAPI {
    private static final Logger LOG = Logger.getLogger(PhysicianAPI.class.getName());
    @Autowired
    private PhysicianScheduleManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PhysicianSchedule> getAllPhysiciansSchedule(){
        LOG.info("Getting all of the physicians schedules");
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE)
    public String savePhysicianSchedule(@RequestBody PhysicianSchedule schedule){
        man.add(schedule);
        return "dashboard";
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{physicianScheduleId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PhysicianSchedule getPhysicianScheduleById(@PathVariable("physicianScheduleId") String physicianScheduleId){
        return (PhysicianSchedule)man.getById(physicianScheduleId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{physicianScheduleId:.+}")
    public void deletePhysicianScheduleById (@PathVariable("physicianScheduleId") String physicianScheduleId){
        man.delete(man.getById(physicianScheduleId));
    }

    @RequestMapping(method = RequestMethod.POST, value = "/update", consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updatePhysician(@RequestBody PhysicianSchedule schedule){
        this.man.update(schedule);
    }
}
