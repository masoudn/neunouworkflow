package com.neunou.schema;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Masoud on 3/17/15.
 */
public class PhoneCallRecords {
    private String phoneCallRecordId;
    private String recordId;
    private String extension;
    private String partyName;
    private String dialedNumber;
    private int talkTimeSeconds;
    private String callType;
    private String day;
    private String date;
    private String time;
    private String holdTime;
    private int holdTimeSeconds;
    private String connectType;
    private String disconnectType;
    private int year;
    private String month;
    private String yearMonth;
    private BigDecimal talkTimeMinutes;
    private int holdTimeMinutes;
    private Date creationDate;


    public String getPhoneCallRecordId() {
        return phoneCallRecordId;
    }

    public void setPhoneCallRecordId(String phoneCallRecordId) {
        this.phoneCallRecordId = phoneCallRecordId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getDialedNumber() {
        return dialedNumber;
    }

    public void setDialedNumber(String dialedNumber) {
        this.dialedNumber = dialedNumber;
    }

    public int getTalkTimeSeconds() {
        return talkTimeSeconds;
    }

    public void setTalkTimeSeconds(int talkTimeSeconds) {
        this.talkTimeSeconds = talkTimeSeconds;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getHoldTime() {
        return holdTime;
    }

    public void setHoldTime(String holdTime) {
        this.holdTime = holdTime;
    }

    public int getHoldTimeSeconds() {
        return holdTimeSeconds;
    }

    public void setHoldTimeSeconds(int holdTimeSeconds) {
        this.holdTimeSeconds = holdTimeSeconds;
    }

    public String getDisconnectType() {
        return disconnectType;
    }

    public void setDisconnectType(String disconnectType) {
        this.disconnectType = disconnectType;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYearMonth() {
        return yearMonth;
    }

    public void setYearMonth(String yearMonth) {
        this.yearMonth = yearMonth;
    }

    public BigDecimal getTalkTimeMinutes() {
        return talkTimeMinutes;
    }

    public void setTalkTimeMinutes(BigDecimal talkTimeMinutes) {
        this.talkTimeMinutes = talkTimeMinutes;
    }

    public int getHoldTimeMinutes() {
        return holdTimeMinutes;
    }

    public void setHoldTimeMinutes(int holdTimeMinutes) {
        this.holdTimeMinutes = holdTimeMinutes;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getRecordId() {
        return recordId;
    }

    public void setRecordId(String recordId) {
        this.recordId = recordId;
    }

    public String getPartyName() {
        return partyName;
    }

    public void setPartyName(String partyName) {
        this.partyName = partyName;
    }

    public String getConnectType() {
        return connectType;
    }

    public void setConnectType(String connectType) {
        this.connectType = connectType;
    }
}
