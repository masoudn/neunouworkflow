package com.neunou.schema;

import java.math.BigDecimal;

/**
 * Created by root on 7/10/15.
 */
public class TimeLogStat {
    private BigDecimal avgPersonalOTPerShift;
    private BigDecimal totalPersonalOT;
    private BigDecimal avgOTPerShift;
    private BigDecimal avgOTPerDoc;
    private BigDecimal totalPracticeOT;

    public BigDecimal getAvgPersonalOTPerShift() {
        return avgPersonalOTPerShift;
    }

    public void setAvgPersonalOTPerShift(BigDecimal avgPersonalOTPerShift) {
        this.avgPersonalOTPerShift = avgPersonalOTPerShift;
    }

    public BigDecimal getTotalPersonalOT() {
        return totalPersonalOT;
    }

    public void setTotalPersonalOT(BigDecimal totalPersonalOT) {
        this.totalPersonalOT = totalPersonalOT;
    }

    public BigDecimal getAvgOTPerShift() {
        return avgOTPerShift;
    }

    public void setAvgOTPerShift(BigDecimal avgOTPerShift) {
        this.avgOTPerShift = avgOTPerShift;
    }

    public BigDecimal getAvgOTPerDoc() {
        return avgOTPerDoc;
    }

    public void setAvgOTPerDoc(BigDecimal avgOTPerDoc) {
        this.avgOTPerDoc = avgOTPerDoc;
    }

    public BigDecimal getTotalPracticeOT() {
        return totalPracticeOT;
    }

    public void setTotalPracticeOT(BigDecimal totalPracticeOT) {
        this.totalPracticeOT = totalPracticeOT;
    }
}
