<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <tiles:insertAttribute name="title" />
    </title>
    <style>

        .mesh {
            fill: none;
            stroke: #000;
            stroke-width: .25px;
        }

        .start {
            fill: none;
            stroke: brown;
        }

        .end {
            fill: none;
            stroke: steelblue;
        }

    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>
    <script src="/template/js/d3.hexbin.min.js"></script>


    <script>

        var width = 960,
                height = 500,
                radius = 10;

        var p0 = [250, 200, 60],
                p1 = [560, 300, 120];

        var svg = d3.select("body").append("svg")
                .attr("width", width)
                .attr("height", height)
                .append("g")
                .call(transition, p0, p1);

        svg.append("path")
                .attr("class", "mesh")
                .attr("d", d3.hexbin()
                        .size([width, height])
                        .radius(radius)
                        .mesh);

        svg.selectAll("circle")
                .data([p0, p1])
                .enter().append("circle")
                .attr("class", function(d, i) { return i ? "end" : "start"; })
                .attr("cx", function(d) { return d[0]; })
                .attr("cy", function(d) { return d[1]; })
                .attr("r", function(d) { return d[2] / 2 - .5; });

        function transition(svg, start, end) {
            var center = [width / 2, height / 2],
                    i = d3.interpolateZoom(start, end);

            svg
                    .attr("transform", transform(start))
                    .transition()
                    .delay(250)
                    .duration(i.duration * 2)
                    .attrTween("transform", function() { return function(t) { return transform(i(t)); }; })
                    .each("end", function() { d3.select(this).call(transition, end, start); });

            function transform(p) {
                var k = height / p[2];
                return "translate(" + (center[0] - p[0] * k) + "," + (center[1] - p[1] * k) + ")scale(" + k + ")";
            }
        }

    </script>

    <%--d3 dependencies--%>
    <%--<link href="/template/css/hexbin.css" media="screen" rel="stylesheet" type="text/css" />--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>--%>
    <%--&lt;%&ndash;Google Analytics&ndash;%&gt;--%>
    <%--<script src="/template/js/googleAnalytics.js" type="text/javascript"></script>--%>
    <%--&lt;%&ndash;Angular Declaration &ndash;%&gt;--%>
    <%--<script src="/template/js/angular.js"></script>--%>
    <%--<script src="/template/js/angular-round-progress-directive.js"></script>--%>
    <%--<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-animate.js"></script>--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.13/angular-route.min.js"></script>--%>
    <%--<link rel="stylesheet" href="/template/css/animations.css">--%>
    <%--&lt;%&ndash;<link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/animate.css/2.0/animate.min.css">&ndash;%&gt;--%>
    <%--&lt;%&ndash;d3 dependencies&ndash;%&gt;--%>
    <%--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.5/d3.min.js"></script>--%>
    <%--<script src="/template/js/d3.hexbin.min.js"></script>--%>
    <%--<script src="/template/js/GraphScript.js"></script>--%>
</head>
</div>
<!-- End Navigation -->

<tiles:insertAttribute name="body" />

</div>
</html>