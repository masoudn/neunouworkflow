package com.neunou.schema;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Masoud on 9/12/14.
 */
public class Visit {
    private String visitId;
    private List<Exam> exams;
    private VisitStatus visitStatus;
    private String visitNumber;
    private List<HL7Document> hl7Documents;
    private String accessionNumber;
    private String physicianId;
    private String readingPhysicianId;
    private Map<String,String> report;
    private Date dictationDate;
    private Date schedueledDate;
    private Date creationDate;
    private Date modifiedDate;

    public String getVisitId() {
        return visitId;
    }

    public void setVisitId(String visitId) {
        this.visitId = visitId;
    }

    public List<Exam> getExams() {
        return exams;
    }

    public void setExams(List<Exam> exams) {
        this.exams = exams;
    }

    public VisitStatus getVisitStatus() {
        return visitStatus;
    }

    public void setVisitStatus(VisitStatus visitStatus) {
        this.visitStatus = visitStatus;
    }

    public String getAccessionNumber() {
        return accessionNumber;
    }

    public void setAccessionNumber(String accessionNumber) {
        this.accessionNumber = accessionNumber;
    }

    public Date getSchedueledDate() {
        return schedueledDate;
    }

    public void setSchedueledDate(Date schedueledDate) {
        this.schedueledDate = schedueledDate;
    }

    public String getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getVisitNumber() {
        return visitNumber;
    }

    public void setVisitNumber(String visitNumber) {
        this.visitNumber = visitNumber;
    }

    public List<HL7Document> getHl7Documents() {
        return hl7Documents;
    }

    public void setHl7Documents(List<HL7Document> hl7Documents) {
        this.hl7Documents = hl7Documents;
    }

    public Map<String, String> getReport() {
        return report;
    }

    public void setReport(Map<String, String> report) {
        this.report = report;
    }

    public String getReadingPhysicianId() {
        return readingPhysicianId;
    }

    public void setReadingPhysicianId(String readingPhysicianId) {
        this.readingPhysicianId = readingPhysicianId;
    }

    public Date getDictationDate() {
        return dictationDate;
    }

    public void setDictationDate(Date dictationDate) {
        this.dictationDate = dictationDate;
    }
}
