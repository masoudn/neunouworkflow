package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.GroupsDAO;
import com.neunou.schema.Groups;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by Masoud on 3/19/15.
 */
@Repository
public class GroupsDAOImpl implements GroupsDAO {
    private static final Logger LOG = Logger.getLogger(GroupsDAOImpl.class.getName());
    @Autowired
    private MongoTemplate mongoTemplate;
    private String GROUP_COL = "groups";

    @Override
    public String add(Object object) {
        LOG.info("Adding objects to the DB...");
        Groups group = (Groups) object;
        if(group.getGroupId()==null) {
            group.setGroupId(UUID.randomUUID().toString());
            group.setCreationDate(new Date());
            group.setExcluded(Boolean.FALSE);
            if (!mongoTemplate.collectionExists(GROUP_COL)) {
                mongoTemplate.createCollection(GROUP_COL);
                mongoTemplate.insert(group, GROUP_COL);
                return group.getGroupId();
            } else {
                mongoTemplate.insert(group, GROUP_COL);
                return group.getGroupId();
            }
        }
            else {
                this.update(object);
                return group.getGroupId();
            }
    }

    @Override
    public void delete(Object object) {
        Groups group = (Groups) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("groupId").is(group.getGroupId()));

        mongoTemplate.remove(query,Groups.class,GROUP_COL);
    }

    @Override
    public void update(Object object) {
        Groups group = (Groups) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("groupId").is(group.getGroupId()));

        group.setModifiedDate(new Date());
        Update update = new Update();
        update.set("groupDesc",group.getGroupDesc());
        update.set("companyName",group.getCompanyName());
        update.set("roles",group.getRoles());
        update.set("modificationDate",group.getModifiedDate());
        update.set("excluded",group.getExcluded());

        mongoTemplate.findAndModify(query,update,Groups.class,GROUP_COL);
    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("groupId").is(id));
        return mongoTemplate.findOne(query,Groups.class,GROUP_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(Groups.class,GROUP_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,Groups.class,GROUP_COL);
        return exists;
    }
}
