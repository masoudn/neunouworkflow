package com.neunou.hl7manager.Impl;

import com.neunou.hl7manager.interfaces.PhysicianManager;
import com.neunou.hl7persistence.Interfaces.PhysicianDAO;
import com.neunou.schema.Physician;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by Masoud on 4/1/15.
 */
@Component
public class PhysicianManagerImpl implements PhysicianManager {
    @Autowired
    private PhysicianDAO dao;
    private Physician physician;
    private static final Logger LOG = Logger.getLogger(PhysicianManagerImpl.class.getName());
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return dao.getAll();
    }

    @Override
    public int count() {
        return dao.count();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return dao.existance(element,elementValue);
    }

    @Override
    public Physician getPhysicianByNPI(String npi) {
        return dao.getPhysicianByNPI(npi);
    }

    @Override
    public Physician getPhysicianByName(String fName, String lName) {
        return dao.getPhysicianByName(fName,lName);
    }
}
