package com.neunou.schema;

import java.util.Date;
import java.util.Map;

/**
 * Created by root on 3/7/16.
 */
public class DICOMDirInfo {
    private String dicomDirId;
    private String filePath;
    private Map<String,Boolean> targetServer;
    private Date createdDate;
    private Date modifiedDate;


    public String getDicomDirId() {
        return dicomDirId;
    }

    public void setDicomDirId(String dicomDirId) {
        this.dicomDirId = dicomDirId;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public Map<String, Boolean> getTargetServer() {
        return targetServer;
    }

    public void setTargetServer(Map<String, Boolean> targetServer) {
        this.targetServer = targetServer;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
}
