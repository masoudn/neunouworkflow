package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.TimeLogDAO;
import com.neunou.schema.TimeLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 6/5/15.
 */
@Repository
public class TimeLogDAOImpl implements TimeLogDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String TIME_COL = "timeLog";
    @Override
    public String add(Object object) {
        TimeLog timeLog = (TimeLog) object;
        if (timeLog.getTimeLogId()==null&&timeLog.getEndTime()==null){
            timeLog.setTimeLogId(UUID.randomUUID().toString());
            timeLog.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(TIME_COL)){
                mongoTemplate.createCollection(TIME_COL);
                mongoTemplate.insert(timeLog,TIME_COL);
                return timeLog.getTimeLogId();
            }
            else {
                mongoTemplate.insert(timeLog,TIME_COL);
                return timeLog.getTimeLogId();
            }
        }
        else{
            this.update(object);
            return timeLog.getTimeLogId();
        }

    }

    @Override
    public void delete(Object object) {
        TimeLog timeLog = (TimeLog) object;

        Query query = new Query();
        query.addCriteria(Criteria.where("timeLogId").is(timeLog.getTimeLogId()));

        mongoTemplate.remove(query,TimeLog.class,TIME_COL);
    }

    @Override
    public void update(Object object) {
        TimeLog timeLog = (TimeLog) object;

        Query query = new Query();
        query.addCriteria(Criteria.where("timeLogId").is(timeLog.getTimeLogId()));

        timeLog.setCreationDate(new Date());
        Update update = new Update();
        update.set("logDate",timeLog.getLogDate());
        update.set("startTime",timeLog.getStartTime());
        update.set("endTime",timeLog.getEndTime());
        update.set("personId",timeLog.getPersonId());
        update.set("creationDate",timeLog.getCreationDate());
        update.set("point",timeLog.getPoint());
        update.set("price",timeLog.getPrice());

        mongoTemplate.findAndModify(query,update,TimeLog.class,TIME_COL);

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("timeLogId").is(id));
        return mongoTemplate.findOne(query,TimeLog.class,TIME_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(TimeLog.class,TIME_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,TimeLog.class,TIME_COL);
        return exists;
    }

    @Override
    public List<TimeLog> getAllTimeLogsByPerson(String personId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("personId").is(personId));
        return mongoTemplate.find(query,TimeLog.class,TIME_COL);
    }
}
