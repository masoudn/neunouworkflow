<%--
  Created by IntelliJ IDEA.
  User: root
  Date: 6/9/15
  Time: 4:21 PM
  To change this template use File | Settings | File Templates.
--%>
<script>
    var app = angular.module("NeuNouApp",["ngAnimate",'ngRoute']);
    app.service("dataService",function($http){
        this.queryData = function(apiURL){
            return $http({
                method : 'GET',
                url: apiURL,
                headers: {'Content-Type': 'application/json'}
            })
                    .then(function(result){
                        console.log(result);
                        return result.data;

                    });
        }
        this.saveData = function(apiURL,element){
            console.log("saving the data ...");

            return $http({
                method: 'POST',
                url: apiURL,
                data: element,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result) {
                console.log(result);
            }, function(error) {
                console.log(error);
            });

        }

        this.startScript = function(apiURL){
            console.log("Calling the script ..."+apiURL);
            return $http({
                method: 'GET',
                url: apiURL,
                headers: {'Content-Type': 'application/json; charset=utf-8'}
            }).then(function(result) {
                console.log(result);
            }, function(error) {
                console.log(error);
            });
        }


    });


    app.controller("timeLogController",function(dataService,$scope, $interval){
        console.log("Loading TimeLogController...");

        $scope.format = 'M/d/yy h:mm:ss a';
        $scope.pendingTimeLog = {show: false};
        $scope.showFormBool = {show: false};
        $scope.showFormManualBool = {show:false};
        $scope.successNotification = {show: false};
        $scope.infoNotification = {show: false};
        $scope.manual = {};
        $scope.timeLog = {};
        $scope.manual.startDate;
        $scope.manual.startTime;
        $scope.manual.endDate;
        $scope.timeLogs = {};
        $scope.person = {};
        $scope.dateRange={};


        $scope.manualTime = {};
        $scope.getDate  = function(){
            console.log(new Date());
            return (new Date);
        }
        $scope.getPending = function(){
            $scope.infoNotification = {show: true};
            $scope.getPerson();
            console.log("Get Pending function...");
            dataService.queryData('/api/timelog/getPending').then(function(dataResponse){
                if (dataResponse.timeLogId != null ){
                    $scope.pendingTimeLog = true;
                    $scope.timeLog =  dataResponse;
                    console.log("Person ID is: "+$scope.timeLog.personId);
                    console.log("Start Time is: "+$scope.timeLog.startTime);
                    console.log("Time LOG ID is: "+$scope.timeLog.timeLogId);
                }
                else{
                    $scope.pendingTimeLog = false;
                }
                console.log($scope.pendingTimeLog);
            })
        }

        $scope.getPerson = function(){
            dataService.queryData('/api/person/getLoggedIn').then(function(dataResponse){
                $scope.person = dataResponse;
                console.log("LoggedinPerson is: "+$scope.person.personId);
            })
        }
        $scope.clockin = function(notes){
            $scope.timeLog.notes=notes;
            $scope.timeLog.logDate = new Date();
            $scope.timeLog.startTime = new Date();
            dataService.saveData("/api/timelog/save/",$scope.timeLog).then(function(dataResponse){
                $scope.getPending();

            });
        }
        $scope.clockout = function(id){
            console.log("Id is: "+$scope.timeLog.timeLogId);
            $scope.infoNotification = {show: false};
            $scope.timeLog.endTime = new Date();
            dataService.saveData("/api/timelog/update/",$scope.timeLog).then(function(dataResponse){
                console.log("Clocking out..."+new Date());
                $scope.getPending();
                $scope.successNotification = {show:true};
                var diff = new Date($scope.timeLog.endTime).getTime() - new Date($scope.timeLog.startTime).getTime();

                $scope.hourElapsed = Math.floor(diff/3600000);
                $scope.minElapsed = Math.floor((diff % 3600000) / 60000);
                console.log("Difference Hour IF: "+$scope.hourElapsed);
                console.log("Difference Minute IF: "+$scope.minElapsed);
                $scope.successMessage = "You logged for "+$scope.hourElapsed +" hour and "+$scope.minElapsed+" minutes";
            });
        }

        $scope.getAllTimeLogs = function(){
            dataService.queryData('/api/timelog/getAllByPerson').then(function(dataResponse){
                $scope.timeLogs=dataResponse;
            });
        }

        $scope.manualClock = function(){

            var clockInTimeParts = $scope.manual.startTime.split(':');
            var clockInDateParts = $scope.manual.startDate.split('-');
            $scope.manualTime.startTime = new Date(clockInDateParts[2],clockInDateParts[1],clockInDateParts[0],clockInTimeParts[0],clockInTimeParts[1]).toISOString();


            var clockOutTimeParts = $scope.manual.endTime.split(':');
            var clockOutDateParts = $scope.manual.endDate.split('-');
            $scope.manualTime.endTime = new Date(clockOutDateParts[2],clockOutDateParts[1],clockOutDateParts[0],clockOutTimeParts[0],clockOutTimeParts[1]).toISOString();
            $scope.manualTime.logDate = new Date();


            console.log("New log hour: "+$scope.manualTime.startTime);
            console.log("New log min: "+$scope.manualTime.endTime );

            console.log(new Date());
            dataService.saveData("/api/timelog/save/",$scope.manualTime).then(function(dataResponse){
                $scope.getPending();

                $scope.manual = {};
                $scope.manual.startDate;
                $scope.manual.startTime;
                $scope.manual.endDate;
                $scope.manual.endTime= new Date().getHours() +':'+new Date().getMinutes();
            });

        }

        $scope.showForm = function(message){
            $scope.timeLog.notes = message;
            console.log(message);
            $scope.showFormBool = {show: true};
        }

        $scope.showFormManual = function(message){
            $scope.showFormManualBool = {show: true};
            $scope.manualTime.notes = message;
        }

        $scope.calculateElapsedHour = function(startTime){
            console.log("StartTime: "+startTime);
            console.log("StartTime: "+$scope.manual.startTime);
            var clockInTimeParts = $scope.manual.endTime.split(':');
            var hour = parseInt(clockInTimeParts[0])+1;
            console.log("Hour is: "+hour);
            clockInTimeParts[0] = hour;
            $scope.manual.endTime = clockInTimeParts[0] + ":" + clockInTimeParts[1] + ":" +clockInTimeParts[2];
            console.log("End time : "+$scope.manual.endTime);

            $scope.manualTime.endTime = new Date(clockInTimeParts[0],clockInTimeParts[1],clockInTimeParts[2]).toISOString();
            console.log("Adding: "+$scope.manualTime.endTime);

        }

        $scope.changeEndTime = function(endTime){
            $scope.manual.endTime = endTime;
        }

        $scope.calculateElapsedMinute = function(startTime){
            console.log("StartTime: "+startTime);
            var clockInTimeParts = $scope.manual.endTime.split(':');
            var hour = parseInt(clockInTimeParts[0]);
            var min = parseInt(clockInTimeParts[1]);
            if (min<45){
                var min = min+15;

                console.log("Min is: "+min);
//                clockInTimeParts[1] = min;
                $scope.manual.endTime = clockInTimeParts[0] + ":" + min + ":" +clockInTimeParts[2];
                console.log("End time : "+$scope.manual.endTime);
            }
            else{
                var hour = hour +1;
                var min = 0;

                console.log("Min is: "+min);
                clockInTimeParts[1] = min;
                $scope.manual.endTime = hour + ":" + min + ":" +clockInTimeParts[2];
                console.log("End time : "+$scope.manual.endTime);
            }

        }

        $scope.getStats = function(dateRange){
            console.log("Date Range Start: "+dateRange.startDateRange)
            console.log("Date Range End: "+dateRange.endDateRange)
            dataService.startScript('/api/timelog/runScript/'+$scope.person.personId+'/'+dateRange.startDateRange+'/'+dateRange.endDateRange).then(function(dataResponse){

            });
        }

        $scope.graphThisForMe= function(){
            var width = 960,
                    height = 500,
                    radius = 10;

            var p0 = [250, 200, 60],
                    p1 = [560, 300, 120];

            var svg = d3.select("body").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .append("g")
                    .call(transition, p0, p1);

            svg.append("path")
                    .attr("class", "mesh")
                    .attr("d", d3.hexbin()
                            .size([width, height])
                            .radius(radius)
                            .mesh);

            svg.selectAll("circle")
                    .data([p0, p1])
                    .enter().append("circle")
                    .attr("class", function(d, i) { return i ? "end" : "start"; })
                    .attr("cx", function(d) { return d[0]; })
                    .attr("cy", function(d) { return d[1]; })
                    .attr("r", function(d) { return d[2] / 2 - .5; });

            function transition(svg, start, end) {
                var center = [width / 2, height / 2],
                        i = d3.interpolateZoom(start, end);

                svg
                        .attr("transform", transform(start))
                        .transition()
                        .delay(250)
                        .duration(i.duration * 2)
                        .attrTween("transform", function() { return function(t) { return transform(i(t)); }; })
                        .each("end", function() { d3.select(this).call(transition, end, start); });

                function transform(p) {
                    var k = height / p[2];
                    return "translate(" + (center[0] - p[0] * k) + "," + (center[1] - p[1] * k) + ")scale(" + k + ")";
                }
            }

        }

        });





        // Register the 'myCurrentTime' directive factory method.
        // We inject $interval and dateFilter service since the factory method is DI.
        app.directive('myCurrentTime', ['$interval', 'dateFilter',
            function($interval, dateFilter) {
                // return the directive link function. (compile function not needed)
                return function(scope, element, attrs) {
                    var format,  // date format
                            stopTime; // so that we can cancel the time updates

                    // used to update the UI
                    function updateTime() {
                        element.text(dateFilter(new Date(), format));
                    }

                    // watch the expression, and update the UI on change.
                    scope.$watch(attrs.myCurrentTime, function(value) {
                        format = value;
                        updateTime();
                    });

                    stopTime = $interval(updateTime, 1000);

                    // listen on DOM destroy (removal) event, and cancel the next UI update
                    // to prevent updating time after the DOM element was removed.
                    element.on('$destroy', function() {
                        $interval.cancel(stopTime);
                    });
                }
            }]);

</script>
