package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.ExamDAO;
import com.neunou.schema.Exam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 * Created by root on 4/29/15.
 */
@Repository
public class ExamDAOImpl implements ExamDAO{
    private static final Logger LOG = Logger.getLogger(ExamDAOImpl.class.getName());
    @Autowired
    private MongoTemplate mongoTemplate;
    private String EXAMS_COL = "exam";
    @Override
    public String add(Object object) {
        Exam exam = (Exam) object;
        if(exam.getExamId()==null){
            exam.setExamId(UUID.randomUUID().toString());
            exam.setCreationDate(new Date());
            if(!mongoTemplate.collectionExists(EXAMS_COL)){
                mongoTemplate.createCollection(EXAMS_COL);
                mongoTemplate.insert(exam,EXAMS_COL);
                return exam.getExamId();
            }
            else {
                mongoTemplate.insert(exam,EXAMS_COL);
                return exam.getExamId();
            }
        }
        else {
            this.update(object);
            return exam.getExamId();
        }
    }

    @Override
    public void delete(Object object) {
        Exam exam = (Exam) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("examId").is(exam.getExamId()));

        mongoTemplate.remove(query,Exam.class,EXAMS_COL);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("examId").is(id));
        return mongoTemplate.findOne(query,Exam.class,EXAMS_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(Exam.class,EXAMS_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,Exam.class,EXAMS_COL);
        return exists;
    }
}
