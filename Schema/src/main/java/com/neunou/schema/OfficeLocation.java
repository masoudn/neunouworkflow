package com.neunou.schema;

import java.util.Date;

/**
 * Created by Masoud on 3/30/15.
 */
public class OfficeLocation {
    private String officeLocationId;
    private Address address;
    private String locationName;
    private PhoneNumber phoneNumber;
    private String billingRefName;
    private Date creationDate;
    private Date modifiedDate;


    public String getOfficeLocationId() {
        return officeLocationId;
    }

    public void setOfficeLocationId(String officeLocationId) {
        this.officeLocationId = officeLocationId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public PhoneNumber getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(PhoneNumber phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public String getBillingRefName() {
        return billingRefName;
    }

    public void setBillingRefName(String billingRefName) {
        this.billingRefName = billingRefName;
    }
}
