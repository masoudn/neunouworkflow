package com.neunou.hl7manager.Impl;

import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.model.Structure;
import ca.uhn.hl7v2.model.v25.datatype.*;
import ca.uhn.hl7v2.model.v25.message.ORM_O01;
import ca.uhn.hl7v2.model.v25.message.ORU_R01;
import ca.uhn.hl7v2.model.v25.segment.IN1;
import com.neunou.hl7manager.interfaces.GetObjectsFromHL7;
import com.neunou.hl7manager.interfaces.PatientManager;
import com.neunou.hl7manager.interfaces.PhysicianManager;
import com.neunou.hl7manager.utility.*;
import com.neunou.schema.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiConsumer;
import java.util.logging.Logger;

/**
 * Created by root on 5/1/15.
 */
@Component
public class GetObjectsImpl implements GetObjectsFromHL7 {
    private static final Logger LOG = Logger.getLogger(GetObjectsImpl.class.getName());
    @Autowired
    private PhysicianManager physicianManager;
    @Autowired
    private PatientManager patMan;
    @Override
    public List<Insurance> getInsurance(String hl7String) throws HL7Exception {
        List<Insurance> insuranceList = new ArrayList<>();

        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        ORM_O01 msg = (ORM_O01) conversion.convertTo25();
        Person insured = new Person();
        PersonInfo insuredInfo = new PersonInfo();
        Address insAddress = new Address();
        PhoneNumber phone = new PhoneNumber();
        Address insuredAddress = new Address();
        PersonName insuredName = new PersonName();




        if(msg.getPATIENT().getINSURANCEAll().size()==0){
            System.out.println("This is GetObjectsImpl if statement, BEFORE LOG");
            LOG.info("There is no standard IN");
            System.out.println("This is GetObjectsImpl if statement");


            System.out.println("************************Length: "+msg.getPATIENT().getAll("IN1").length);
            System.out.println("We are going to if statement....");

            if (msg.getPATIENT().getAll("IN1").length>1){
                System.out.println("****If statement after the length...");
                for (Structure structure : msg.getPATIENT().getAll("IN1")) {
                    Insurance insurance = new Insurance();
                    IN1 in1 = (IN1) structure;
                    insurance.setGroupNumber(in1.getGroupNumber().toString());
                    insurance.setInsurancePlanId(in1.getInsurancePlanID().getCe1_Identifier().toString());
                    insurance.setGroupNumber(in1.getGroupNumber().toString());
                    insurance.setAuthInformation(in1.getAuthorizationInformation().getAui3_Source().toString());
                    insurance.setPlanType(in1.getPlanType().toString());
                    for (XON xon : in1.getGroupName()) {
                        insurance.setGroupName(xon.getXon1_OrganizationName().toString());
                    }
                    for (CX cx : in1.getInsuranceCompanyID()) {
                        insurance.setInsuranceCompanyId(cx.getIDNumber().toString());
                        insurance.setInsuranceCompanyIdCheckDigit(cx.getCheckDigit().toString());
                    }
                    for (XON xon : in1.getInsuranceCompanyName()) {
                        insurance.setInsuranceCompanyName(xon.getOrganizationName().toString().toString());
                    }
                    for (XAD xad : in1.getInsuranceCompanyAddress()) {
                        insAddress.setAddress1(xad.getXad1_StreetAddress().getSad1_StreetOrMailingAddress().toString());
                        insAddress.setCity(xad.getXad3_City().toString());
                        insAddress.setState(xad.getXad4_StateOrProvince().toString());
                        insAddress.setCountry(xad.getXad6_Country().toString());
                        insAddress.setZipCode(xad.getXad5_ZipOrPostalCode().toString());
                    }
                    for (XTN xtn : in1.getInsuranceCoPhoneNumber()) {
                        phone.setOfficePhoneNumber(xtn.getXtn1_TelephoneNumber().toString());
                    }
                    for (XPN xpn : in1.getIn116_NameOfInsured()) {
                        insuredName.setPersonFirstName(xpn.getGivenName().toString());
                        insuredName.setPersonLastName(xpn.getFamilyName().getFn1_Surname().toString());
                    }
                    for (XAD xad : in1.getInsuredSAddress()) {
                        insuredAddress.setAddress1(xad.getXad1_StreetAddress().getSad1_StreetOrMailingAddress().toString());
                        insuredAddress.setCity(xad.getXad3_City().toString());
                        insuredAddress.setState(xad.getXad4_StateOrProvince().toString());
                        insuredAddress.setZipCode(xad.getXad5_ZipOrPostalCode().toString());
                        insuredAddress.setCountry(xad.getXad6_Country().toString());
                    }
                    LOG.info("DOB1 is....");
                    System.out.println("DOB1: "+in1.getInsuredSDateOfBirth().getTs1_Time().toString());
                    if (in1.getInsuredSDateOfBirth().getTs1_Time().toString()!=null){
                        System.out.println("Well, DOB is null ...");
                        insuredInfo.setDOB(in1.getInsuredSDateOfBirth().getTs1_Time().toString());
                    }
                    insuredInfo.setDOB(in1.getInsuredSDateOfBirth().getTs1_Time().toString());
                    insuredInfo.setAddress(insAddress);
                    insurance.setInsuredRelationToPatient(in1.getInsuredSRelationshipToPatient().getCe1_Identifier().toString());
                    insuredInfo.setPersonName(insuredName);
                    insured.setPersonInfo(insuredInfo);
                    insurance.setInsuredInfo(insured);
                    insurance.setInsuranceCompanyAddress(insAddress);
                    insurance.setInsurancePhoneNumber(phone);

                    if(in1.getSetIDIN1().toString().equalsIgnoreCase("1")){
                        insurance.setPrimaryInsurance(Boolean.TRUE);
                        insuranceList.add(insurance);
                    }
                    else if (in1.getSetIDIN1().toString().equalsIgnoreCase("2")){
                        insurance.setSecondaryInsurance(Boolean.TRUE);
                        insuranceList.add(insurance);
                    }
                    else if (in1.getSetIDIN1().toString().equalsIgnoreCase("3")){
                        insurance.setTertiaryInsurance(Boolean.TRUE);
                        insuranceList.add(insurance);
                    }

                }
                return insuranceList;
            }

            else{
                System.out.println("WE are in the else statement.....");
                IN1 in1 = (IN1) msg.getPATIENT().get("IN1");
                Insurance insurance = new Insurance();
                insurance.setGroupNumber(in1.getGroupNumber().toString());
                insurance.setInsurancePlanId(in1.getInsurancePlanID().getCe1_Identifier().toString());
                insurance.setGroupNumber(in1.getGroupNumber().toString());
                insurance.setAuthInformation(in1.getAuthorizationInformation().getAui3_Source().toString());
                insurance.setPlanType(in1.getPlanType().toString());
                for (XON xon : in1.getGroupName()) {
                    insurance.setGroupName(xon.getXon1_OrganizationName().toString());
                }
                for (CX cx : in1.getInsuranceCompanyID()) {
                    insurance.setInsuranceCompanyId(cx.getIDNumber().toString());
                    insurance.setInsuranceCompanyIdCheckDigit(cx.getCheckDigit().toString());
                }
                for (XON xon : in1.getInsuranceCompanyName()) {
                    insurance.setInsuranceCompanyName(xon.getOrganizationName().toString().toString());
                }
                for (XAD xad : in1.getInsuranceCompanyAddress()) {
                    insAddress.setAddress1(xad.getXad1_StreetAddress().getSad1_StreetOrMailingAddress().toString());
                    insAddress.setCity(xad.getXad3_City().toString());
                    insAddress.setState(xad.getXad4_StateOrProvince().toString());
                    insAddress.setCountry(xad.getXad6_Country().toString());
                    insAddress.setZipCode(xad.getXad5_ZipOrPostalCode().toString());
                }
                for (XTN xtn : in1.getInsuranceCoPhoneNumber()) {
                    phone.setOfficePhoneNumber(xtn.getXtn1_TelephoneNumber().toString());
                }
                for (XPN xpn : in1.getIn116_NameOfInsured()) {
                    insuredName.setPersonFirstName(xpn.getGivenName().toString());
                    insuredName.setPersonLastName(xpn.getFamilyName().getFn1_Surname().toString());
                }
                for (XAD xad : in1.getInsuredSAddress()) {
                    insuredAddress.setAddress1(xad.getXad1_StreetAddress().getSad1_StreetOrMailingAddress().toString());
                    insuredAddress.setCity(xad.getXad3_City().toString());
                    insuredAddress.setState(xad.getXad4_StateOrProvince().toString());
                    insuredAddress.setZipCode(xad.getXad5_ZipOrPostalCode().toString());
                    insuredAddress.setCountry(xad.getXad6_Country().toString());
                }
                LOG.info("DOB2 is....");
//                System.out.println("DOB2: "+in1.getInsuredSDateOfBirth().getTs1_Time().toString());
                System.out.println(in1.getInsuredSDateOfBirth().getTs1_Time());
                if (in1.getInsuredSDateOfBirth().getTs1_Time().toString()!=null){
                    System.out.println("Well, DOB is null ...");
                    insuredInfo.setDOB(in1.getInsuredSDateOfBirth().getTs1_Time().toString());
                }
                System.out.println("Setting the address...");
                insuredInfo.setAddress(insAddress);
                insurance.setInsuredRelationToPatient(in1.getInsuredSRelationshipToPatient().getCe1_Identifier().toString());
                insuredInfo.setPersonName(insuredName);
                insured.setPersonInfo(insuredInfo);
                System.out.println("Setting the insurance...");
                insurance.setInsuredInfo(insured);
                insurance.setInsuranceCompanyAddress(insAddress);
                insurance.setInsurancePhoneNumber(phone);

                //Adding the insurance to the insurance list and returning it
                insuranceList.add(insurance);
                System.out.println("Setting the insurance after...");
                return insuranceList;

            }

        }
        else {
            LOG.info("YaaaY there is a standard IN");
            System.out.println("Size of insurance: " + msg.getPATIENT().getINSURANCEAll().size());
            System.out.println("Set ID insurance: "+msg.getPATIENT().getINSURANCE().getIN1().getSetIDIN1());
            return insuranceList;
        }
    }

    @Override
    public Visit getVisit(String hl7String) throws HL7Exception, ParseException {
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        Visit visit = new Visit();

        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        if (conversion.convertTo25().getName().contains("ORM")){
            ORM_O01 msg = (ORM_O01) conversion.convertTo25();

            List<Exam> examList = new ArrayList<>();
            Exam exam = new Exam();
            if (msg.getORDERAll().size()>1){
                msg.getORDERAll().stream()
                        .forEach(OBRmsg -> {OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier();
                            Exam newExam = new Exam();
                            ModalityEnum modality = new ModalityEnum(OBRmsg.getORDER_DETAIL().getOBR().getObr24_DiagnosticServSectID().toString());
                            newExam.setModality(modality.transferToModality());
                            newExam.setExamCode(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe1_Identifier().toString());
                            newExam.setExamDesc(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe2_Text().toString());
                            examList.add(newExam);
                        });
                visit.setExams(examList);
                TS dos = msg.getORDER().getORDER_DETAIL().getOBR().getObr7_ObservationDateTime();

                Date date = format.parse(dos.getTs1_Time().toString());
                visit.setSchedueledDate(date);

                VisitStatusEnum visitStatusEnum = new VisitStatusEnum(msg.getORDER().getORC().getOrc1_OrderControl().toString(),msg.getORDER().getORC().getOrc5_OrderStatus().toString());
                visit.setVisitStatus(visitStatusEnum.transformToEnum());

                System.out.println("-----Order Control: "+msg.getORDER().getORC().getOrc1_OrderControl().toString()+"      "+visit.getVisitStatus());

            }
            else if (msg.getORDERAll().size()<=1){
                msg.getORDERAll().stream()
                        .forEach(OBRmsg -> {OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier();
                            Exam newExam = new Exam();
                            ModalityEnum modality = new ModalityEnum(OBRmsg.getORDER_DETAIL().getOBR().getObr24_DiagnosticServSectID().toString());
                            newExam.setModality(modality.transferToModality());
                            System.out.println(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe1_Identifier().toString());
                            System.out.println(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe2_Text().toString());
                            newExam.setExamCode(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe1_Identifier().toString());
                            newExam.setExamDesc(OBRmsg.getORDER_DETAIL().getOBR().getObr4_UniversalServiceIdentifier().getCe2_Text().toString());
                            examList.add(newExam);
                        });
                visit.setExams(examList);
                TS dos = msg.getORDER().getORDER_DETAIL().getOBR().getObr7_ObservationDateTime();

                Date date = format.parse(dos.getTs1_Time().toString());
                visit.setSchedueledDate(date);

                VisitStatusEnum visitStatusEnum = new VisitStatusEnum(msg.getORDER().getORC().getOrc1_OrderControl().toString(),msg.getORDER().getORC().getOrc5_OrderStatus().toString());
                visit.setVisitStatus(visitStatusEnum.transformToEnum());

                System.out.println("-----Order Control: "+msg.getORDER().getORC().getOrc1_OrderControl().toString()+"      "+visit.getVisitStatus());
            }
            if (!msg.getPATIENT().getPATIENT_VISIT().getPV1().isEmpty()){
                String visitNumber = msg.getPATIENT().getPATIENT_VISIT().getPV1().getPv119_VisitNumber().getCx1_IDNumber().toString();
                visit.setVisitNumber(visitNumber);
            }
            if (msg.getPATIENT().getPATIENT_VISIT().getPV2().isEmpty()){
                LOG.info("There is a PV2 section that is not mapped...");
            }


            List<HL7Document> hl7Documents = new ArrayList<>();
            HL7Document hl7Document = new HL7Document();
            hl7Document.setHl7MessageContent(hl7String);
            Date hl7Date = format.parse(msg.getMSH().getDateTimeOfMessage().getTs1_Time().toString());

            String messageType = msg.getMSH().getMessageType().getMsg1_MessageCode().toString()+msg.getMSH().getMessageType().getMsg2_TriggerEvent().toString();
            HL7MessageTypes hl7MessageTypes = new HL7MessageTypes();
            hl7MessageTypes.setHl7Type(messageType);
            hl7MessageTypes.setHl7Version(msg.getMSH().getMsh12_VersionID().getVid1_VersionID().toString());
            hl7Document.setHl7MessageTypes(hl7MessageTypes);

            hl7Document.setHl7Date(hl7Date);

            hl7Documents.add(hl7Document);
            visit.setHl7Documents(hl7Documents);

            String accNumber = msg.getORDER().getORC().getOrc2_PlacerOrderNumber().getEi1_EntityIdentifier().toString();
            visit.setAccessionNumber(accNumber);



            return visit;
        }
        else if (conversion.convertTo25().getName().contains("ORU")){
            ORU_R01 oruMsg = (ORU_R01) conversion.convertTo25();
            String visitNumber = oruMsg.getPATIENT_RESULT().getPATIENT().getVISIT().getPV1().getVisitNumber().getCx1_IDNumber().toString();
            visit.setVisitNumber(visitNumber);
            String status = oruMsg.getPATIENT_RESULT().getORDER_OBSERVATION().getOBSERVATION().getOBX().getObx11_ObservationResultStatus().toString();

            if (status.toLowerCase().contains("f")){
                VisitStatus visitStatus= VisitStatus.Final;
                visit.setVisitStatus(visitStatus);
                List<HL7Document> hl7s = new ArrayList<>();
                hl7s.add(this.getHL7Doc(hl7String));
                visit.setHl7Documents(hl7s);

                //Get Reading Physician ID
                Physician readingPhyFromMSG = this.getReadingPhysician(hl7String);
                Physician readingPhyFromDB = this.physicianManager.getPhysicianByNPI(readingPhyFromMSG.getPhysicianNPI());
                if (readingPhyFromDB==null){
                    System.out.println("------------------------Reading was null---------------");
                    String readingPhyId = this.physicianManager.add(readingPhyFromMSG);
                    readingPhyFromMSG.setPhysicianId(readingPhyId);
                    visit.setPhysicianId(readingPhyFromMSG.getPhysicianId());
                    return visit;
                }
                else {
                    System.out.println("------------------------Reading was not null---------------"+readingPhyFromDB.getPhysicianId());
                    visit.setPhysicianId(readingPhyFromDB.getPhysicianId());
                    return visit;
                }

            }
            else {
                System.out.println("**********************Status is ENDED*********************************");
                VisitStatus visitStatus = VisitStatus.Ended;
                visit.setVisitStatus(visitStatus);
                List<HL7Document> hl7s = new ArrayList<>();
                hl7s.add(this.getHL7Doc(hl7String));
                visit.setHl7Documents(hl7s);

                //Get Reading Physician ID
                Physician readingPhyFromMSG = this.getReadingPhysician(hl7String);
                Physician readingPhysicianFromDB = physicianManager.getPhysicianByNPI(readingPhyFromMSG.getPhysicianNPI());
                if (readingPhysicianFromDB!=null){
                    physicianManager.update(readingPhyFromMSG);
                    visit.setReadingPhysicianId(readingPhysicianFromDB.getPhysicianId());
                    return visit;
                }
                else {
                    visit.setReadingPhysicianId(physicianManager.add(readingPhyFromMSG));
                    return visit;
                }

            }

        }

        else {
            LOG.info("No compatible HL7 to get visit from");
            return null;
        }
    }

    @Override
    public PersonInfo getPersonInfo(String hl7String) throws HL7Exception {
        PersonInfo personInfo = new PersonInfo();
        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        if (conversion.convertTo25().getName().contains("ORM")){
            ORM_O01 orm = (ORM_O01) conversion.convertTo25();
            //Get person name
            GetPersonNameFromORM personNameFromORM = new GetPersonNameFromORM(orm);
            personInfo.setPersonName(personNameFromORM.nameFactory());

            return personInfo;
        }
        else if (conversion.convertTo25().getName().contains("ORU")){
            System.out.println("*********@@@@@@Getting person name... ");
            ORU_R01 oru = (ORU_R01) conversion.convertTo25();
            //Get person name
            GetPersonNameFromORU personNameFromORU = new GetPersonNameFromORU(oru);
            personInfo.setPersonName(personNameFromORU.nameFactory());
            personInfo.setDOB(oru.getPATIENT_RESULT().getPATIENT().getPID().getDateTimeOfBirth().getTs1_Time().toString());

            return personInfo;
        }

        else {
            LOG.info("HL7 message type is not supported.");
            return null;
        }

    }

    @Override
    public Physician getPhysician(String hl7String) throws HL7Exception {
        Physician physician = new Physician();
        PersonInfo physicianInfo = new PersonInfo();
        Address physicianAddress = new Address();

        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        if (conversion.convertTo25().getName().contains("ORM")){
            ORM_O01 orm = (ORM_O01) conversion.convertTo25();
            //Get physician name
            GetPersonNameFromORM personNameFromORM = new GetPersonNameFromORM(orm);
            physicianInfo.setPersonName(personNameFromORM.nameFactory());
            for (XAD xad : orm.getORDER().getORC().getOrderingFacilityAddress()) {
                System.out.println("Physician Address: "+xad.getXad1_StreetAddress().toString());
                System.out.println("Physician Address: "+xad.getXad4_StateOrProvince().toString());
                System.out.println("Physician Address: "+xad.getCity());
            }
            physicianInfo.setAddress(physicianAddress);
            physician.setPersonInfo(physicianInfo);

            return physician;
        }
        else if (conversion.convertTo25().getName().contains("ORU")){
            ORU_R01 oru = (ORU_R01) conversion.convertTo25();
            //Get physician name
            GetPersonNameFromORU personNameFromORU = new GetPersonNameFromORU(oru);
            physicianInfo.setPersonName(personNameFromORU.nameFactory());

            return physician;
        }
        else {
            return physician;
        }

    }

    @Override
    public Physician getReadingPhysician(String hl7String) throws HL7Exception {
        Physician physician = new Physician();
        PersonInfo physicianInfo = new PersonInfo();
        PersonName physicianName = new PersonName();

        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        if (conversion.convertTo25().getName().contains("ORM")){
            ORM_O01 orm = (ORM_O01) conversion.convertTo25();

            return physician;
        }
        else if(conversion.convertTo25().getName().contains("ORU")){
            ORU_R01 oru = (ORU_R01) conversion.convertTo25();
            String npi = oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBR().getObr32_PrincipalResultInterpreter().getNdl1_NameOfPerson().getCnn1_IDNumber().toString();
            System.out.println("@@@@@@@######### Dictating NPI::: "+npi);
//            physician = physicianManager.getPhysicianByNPI(npi);
//            System.out.println("We are trying to get the physy....");
//            if (physician!=null){
//                System.out.println("@@@@@@@######### Dictating ID::: "+physician.getPhysicianId());
//                return physician;
//            }
//            else{
//
//
//            }
            physician = new Physician();
            LOG.info("Could not find the dictating physician in the system, dissecting the HL7 to add");

            SlicingText slicingText = new SlicingText(oru.getPATIENT_RESULT().getORDER_OBSERVATION().getOBR().getPrincipalResultInterpreter().toString());
            HashMap dataMap = slicingText.dissectForChar("^");
            System.out.println("Size of the MAP is: "+dataMap.size());

            BiConsumer<Integer, String> biConsumer = (key, value) -> {
                if (key==1){
                    physicianName.setPersonLastName(value);
                }
                else if (key==2){
                    physicianName.setPersonFirstName(value);
                }
                else if(key==3){
                    physicianName.setPersonMiddleName(value);
                }

                System.out.println(key);
                System.out.println(value);
            };
            dataMap.forEach(biConsumer);
            physicianName.setPersonFullName(physicianName.getPersonFirstName()+" "+physicianName.getPersonLastName());

            LOG.info("Setting ID to physician.....Setting the name");
            physicianInfo.setPersonName(physicianName);
            LOG.info("Setting ID to physician.....Getting NPI: "+npi);
            physician.setPhysicianNPI(npi);
            LOG.info("Setting ID to physician.....Setting info");
            physician.setPersonInfo(physicianInfo);
            LOG.info("Setting ID to physician.....");
            return physician;

        }
        else {
            return physician;
        }

    }

    @Override
    public HL7Document getHL7Doc(String hl7String) throws HL7Exception, ParseException {
        DateFormat format = new SimpleDateFormat("yyyyMMddHHmmss");
        HL7Document hl7Document = new HL7Document();
        hl7Document.setCreationDate(new Date());
        HL7VersionConversion conversion = new HL7VersionConversion(hl7String);
        hl7Document.setHl7MessageContent(hl7String);
        if (conversion.convertTo25().getName().contains("ORM")){
            ORM_O01 orm = (ORM_O01) conversion.convertTo25();
            HL7MessageTypes types = new HL7MessageTypes();
            types.setHl7Type("ORM");
            types.setHl7Version(orm.getVersion());
            types.setHl7TypeDesc(orm.getMSH().getMsh4_SendingFacility().getHd1_NamespaceID().toString());
            Date hl7Date = format.parse(orm.getMSH().getDateTimeOfMessage().getTs1_Time().toString());
            hl7Document.setHl7Date(hl7Date);
            hl7Document.setHl7MessageTypes(types);
            hl7Document.setPrintStructure(orm.printStructure());
            return hl7Document;

        }
        else if(conversion.convertTo25().getName().contains("ORU")){
            ORU_R01 oru = (ORU_R01) conversion.convertTo25();
            HL7MessageTypes types = new HL7MessageTypes();
            types.setHl7Type("ORU");
            types.setHl7TypeDesc(oru.getMSH().getMsh4_SendingFacility().getHd1_NamespaceID().toString());
            types.setHl7Version(oru.getVersion());
            Date hl7Date = format.parse(oru.getMSH().getDateTimeOfMessage().getTs1_Time().toString());
            System.out.println("HL7 Date is: " + hl7Date);
            hl7Document.setHl7Date(hl7Date);
            hl7Document.setHl7MessageTypes(types);
            hl7Document.setPrintStructure(oru.printStructure());
            return hl7Document;
        }
        else {
            LOG.info("HL7 message is not supported yet.");
            return null;
        }

    }

    private PersonName getName(){
        PersonName name = new PersonName();
        return name;
    }
}
