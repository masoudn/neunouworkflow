package com.neunou.ropersistence.Interfaces;

import com.neunou.schema.TimeLog;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by root on 6/5/15.
 */
@Service
public interface TimeLogDAO extends GenericInterface {
    List<TimeLog> getAllTimeLogsByPerson(String personId);
}
