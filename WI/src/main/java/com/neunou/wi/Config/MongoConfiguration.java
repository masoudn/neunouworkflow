package com.neunou.wi.Config;

import com.mongodb.MongoClient;
import com.mongodb.ServerAddress;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by Masoud on 2/13/15.
 */
@Configuration
@EnableMongoRepositories
@ComponentScan(basePackages = "com.neunou")
public class MongoConfiguration extends AbstractMongoConfiguration {
    @Override
    protected String getDatabaseName() {
        return "WorkflowTest";
    }

//    @Override
//    protected String getDatabaseName() {
//        return "WorkflowTest";
//    }

//    @Override
//    public MongoClient mongo() throws Exception {
//        return new MongoClient(new ServerAddress("localhost",27017));
//    }

    @Override
    public MongoClient mongo() throws Exception {
        return new MongoClient(new ServerAddress("50.200.165.106",27017));
    }
}
