package com.neunou.hl7manager.utility;

import ca.uhn.hl7v2.model.v25.message.ORU_R01;

/**
 * Created by root on 2/25/16.
 */
public class GetMRNFromORU {
    private ORU_R01 oru;
    private String mrn;

    public GetMRNFromORU(ORU_R01 oru) {
        this.setOru(oru);
    }

    public String mrnFactory(){
        setMrn(getOru().getPATIENT_RESULT().getPATIENT().getPID().getPatientID().getCx1_IDNumber().toString());
        return mrn;
    }


    public ORU_R01 getOru() {
        return oru;
    }

    public void setOru(ORU_R01 oru) {
        this.oru = oru;
    }

    public String getMrn() {
        return mrn;
    }

    public void setMrn(String mrn) {
        this.mrn = mrn;
    }
}
