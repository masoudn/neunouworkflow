package com.neunou.schema;

import java.util.Date;
import java.util.Map;

/**
 * Created by Masoud on 3/3/15.
 */
public class LDAPSettings{
    private String ldapId;
    private String companyId;
    private String ldapURL;
    private String ldapBase;
    private String groupSearchFilter;
    private String userSearchFilter;
    private String ldapUserDN;
    private String ldapPassword;
    private Date creationDate;
    private Boolean activeDirectory;
    private Map<Date,String> modifiedDate;


    public String getLdapId() {
        return ldapId;
    }

    public void setLdapId(String ldapId) {
        this.ldapId = ldapId;
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getLdapURL() {
        return ldapURL;
    }

    public void setLdapURL(String ldapURL) {
        this.ldapURL = ldapURL.toLowerCase();
    }

    public String getLdapBase() {
        return ldapBase;
    }

    public void setLdapBase(String ldapBase) {
        this.ldapBase = ldapBase.toLowerCase();
    }

    public String getLdapUserDN() {
        return ldapUserDN;
    }

    public void setLdapUserDN(String ldapUserDN) {
        this.ldapUserDN = ldapUserDN.toLowerCase();
    }

    public String getLdapPassword() {
        return ldapPassword;
    }

    public void setLdapPassword(String ldapPassword) {
        this.ldapPassword = ldapPassword;
    }


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public Map<Date, String> getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Map<Date, String> modifiedDate) {
        this.modifiedDate = modifiedDate;
    }


    public String getGroupSearchFilter() {
        return groupSearchFilter;
    }

    public void setGroupSearchFilter(String groupSearchFilter) {
        this.groupSearchFilter = groupSearchFilter.toLowerCase();
    }

    public String getUserSearchFilter() {
        return userSearchFilter;
    }

    public void setUserSearchFilter(String userSearchFilter) {
        this.userSearchFilter = userSearchFilter.toLowerCase();
    }

    public Boolean getActiveDirectory() {
        return activeDirectory;
    }

    public void setActiveDirectory(Boolean activeDirectory) {
        this.activeDirectory = activeDirectory;
    }

}

