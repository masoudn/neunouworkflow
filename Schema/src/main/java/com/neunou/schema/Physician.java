package com.neunou.schema;

import java.util.Date;

/**
 * Created by Masoud on 9/16/14.
 */
public class Physician extends Person{
    private String physicianId;
    private String physicianNPI;
    private Boolean physicianAssistance;
    private String physicianSubspecialty;
    private Date createdDate;
    private Date modifiedDate;


    public String getPhysicianId() {
        return physicianId;
    }

    public void setPhysicianId(String physicianId) {
        this.physicianId = physicianId;
    }

    public String getPhysicianNPI() {
        return physicianNPI;
    }

    public void setPhysicianNPI(String physicianNPI) {
        this.physicianNPI = physicianNPI;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public String getPhysicianSubspecialty() {
        return physicianSubspecialty;
    }

    public void setPhysicianSubspecialty(String physicianSubspecialty) {
        this.physicianSubspecialty = physicianSubspecialty;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public Boolean getPhysicianAssistance() {
        return physicianAssistance;
    }

    public void setPhysicianAssistance(Boolean physicianAssistance) {
        this.physicianAssistance = physicianAssistance;
    }
}
