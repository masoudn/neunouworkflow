package com.neunou.ropersistence.Impl;

import com.neunou.ropersistence.Interfaces.BillingBudgetRecordsDAO;
import com.neunou.schema.Audit;
import com.neunou.schema.BillingBudgetRecords;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by root on 4/7/15.
 */
@Repository
public class BillingBudgetRecordsDAOImpl implements BillingBudgetRecordsDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String BILLBUDG_COL = "billingBudgetRecords";
    @Override
    public String add(Object object) {
        BillingBudgetRecords billingBudgetRecords = (BillingBudgetRecords) object;
        Audit audit = new Audit();
        audit.setAuditId(UUID.randomUUID().toString());
        audit.setCreatedOn(new Date());
        if(billingBudgetRecords.getBillingBudgetRecordsId()==null){
            billingBudgetRecords.setBillingBudgetRecordsId(UUID.randomUUID().toString());
            billingBudgetRecords.setAudit(audit);
            if(!mongoTemplate.collectionExists(BILLBUDG_COL)){
                mongoTemplate.createCollection(BILLBUDG_COL);
                mongoTemplate.insert(billingBudgetRecords,BILLBUDG_COL);
                return billingBudgetRecords.getBillingBudgetRecordsId();
            }
            else {
                mongoTemplate.insert(billingBudgetRecords,BILLBUDG_COL);
                return billingBudgetRecords.getBillingBudgetRecordsId();
            }

        }
        else {
            this.update(object);
            return billingBudgetRecords.getBillingBudgetRecordsId();
        }
    }

    @Override
    public void delete(Object object) {
        BillingBudgetRecords billingBudgetRecords = (BillingBudgetRecords) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("billingBudgetRecordsId").is(billingBudgetRecords.getBillingBudgetRecordsId()));

        mongoTemplate.remove(query,BillingBudgetRecords.class,BILLBUDG_COL);
    }

    @Override
    public void update(Object object) {

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("billingBudgetRecordsId").is(id));
        return mongoTemplate.findOne(query,BillingBudgetRecords.class,BILLBUDG_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(BillingBudgetRecords.class,BILLBUDG_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,BillingBudgetRecords.class,BILLBUDG_COL);
        return exists;
    }
}
