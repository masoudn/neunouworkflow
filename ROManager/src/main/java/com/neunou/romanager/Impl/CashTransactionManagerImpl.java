package com.neunou.romanager.Impl;

import com.neunou.romanager.Interfaces.CashTransactionManager;
import com.neunou.ropersistence.Interfaces.CashTransactionDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Masoud on 10/12/15.
 */
@Repository
public class CashTransactionManagerImpl implements CashTransactionManager {
    @Autowired
    private CashTransactionDAO dao;
    @Override
    public String add(Object object) {
        return dao.add(object);
    }

    @Override
    public void delete(Object object) {
        dao.delete(object);
    }

    @Override
    public void update(Object object) {
        dao.update(object);
    }

    @Override
    public Object getById(String id) {
        return dao.getById(id);
    }

    @Override
    public List getAll() {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        return null;
    }
}
