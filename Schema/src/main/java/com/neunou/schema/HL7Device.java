package com.neunou.schema;

import java.util.Date;

/**
 * Created by Masoud on 9/17/14.
 */
public class HL7Device {
    private String hl7DeviceId;
    private String deviceName;
    private String host;
    private int port;
    private Boolean inbound;
    private Boolean outbound;
    private Boolean running;
    private Boolean useTLS;
    private Date creationDate;
    private Date modificationDate;


    public String getHl7DeviceId() {
        return hl7DeviceId;
    }

    public void setHl7DeviceId(String hl7DeviceId) {
        this.hl7DeviceId = hl7DeviceId;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public Boolean getInbound() {
        return inbound;
    }

    public void setInbound(Boolean inbound) {
        this.inbound = inbound;
    }

    public Boolean getOutbound() {
        return outbound;
    }

    public void setOutbound(Boolean outbound) {
        this.outbound = outbound;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }


    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public Boolean getUseTLS() {
        return useTLS;
    }

    public void setUseTLS(Boolean useTLS) {
        this.useTLS = useTLS;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Boolean getRunning() {
        return running;
    }

    public void setRunning(Boolean running) {
        this.running = running;
    }
}
