package com.neunou.schema;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Created by root on 4/30/15.
 */
public class ChargeActiveRecords {
    private String chargeActiveRecordsId;
    private String accessionID;
    private int chargeId;
    private String clientId;
    private int claimNumber;
    private String patientId;
    private String chargeCarrierType;
    private Boolean status;
    private String creationDate;
    private String lastUpdateDate;
    private String reversalDate;
    private int statementTimesSent;
    private String emerRelatedIndicator;
    private BigDecimal baseFee;
    private BigDecimal balance;
    private BigDecimal primaryBalance;
    private BigDecimal primaryAllowed;
    private BigDecimal primaryWriteOff;
    private BigDecimal primaryCoinsurance;
    private BigDecimal primaryDeductible;
    private String primaryDateFiled;
    private int primaryTotalPayments;
    private String primaryLastDatePaid;
    private BigDecimal secondaryBalance;
    private BigDecimal secondaryAllowed;
    private BigDecimal secondaryWriteOff;
    private BigDecimal secondaryDeductible;
    private BigDecimal secondaryCoinsurance;
    private String secondaryDateFiled;
    private int secondaryTotalPayments;
    private String secondaryLastDatePaid;
    private BigDecimal tertiaryBalance;
    private BigDecimal tertiaryAllowed;
    private BigDecimal tertiaryWriteOff;
    private BigDecimal tertiaryDeductible;
    private BigDecimal tertiaryCoinsurance;
    private String tertiaryDateFiled;
    private int tertiaryTotalPayments;
    private String tertiaryLastDatePaid;
    private BigDecimal guarantorBalance;
    private BigDecimal guarantorWriteOff;
    private BigDecimal guarantorDebits;
    private BigDecimal guarantorTotalPayments;
    private String guarantorLastDatePaid;
    private String filingProfileID;
    private String renderingProviderCode;
    private int renderingProviderLocalID;
    private String referringProviderCode;
    private String pOSCode;
    private String locationID;
    private String procedureID;
    private String modifier1;
    private String modifier2;
    private String modifier3;
    private String primaryICD9;
    private String secondaryICD9;
    private String tertiaryICD9;
    private String dOSTo;
    private String stateOfAccident;
    private String dOSFrom;
    private Date creationDateAudit;
    private String otherICD9;
    private String atAgency;
    private String denialReasonID;
    private String accident;
    private String dOAccident;
    private String referringProvideCode;
    private List<Insurance> insurance;
    private String wRvu;

    private String dateOfService;
    private String modality;
    private String procedureDescription;
    private Physician renderingProvider;
    private Physician referringProvider;
    private OfficeLocation location;

    private String diagnosis1ICD10;
    private String diagnosis2ICD10;
    private String diagnosis3ICD10;
    private String diagnosis4ICD10;
    private String diagnosis5ICD10;
    private String diagnosis6ICD10;
    private String diagnosis7ICD10;
    private String diagnosis8ICD10;
    private String diagnosis9ICD10;
    private String diagnosis10ICD10;
    private String diagnosis11ICD10;
    private String diagnosis12ICD10;
    private String diagnosisFullICD10;

    private Boolean mbx;


    public String getChargeActiveRecordsId() {
        return chargeActiveRecordsId;
    }

    public void setChargeActiveRecordsId(String chargeActiveRecordsId) {
        this.chargeActiveRecordsId = chargeActiveRecordsId;
    }

    public int getChargeId() {
        return chargeId;
    }

    public void setChargeId(int chargeId) {
        this.chargeId = chargeId;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getClaimNumber() {
        return claimNumber;
    }

    public void setClaimNumber(int claimNumber) {
        this.claimNumber = claimNumber;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getChargeCarrierType() {
        return chargeCarrierType;
    }

    public void setChargeCarrierType(String chargeCarrierType) {
        this.chargeCarrierType = chargeCarrierType;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public int getStatementTimesSent() {
        return statementTimesSent;
    }

    public void setStatementTimesSent(int statementTimesSent) {
        this.statementTimesSent = statementTimesSent;
    }

    public BigDecimal getBaseFee() {
        return baseFee;
    }

    public void setBaseFee(BigDecimal baseFee) {
        this.baseFee = baseFee;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getPrimaryBalance() {
        return primaryBalance;
    }

    public void setPrimaryBalance(BigDecimal primaryBalance) {
        this.primaryBalance = primaryBalance;
    }

    public BigDecimal getPrimaryAllowed() {
        return primaryAllowed;
    }

    public void setPrimaryAllowed(BigDecimal primaryAllowed) {
        this.primaryAllowed = primaryAllowed;
    }

    public BigDecimal getPrimaryWriteOff() {
        return primaryWriteOff;
    }

    public void setPrimaryWriteOff(BigDecimal primaryWriteOff) {
        this.primaryWriteOff = primaryWriteOff;
    }

    public BigDecimal getPrimaryCoinsurance() {
        return primaryCoinsurance;
    }

    public void setPrimaryCoinsurance(BigDecimal primaryCoinsurance) {
        this.primaryCoinsurance = primaryCoinsurance;
    }

    public BigDecimal getPrimaryDeductible() {
        return primaryDeductible;
    }

    public void setPrimaryDeductible(BigDecimal primaryDeductible) {
        this.primaryDeductible = primaryDeductible;
    }

    public String getPrimaryDateFiled() {
        return primaryDateFiled;
    }

    public void setPrimaryDateFiled(String primaryDateFiled) {
        this.primaryDateFiled = primaryDateFiled;
    }

    public int getPrimaryTotalPayments() {
        return primaryTotalPayments;
    }

    public void setPrimaryTotalPayments(int primaryTotalPayments) {
        this.primaryTotalPayments = primaryTotalPayments;
    }

    public BigDecimal getSecondaryBalance() {
        return secondaryBalance;
    }

    public void setSecondaryBalance(BigDecimal secondaryBalance) {
        this.secondaryBalance = secondaryBalance;
    }

    public BigDecimal getSecondaryAllowed() {
        return secondaryAllowed;
    }

    public void setSecondaryAllowed(BigDecimal secondaryAllowed) {
        this.secondaryAllowed = secondaryAllowed;
    }

    public BigDecimal getSecondaryWriteOff() {
        return secondaryWriteOff;
    }

    public void setSecondaryWriteOff(BigDecimal secondaryWriteOff) {
        this.secondaryWriteOff = secondaryWriteOff;
    }

    public BigDecimal getSecondaryDeductible() {
        return secondaryDeductible;
    }

    public void setSecondaryDeductible(BigDecimal secondaryDeductible) {
        this.secondaryDeductible = secondaryDeductible;
    }

    public BigDecimal getSecondaryCoinsurance() {
        return secondaryCoinsurance;
    }

    public void setSecondaryCoinsurance(BigDecimal secondaryCoinsurance) {
        this.secondaryCoinsurance = secondaryCoinsurance;
    }

    public int getSecondaryTotalPayments() {
        return secondaryTotalPayments;
    }

    public void setSecondaryTotalPayments(int secondaryTotalPayments) {
        this.secondaryTotalPayments = secondaryTotalPayments;
    }

    public BigDecimal getTertiaryBalance() {
        return tertiaryBalance;
    }

    public void setTertiaryBalance(BigDecimal tertiaryBalance) {
        this.tertiaryBalance = tertiaryBalance;
    }

    public BigDecimal getTertiaryAllowed() {
        return tertiaryAllowed;
    }

    public void setTertiaryAllowed(BigDecimal tertiaryAllowed) {
        this.tertiaryAllowed = tertiaryAllowed;
    }

    public BigDecimal getTertiaryWriteOff() {
        return tertiaryWriteOff;
    }

    public void setTertiaryWriteOff(BigDecimal tertiaryWriteOff) {
        this.tertiaryWriteOff = tertiaryWriteOff;
    }

    public BigDecimal getTertiaryDeductible() {
        return tertiaryDeductible;
    }

    public void setTertiaryDeductible(BigDecimal tertiaryDeductible) {
        this.tertiaryDeductible = tertiaryDeductible;
    }

    public BigDecimal getTertiaryCoinsurance() {
        return tertiaryCoinsurance;
    }

    public void setTertiaryCoinsurance(BigDecimal tertiaryCoinsurance) {
        this.tertiaryCoinsurance = tertiaryCoinsurance;
    }

    public String getTertiaryDateFiled() {
        return tertiaryDateFiled;
    }

    public void setTertiaryDateFiled(String tertiaryDateFiled) {
        this.tertiaryDateFiled = tertiaryDateFiled;
    }

    public int getTertiaryTotalPayments() {
        return tertiaryTotalPayments;
    }

    public void setTertiaryTotalPayments(int tertiaryTotalPayments) {
        this.tertiaryTotalPayments = tertiaryTotalPayments;
    }

    public String getTertiaryLastDatePaid() {
        return tertiaryLastDatePaid;
    }

    public void setTertiaryLastDatePaid(String tertiaryLastDatePaid) {
        this.tertiaryLastDatePaid = tertiaryLastDatePaid;
    }

    public BigDecimal getGuarantorBalance() {
        return guarantorBalance;
    }

    public void setGuarantorBalance(BigDecimal guarantorBalance) {
        this.guarantorBalance = guarantorBalance;
    }

    public BigDecimal getGuarantorWriteOff() {
        return guarantorWriteOff;
    }

    public void setGuarantorWriteOff(BigDecimal guarantorWriteOff) {
        this.guarantorWriteOff = guarantorWriteOff;
    }

    public BigDecimal getGuarantorDebits() {
        return guarantorDebits;
    }

    public void setGuarantorDebits(BigDecimal guarantorDebits) {
        this.guarantorDebits = guarantorDebits;
    }

    public BigDecimal getGuarantorTotalPayments() {
        return guarantorTotalPayments;
    }

    public void setGuarantorTotalPayments(BigDecimal guarantorTotalPayments) {
        this.guarantorTotalPayments = guarantorTotalPayments;
    }

    public String getFilingProfileID() {
        return filingProfileID;
    }

    public void setFilingProfileID(String filingProfileID) {
        this.filingProfileID = filingProfileID;
    }

    public String getRenderingProviderCode() {
        return renderingProviderCode;
    }

    public void setRenderingProviderCode(String renderingProviderCode) {
        this.renderingProviderCode = renderingProviderCode;
    }

    public int getRenderingProviderLocalID() {
        return renderingProviderLocalID;
    }

    public void setRenderingProviderLocalID(int renderingProviderLocalID) {
        this.renderingProviderLocalID = renderingProviderLocalID;
    }

    public String getReferringProviderCode() {
        return referringProviderCode;
    }

    public void setReferringProviderCode(String referringProviderCode) {
        this.referringProviderCode = referringProviderCode;
    }

    public String getpOSCode() {
        return pOSCode;
    }

    public void setpOSCode(String pOSCode) {
        this.pOSCode = pOSCode;
    }

    public String getLocationID() {
        return locationID;
    }

    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    public String getProcedureID() {
        return procedureID;
    }

    public void setProcedureID(String procedureID) {
        this.procedureID = procedureID;
    }

    public String getModifier1() {
        return modifier1;
    }

    public void setModifier1(String modifier1) {
        this.modifier1 = modifier1;
    }

    public String getModifier2() {
        return modifier2;
    }

    public void setModifier2(String modifier2) {
        this.modifier2 = modifier2;
    }

    public String getModifier3() {
        return modifier3;
    }

    public void setModifier3(String modifier3) {
        this.modifier3 = modifier3;
    }

    public String getPrimaryICD9() {
        return primaryICD9;
    }

    public void setPrimaryICD9(String primaryICD9) {
        this.primaryICD9 = primaryICD9;
    }

    public String getSecondaryICD9() {
        return secondaryICD9;
    }

    public void setSecondaryICD9(String secondaryICD9) {
        this.secondaryICD9 = secondaryICD9;
    }

    public Date getCreationDateAudit() {
        return creationDateAudit;
    }

    public void setCreationDateAudit(Date creationDateAudit) {
        this.creationDateAudit = creationDateAudit;
    }

    public String getAccessionID() {
        return accessionID;
    }

    public void setAccessionID(String accessionID) {
        this.accessionID = accessionID;
    }

    public String getEmerRelatedIndicator() {
        return emerRelatedIndicator;
    }

    public void setEmerRelatedIndicator(String emerRelatedIndicator) {
        this.emerRelatedIndicator = emerRelatedIndicator;
    }


    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(String lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getReversalDate() {
        return reversalDate;
    }

    public void setReversalDate(String reversalDate) {
        this.reversalDate = reversalDate;
    }

    public String getSecondaryDateFiled() {
        return secondaryDateFiled;
    }

    public void setSecondaryDateFiled(String secondaryDateFiled) {
        this.secondaryDateFiled = secondaryDateFiled;
    }

    public String getSecondaryLastDatePaid() {
        return secondaryLastDatePaid;
    }

    public void setSecondaryLastDatePaid(String secondaryLastDatePaid) {
        this.secondaryLastDatePaid = secondaryLastDatePaid;
    }

    public String getGuarantorLastDatePaid() {
        return guarantorLastDatePaid;
    }

    public void setGuarantorLastDatePaid(String guarantorLastDatePaid) {
        this.guarantorLastDatePaid = guarantorLastDatePaid;
    }

    public String getPrimaryLastDatePaid() {
        return primaryLastDatePaid;
    }

    public void setPrimaryLastDatePaid(String primaryLastDatePaid) {
        this.primaryLastDatePaid = primaryLastDatePaid;
    }

    public String getdOSTo() {
        return dOSTo;
    }

    public void setdOSTo(String dOSTo) {
        this.dOSTo = dOSTo;
    }

    public String getStateOfAccident() {
        return stateOfAccident;
    }

    public void setStateOfAccident(String stateOfAccident) {
        this.stateOfAccident = stateOfAccident;
    }

    public String getdOSFrom() {
        return dOSFrom;
    }

    public void setdOSFrom(String dOSFrom) {
        this.dOSFrom = dOSFrom;
    }

    public String getTertiaryICD9() {
        return tertiaryICD9;
    }

    public void setTertiaryICD9(String tertiaryICD9) {
        this.tertiaryICD9 = tertiaryICD9;
    }

    public String getOtherICD9() {
        return otherICD9;
    }

    public void setOtherICD9(String otherICD9) {
        this.otherICD9 = otherICD9;
    }

    public String getAtAgency() {
        return atAgency;
    }

    public void setAtAgency(String atAgency) {
        this.atAgency = atAgency;
    }

    public String getDenialReasonID() {
        return denialReasonID;
    }

    public void setDenialReasonID(String denialReasonID) {
        this.denialReasonID = denialReasonID;
    }

    public String getAccident() {
        return accident;
    }

    public void setAccident(String accident) {
        this.accident = accident;
    }

    public String getdOAccident() {
        return dOAccident;
    }

    public void setdOAccident(String dOAccident) {
        this.dOAccident = dOAccident;
    }

    public String getReferringProvideCode() {
        return referringProvideCode;
    }

    public void setReferringProvideCode(String referringProvideCode) {
        this.referringProvideCode = referringProvideCode;
    }

    public List<Insurance> getInsurance() {
        return insurance;
    }

    public void setInsurance(List<Insurance> insurance) {
        this.insurance = insurance;
    }

    public String getwRvu() {
        return wRvu;
    }

    public void setwRvu(String wRvu) {
        this.wRvu = wRvu;
    }

    public String getDateOfService() {
        return dateOfService;
    }

    public void setDateOfService(String dateOfService) {
        this.dateOfService = dateOfService;
    }

    public String getModality() {
        return modality;
    }

    public void setModality(String modality) {
        this.modality = modality;
    }

    public String getProcedureDescription() {
        return procedureDescription;
    }

    public void setProcedureDescription(String procedureDescription) {
        this.procedureDescription = procedureDescription;
    }

    public Physician getRenderingProvider() {
        return renderingProvider;
    }

    public void setRenderingProvider(Physician renderingProvider) {
        this.renderingProvider = renderingProvider;
    }

    public Physician getReferringProvider() {
        return referringProvider;
    }

    public void setReferringProvider(Physician referringProvider) {
        this.referringProvider = referringProvider;
    }

    public OfficeLocation getLocation() {
        return location;
    }

    public void setLocation(OfficeLocation location) {
        this.location = location;
    }

    public String getDiagnosis1ICD10() {
        return diagnosis1ICD10;
    }

    public void setDiagnosis1ICD10(String diagnosis1ICD10) {
        this.diagnosis1ICD10 = diagnosis1ICD10;
    }

    public String getDiagnosis2ICD10() {
        return diagnosis2ICD10;
    }

    public void setDiagnosis2ICD10(String diagnosis2ICD10) {
        this.diagnosis2ICD10 = diagnosis2ICD10;
    }

    public String getDiagnosis3ICD10() {
        return diagnosis3ICD10;
    }

    public void setDiagnosis3ICD10(String diagnosis3ICD10) {
        this.diagnosis3ICD10 = diagnosis3ICD10;
    }

    public String getDiagnosis4ICD10() {
        return diagnosis4ICD10;
    }

    public void setDiagnosis4ICD10(String diagnosis4ICD10) {
        this.diagnosis4ICD10 = diagnosis4ICD10;
    }

    public String getDiagnosis5ICD10() {
        return diagnosis5ICD10;
    }

    public void setDiagnosis5ICD10(String diagnosis5ICD10) {
        this.diagnosis5ICD10 = diagnosis5ICD10;
    }

    public String getDiagnosis6ICD10() {
        return diagnosis6ICD10;
    }

    public void setDiagnosis6ICD10(String diagnosis6ICD10) {
        this.diagnosis6ICD10 = diagnosis6ICD10;
    }

    public String getDiagnosis7ICD10() {
        return diagnosis7ICD10;
    }

    public void setDiagnosis7ICD10(String diagnosis7ICD10) {
        this.diagnosis7ICD10 = diagnosis7ICD10;
    }

    public String getDiagnosis8ICD10() {
        return diagnosis8ICD10;
    }

    public void setDiagnosis8ICD10(String diagnosis8ICD10) {
        this.diagnosis8ICD10 = diagnosis8ICD10;
    }

    public String getDiagnosis9ICD10() {
        return diagnosis9ICD10;
    }

    public void setDiagnosis9ICD10(String diagnosis9ICD10) {
        this.diagnosis9ICD10 = diagnosis9ICD10;
    }

    public String getDiagnosis10ICD10() {
        return diagnosis10ICD10;
    }

    public void setDiagnosis10ICD10(String diagnosis10ICD10) {
        this.diagnosis10ICD10 = diagnosis10ICD10;
    }

    public String getDiagnosis11ICD10() {
        return diagnosis11ICD10;
    }

    public void setDiagnosis11ICD10(String diagnosis11ICD10) {
        this.diagnosis11ICD10 = diagnosis11ICD10;
    }

    public String getDiagnosis12ICD10() {
        return diagnosis12ICD10;
    }

    public void setDiagnosis12ICD10(String diagnosis12ICD10) {
        this.diagnosis12ICD10 = diagnosis12ICD10;
    }

    public String getDiagnosisFullICD10() {
        return diagnosisFullICD10;
    }

    public void setDiagnosisFullICD10(String diagnosisFullICD10) {
        this.diagnosisFullICD10 = diagnosisFullICD10;
    }

    public Boolean getMbx() {
        return mbx;
    }

    public void setMbx(Boolean mbx) {
        this.mbx = mbx;
    }
}
