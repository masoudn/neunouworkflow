package com.neunou.hl7persistence.Impl;

import com.neunou.hl7persistence.Interfaces.PhysicianScheduleDAO;
import com.neunou.schema.Physician;
import com.neunou.schema.PhysicianSchedule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * Created by Masoud on 4/1/15.
 */
@Repository
public class PhysicianScheduleDAOImpl implements PhysicianScheduleDAO {
    @Autowired
    private MongoTemplate mongoTemplate;
    private String PhysicianSchedule_COL = "physicianSchedule";
    @Override
    public String add(Object object) {
        PhysicianSchedule physicianSchedule = (PhysicianSchedule) object;
        physicianSchedule.setCreatedDate(new Date());
        if(physicianSchedule.getPhysicianScheduleId()==null){
            if(!mongoTemplate.collectionExists(PhysicianSchedule_COL)){
                mongoTemplate.createCollection(PhysicianSchedule_COL);
                mongoTemplate.insert(physicianSchedule,PhysicianSchedule_COL);
                return physicianSchedule.getPhysicianScheduleId();
            }
            else {
                mongoTemplate.insert(physicianSchedule,PhysicianSchedule_COL);
                return physicianSchedule.getPhysicianScheduleId();
            }
        }
        else{
            this.update(object);
            return physicianSchedule.getPhysicianScheduleId();
        }
    }

    @Override
    public void delete(Object object) {
        PhysicianSchedule physicianSchedule = (PhysicianSchedule) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianScheduleId").is(physicianSchedule.getPhysicianScheduleId()));
        mongoTemplate.remove(query,PhysicianSchedule.class,PhysicianSchedule_COL);
    }

    @Override
    public void update(Object object) {
        PhysicianSchedule physicianSchedule = (PhysicianSchedule) object;
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianScheduleId").is(physicianSchedule.getPhysicianScheduleId()));

        physicianSchedule.setModifiedDate(new Date());
        Update update = new Update();

        update.set("physicianId",physicianSchedule.getPhysicianId());
        update.set("scheduleDate",physicianSchedule.getScheduleDate());
        update.set("startTime",physicianSchedule.getStartTime());
        update.set("endTime",physicianSchedule.getEndTime());
        update.set("officeLocationId",physicianSchedule.getOfficeLocationId());
        update.set("shiftName",physicianSchedule.getShiftName());
        update.set("credit",physicianSchedule.getCredit());
        update.set("modifiedDate",physicianSchedule.getModifiedDate());

        mongoTemplate.findAndModify(query,update,PhysicianSchedule.class,PhysicianSchedule_COL);

    }

    @Override
    public Object getById(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where("physicianScheduleId").is(id));

        return mongoTemplate.find(query,PhysicianSchedule.class,PhysicianSchedule_COL);
    }

    @Override
    public List getAll() {
        return mongoTemplate.findAll(PhysicianSchedule.class,PhysicianSchedule_COL);
    }

    @Override
    public int count() {
        return getAll().size();
    }

    @Override
    public Boolean existance(String element, String elementValue) {
        Boolean exists;
        Query query = new Query();
        query.addCriteria(Criteria.where(element).is(elementValue));
        exists = mongoTemplate.exists(query,PhysicianSchedule.class,PhysicianSchedule_COL);
        return exists;
    }
}
