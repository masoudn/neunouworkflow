package com.neunou.wi.API;

import com.neunou.hl7manager.interfaces.ExamsManager;
import com.neunou.schema.Exam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

/**
 * Created by root on 4/29/15.
 */
@Controller
@RequestMapping(value="/api/exam")
public class ExamsAPI {
    private static final Logger LOG = Logger.getLogger(ExamsAPI.class.getName());
    @Autowired
    private ExamsManager man;

    @RequestMapping(method = RequestMethod.GET, value="/getAll",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Exam> getAllExams(){
        return man.getAll();
    }

    @RequestMapping(method = RequestMethod.POST, value="/save",consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Exam saveExam(@RequestBody Exam exam){
        LOG.info("Adding new Exam...");
        man.add(exam);
        return exam;
    }

    @RequestMapping(method = RequestMethod.GET, value="/get/{examId:.+}",produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Exam getExamById(@PathVariable("examId") String examId){
        return (Exam)man.getById(examId);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/delete/{examId:.+}")
    public void deleteExamById (@PathVariable("examId") String examId){
        LOG.info("Deleting OfficeLocation...");
        man.delete(man.getById(examId));
    }

}
