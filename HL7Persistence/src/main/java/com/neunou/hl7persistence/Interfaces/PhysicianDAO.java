package com.neunou.hl7persistence.Interfaces;

import com.neunou.schema.Physician;
import org.springframework.stereotype.Service;

/**
 * Created by Masoud on 3/31/15.
 */
@Service
public interface PhysicianDAO extends GenericInterface {
    Physician getPhysicianByNPI(String npi);
    Physician getPhysicianByName (String fName, String lName);
}
